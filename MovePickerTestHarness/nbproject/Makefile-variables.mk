#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux
CND_ARTIFACT_NAME_Debug=movepickertestharness
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux/movepickertestharness
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux/package
CND_PACKAGE_NAME_Debug=movepickertestharness.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux/package/movepickertestharness.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux
CND_ARTIFACT_NAME_Release=movepickertestharness
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux/movepickertestharness
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux/package
CND_PACKAGE_NAME_Release=movepickertestharness.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux/package/movepickertestharness.tar
# WinRelease configuration
CND_PLATFORM_WinRelease=MinGW-Windows
CND_ARTIFACT_DIR_WinRelease=dist/WinRelease/MinGW-Windows
CND_ARTIFACT_NAME_WinRelease=movepickertestharness
CND_ARTIFACT_PATH_WinRelease=dist/WinRelease/MinGW-Windows/movepickertestharness
CND_PACKAGE_DIR_WinRelease=dist/WinRelease/MinGW-Windows/package
CND_PACKAGE_NAME_WinRelease=movepickertestharness.tar
CND_PACKAGE_PATH_WinRelease=dist/WinRelease/MinGW-Windows/package/movepickertestharness.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
