#include <iostream>
#include "GameController.h"
#include "MCTSTreeSearchLogger.h"
#include "SgfInfluenceLogger.h"
#include "SgfLooseGroupsLogger.h"
#include "SgfBouzyMapLogger.h"
#include "SgfTerritoryLogger.h"
#include "TreeSearchLogger.h"
#include "SgfReader.h"
#include "Utility.h"
#include "EngineMoveLogger.h"
#include "GameEngineProperties.h"

int main()
{
    GameEngineProperties properties;
    
    GoLogic::SgfReader reader;
    GoLogic::SimpleSgfRecord record = reader.readSimpleSgfFile("../../sgf/size7/game4.sgf");
    
    EngineMoveLogger::instance()->enable();
    MCTS::TreeSearchLogger::instance()->enable();

    SgfInfluenceLogger::instance()->enable();
    SgfInfluenceLogger::instance()->setKomi(record.komi());
    SgfInfluenceLogger::instance()->setBoardSize(record.boardSize());

    SgfLooseGroupsLogger::instance()->enable();
    SgfLooseGroupsLogger::instance()->setKomi(record.komi());
    SgfLooseGroupsLogger::instance()->setBoardSize(record.boardSize());

    SgfBouzyMapLogger::instance()->enable();
    SgfBouzyMapLogger::instance()->setKomi(record.komi());
    SgfBouzyMapLogger::instance()->setBoardSize(record.boardSize());

    SgfTerritoryLogger::instance()->enable();
    SgfTerritoryLogger::instance()->setKomi(record.komi());
    SgfTerritoryLogger::instance()->setBoardSize(record.boardSize());
    
    TreeSearchLogger::instance()->enable();

    GoEngine::GameController controller(GoLogic::Ruleset::CHINESE, GoLogic::SuperkoRuleset::PSK);
    controller.boardsize(record.boardSize());

    for (auto& sgf_move : record.moveHistory())
    {
        controller.makeMove(sgf_move);
        controller.generateDebugInfo();
    }
    
    std::cout << "Best move = ";
    std::cout << GoLogic::Utility::MoveAsString(controller.generateMove(GoLogic::StoneColor::BLACK, false)) << std::endl;
    std::cout << "Expected C4" << std::endl;
    
    return 0;
}
