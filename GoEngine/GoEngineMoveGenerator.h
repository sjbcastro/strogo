#pragma once

#include "MCTSMoveGenerator.h"

#include "GoGameScorer.h"
#include "GoPossibleMoves.h"
#include "GameState.h"

namespace GoEngine
{
	class MoveGenerator : public MCTS::MoveGenerator
	{
	public:

        void setState(const GameState& game_state) { _game_state = game_state; }

        // Returns the possible moves for the current game state
        virtual std::unique_ptr<MCTS::PossibleMoves> possibleMoves() const override;

        // Populates the possible moves for the current game state. Basically the same as the other overload but will overwrite existing memory.
        virtual void possibleMoves(std::shared_ptr<MCTS::PossibleMoves>&) override;

        // Used to initialise the current instance based on the input
        virtual void set(const MCTS::MoveGenerator&) override;

        // Clones the current instance
        virtual std::unique_ptr<MCTS::MoveGenerator> clone() const override;

        // Takes the current move set and advances the game state
        virtual void processMove(const MCTS::Move&) override;

        // This is called in the event that the tree search is empty i.e. there are no legal moves to make. As I recall this was an edge case that would crop up in Battlesnake
        // when it was cornered, and for some games probably won't ever happen (e.g. by the time this case is reached for Tic-Tac-Toe, the game is already over)
        virtual std::shared_ptr<MCTS::Move> defaultMoveWhenNoneFound() const override;

        // Returns true if the game is over, and returns a score via the output argument
        virtual bool gameOver(float& score) const override;

        virtual std::shared_ptr<MCTS::Move> generateRandomMove(std::mt19937& rng) override;

        virtual bool nextMoveIsMainPlayer() const override { return _requested_next_move_color == _next_move_color; }

        void flipColour();

        virtual void print() const override { _game_state.currentBoard().printState(); }

        GoLogic::StoneColor nextMoveColor() const;
        void nextMoveColor(GoLogic::StoneColor color) const;

	private:

        virtual void generateRandomMove(std::mt19937& rng, GoLogic::Stone& candidate_move, int& n_empty_points) const;

        mutable std::vector<std::vector<int8_t>> _occupied_points;

        GameState _game_state;
        mutable GoLogic::StoneColor _next_move_color;
        mutable GoLogic::StoneColor _requested_next_move_color;
    };
}
