#pragma once

#include "Board.h"
#include "Rulesets.h"
#include "MCTSPossibleMoves.h"

#include "GoEngineMove.h"

#include <vector>

namespace GoEngine
{
    class GameState
    {
    public:

        GameState() = default;
        GameState(uint8_t sz, GoLogic::Ruleset r, GoLogic::SuperkoRuleset sk);

        // Copy constructor
        GameState(const GameState& rhs) = default;

        bool addStone(const GoLogic::Stone& s);
        bool canAddStone(const GoLogic::Stone& s);

        const GoLogic::Board& currentBoard() const { return _board; }

        float komi() const { return _komi; }
        void komi(float val) { _komi = val; }

        GoLogic::Move lastMove() const { return _last_move; }

        void pass();

        void updateMetaDataWithLastMove();

        void logMetaData() const;

    private:

        void resetTempBoard(const GoLogic::Stone& last_move);

        bool doesMoveViolateKo(const GoLogic::Stone& s) const;

        GoLogic::Board                      _board;
        GoLogic::Board                      _temp_board;
        
        std::vector<GoLogic::ZobristHash>   _board_hashes;
        mutable int                         _hash_search_limit = 0;
        GoLogic::Ruleset                    _ruleset;
        GoLogic::SuperkoRuleset             _superko_ruleset;
        float                               _komi = 6.5;
        GoLogic::Move                       _last_move;
    };
}