#include "EngineMoveLogger.h"
#include <sstream>

void EngineMoveLogger::logMove(const GoLogic::Move& m)
{
    writeLine(m.asString());
}