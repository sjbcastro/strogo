#include "SgfLogger.h"
#include "Utility.h"
#include <fstream>

std::string SgfMove::toSgfString() const
{
    std::string output;

    // The move itself
    if ( _move.stone().color() == GoLogic::StoneColor::BLACK ) output += ";B[";
    else output += ";W[";

    output += GoLogic::Utility::PointAsSgfString(_move.stone().point());
    output += "]";

    // Annotations
    for (auto it = _annotations.begin(); it != _annotations.end(); ++it )
    {
        output += (*it)->toSgfString();
    }

    return output;
}

void SgfMove::addAnnotation(std::unique_ptr<SgfAnnotation>&& annotation)
{
    _annotations.emplace_back(std::move(annotation));
}

void SgfLogger::logMove(const GoLogic::Move& m)
{
    if (_enabled)
    {
        _moves.push_back(m);
        write();
    }
}

void SgfLogger::write()
{
    std::ofstream out;
    out.open(_filepath, std::ios::app);
    
    out << "(;FF[4]AP[StroGo 0.1.0]GM[1]SZ[" << _size << "]\n";
    out << "KM[" << _komi << "]\n";
    out << "(\n";

    for (auto& sgf_move : _moves)
    {
        out << sgf_move.toSgfString();
    }

    out << ")\n)\n";

    out.close();
}

void SgfLogger::annotateLastMove(std::unique_ptr<SgfAnnotation>&& annotation)
{
    if (_enabled)
    {
        assert(!_moves.empty());
        _moves.back().addAnnotation(std::move(annotation));
        write();
    }
}
