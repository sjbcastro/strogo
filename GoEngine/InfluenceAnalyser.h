#pragma once

#include "GameState.h"
#include "InfluenceMap.h"

class InfluenceAnalyser
{
public:
    virtual InfluenceMap analyseInfluence(const GoLogic::Board& board) = 0;
};
