#pragma once

#include "SgfLogger.h"
#include "Singleton.h"
#include "InfluenceMap.h"

class SgfLooseGroupsLogger :  public SgfLogger, public Singleton<SgfLooseGroupsLogger>
{
public:
    virtual std::string filename() const override { return "SgfLooseGroupsLog.sgf"; }

    //void log(const std::list<LooseGroup>& loose_groups);
};

