#include "GameState.h"

#include "GoEngineMove.h"

namespace GoEngine
{
    GameState::GameState(uint8_t sz, GoLogic::Ruleset r, GoLogic::SuperkoRuleset sk)
        : _ruleset(r)
        , _superko_ruleset(sk)
        , _board(sz)
        , _temp_board(sz)
    {
        _board_hashes.reserve(300);
    }

    bool GameState::addStone(const GoLogic::Stone& s)
    {
        if (_temp_board.addStone(s))
        {
            if (!doesMoveViolateKo(s))
            {
                _board = _temp_board;
                _last_move = GoLogic::Move(s);
                _board_hashes.push_back(_board.zobristHash());

                return true;
            }

            // Added a stone but was not legal due to superko - reset the temp board
            resetTempBoard(s);
        }

        return false;
    }

    bool GameState::canAddStone(const GoLogic::Stone& s)
    {
        bool can_add = false;
        if (_temp_board.addStone(s))
        {
            if (!doesMoveViolateKo(s))
            {
                can_add = true;
            }

            resetTempBoard(s);
        }

        return can_add;
    }

    void GameState::resetTempBoard(const GoLogic::Stone& last_move)
    {
        if (_temp_board.canUndoLastMoveByRemoving())
        {
            _temp_board.undoLastMove(last_move);
        }
        else
        {
            _temp_board = _board;
        }
    }

    bool GameState::doesMoveViolateKo(const GoLogic::Stone& s) const
    {
        // Ignoring move history last move was legal, now check it does not violate ko rules
        if (_ruleset == GoLogic::Ruleset::CHINESE)
        {
            if (_superko_ruleset == GoLogic::SuperkoRuleset::PSK)
            {
                // Chinese rules, positional superko, no position may be repeated twice
                // regardless of whose turn it is

                // Use Zobrist hashing for efficiency
                // Temp board already has the new stone added so we can just grab the hash from there
                GoLogic::ZobristHash new_hash = _temp_board.zobristHash();

                // The number of captures indicates how far back in the history we need to go
                _hash_search_limit += _temp_board.numberStonesLastCapture();
                int search_counter = _hash_search_limit;

                auto hash = _board_hashes.rbegin();
                while (search_counter > 0 && hash != _board_hashes.rend())
                {
                    if (*hash == new_hash) return true;

                    ++hash;
                    --search_counter;
                }

                return false;
            }
            else
            {
                return true;
            }
        }

        return true;
    }

    void GameState::pass()
    {
        _last_move = GoLogic::Move(GoLogic::MoveType::PASS);
    }

    void GameState::updateMetaDataWithLastMove()
    {
        //_meta_data.updateWithLastMove(_last_move, currentBoard().lastCapturedGroups());
    }

    void GameState::logMetaData() const
    {
    }
}