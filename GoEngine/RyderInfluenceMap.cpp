#include "RyderInfluenceMap.h"
#include <algorithm>

RyderInfluenceMap::RyderInfluenceMap(size_t board_size)
    : InfluenceMap(board_size)
{
    _ryder_function.push_back({0, 0, 0, 1, 0, 0, 0});
    _ryder_function.push_back({0, 1, 2, 5, 2, 1, 0});
    _ryder_function.push_back({0, 2, 6, 13, 6, 2, 0});
    _ryder_function.push_back({1, 5, 13, 64, 13, 5, 1});
    _ryder_function.push_back({0, 2, 6, 13, 6, 2, 0});
    _ryder_function.push_back({0, 1, 2, 5, 2, 1, 0});
    _ryder_function.push_back({0, 0, 0, 1, 0, 0, 0});

    _territory_threshold = 7;
    _capture_threshold = 25;
}

void RyderInfluenceMap::updateWithStone(const GoLogic::Stone& stone, bool stone_removed)
{
    int32_t i = stone.i();
    int32_t j = stone.j();
    int32_t zero = 0;
    int32_t board_size = _influence.size();

    size_t min_i = static_cast<size_t>(std::max(i - 3, zero));
    size_t min_j = static_cast<size_t>(std::max(j - 3, zero));

    size_t max_i = static_cast<size_t>(std::min(i + 3, board_size - 1));
    size_t max_j = static_cast<size_t>(std::min(j + 3, board_size - 1));
    
    int8_t add_remove_multiplier = stone_removed ? -1 : 1;

    for (size_t x = min_i; x <= max_i; ++x)
    {
        for (size_t y = min_j; y <= max_j; ++y)
        {
            size_t x_index = x - i + 3;
            size_t y_index = y - j + 3;

            _influence[x][y] += add_remove_multiplier * 
                    static_cast<int8_t>(stone.color()) * _ryder_function[x_index][y_index];
        }
    }

}

//void RyderInfluenceMap::updateWithLastMove(const GoLogic::Move& last_move, const std::vector<GoLogic::ConnectedGroup>& last_captured_groups)
//{
//    if (!last_move.isPass() && !last_move.isResign())
//    {
//        updateWithStone(last_move.stone());
//        
//        for (const auto& group : last_captured_groups)
//        {
//            for (const auto& stone : group.stones())
//            {
//                updateWithStone(stone, true);
//            }
//        }
//    }
//}

std::unique_ptr<InfluenceMap> RyderInfluenceMap::clone() const
{
    return std::unique_ptr<InfluenceMap>(new RyderInfluenceMap(*this));
}

TerritoryMap RyderInfluenceMap::createTerritoryMap(const GoLogic::Board& board) const
{
    int32_t board_size = _influence.size();
    TerritoryMap territory = TerritoryMap(board_size,std::vector<int8_t>(board_size,0));
    
    const auto& board_state = board.boardState();
    const int8_t BLACK = static_cast<int8_t>(GoLogic::StoneColor::BLACK);
    const int8_t WHITE = static_cast<int8_t>(GoLogic::StoneColor::WHITE);
    
    for (size_t x = 0; x < board_state.size(); ++x)
    {
        for (size_t y = 0; y < board_state.size(); ++y)
        {
            if (board_state[x][y] == BLACK)
            {
                if (_influence[x][y] < _capture_threshold) territory[x][y] = WHITE;
            }
            else if (board_state[x][y] == WHITE)
            {
                if (_influence[x][y] > -_capture_threshold) territory[x][y] = BLACK;
            }
            else
            {
                if (_influence[x][y] > _territory_threshold) territory[x][y] = BLACK;
                else if (_influence[x][y] < -_territory_threshold) territory[x][y] = WHITE;
            }
        }
    }
    
    return territory;
}