#pragma once

#include "SgfLogger.h"
#include "Singleton.h"
#include "InfluenceMap.h"

class SgfInfluenceLogger :  public SgfLogger, public Singleton<SgfInfluenceLogger>
{
public:
    virtual std::string filename() const override { return "SgfInfluenceLog.sgf"; }

    void logInfluence(InfluenceMap* result);
};
