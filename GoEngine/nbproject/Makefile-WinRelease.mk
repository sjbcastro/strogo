#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW-Windows
CND_DLIB_EXT=dll
CND_CONF=WinRelease
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/AlphaBetaSearchTee.o \
	${OBJECTDIR}/BruteForceSearchTree.o \
	${OBJECTDIR}/GameController.o \
	${OBJECTDIR}/GameEngine.o \
	${OBJECTDIR}/GameEngineProperties.o \
	${OBJECTDIR}/GameEngineUtils.o \
	${OBJECTDIR}/GameState.o \
	${OBJECTDIR}/GameStateMetaData.o \
	${OBJECTDIR}/GoEngineInfo.o \
	${OBJECTDIR}/GoMoveTreeNode.o \
	${OBJECTDIR}/GtpInterface.o \
	${OBJECTDIR}/InfluenceMap.o \
	${OBJECTDIR}/Logger.o \
	${OBJECTDIR}/LooseGroup.o \
	${OBJECTDIR}/RyderInfluenceMap.o \
	${OBJECTDIR}/ScoreEstimator.o \
	${OBJECTDIR}/SearchTree.o \
	${OBJECTDIR}/SgfAnnotations.o \
	${OBJECTDIR}/SgfBouzyMapLogger.o \
	${OBJECTDIR}/SgfInfluenceLogger.o \
	${OBJECTDIR}/SgfLogger.o \
	${OBJECTDIR}/SgfLooseGroupsLogger.o \
	${OBJECTDIR}/SgfTerritoryLogger.o \
	${OBJECTDIR}/SimpleMovePicker.o \
	${OBJECTDIR}/TreeSearchType.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libgoengine.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libgoengine.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libgoengine.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libgoengine.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libgoengine.a

${OBJECTDIR}/AlphaBetaSearchTee.o: AlphaBetaSearchTee.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/AlphaBetaSearchTee.o AlphaBetaSearchTee.cpp

${OBJECTDIR}/BruteForceSearchTree.o: BruteForceSearchTree.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BruteForceSearchTree.o BruteForceSearchTree.cpp

${OBJECTDIR}/GameController.o: GameController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GameController.o GameController.cpp

${OBJECTDIR}/GameEngine.o: GameEngine.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GameEngine.o GameEngine.cpp

${OBJECTDIR}/GameEngineProperties.o: GameEngineProperties.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GameEngineProperties.o GameEngineProperties.cpp

${OBJECTDIR}/GameEngineUtils.o: GameEngineUtils.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GameEngineUtils.o GameEngineUtils.cpp

${OBJECTDIR}/GameState.o: GameState.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GameState.o GameState.cpp

${OBJECTDIR}/GameStateMetaData.o: GameStateMetaData.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GameStateMetaData.o GameStateMetaData.cpp

${OBJECTDIR}/GoEngineInfo.o: GoEngineInfo.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GoEngineInfo.o GoEngineInfo.cpp

${OBJECTDIR}/GoMoveTreeNode.o: GoMoveTreeNode.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GoMoveTreeNode.o GoMoveTreeNode.cpp

${OBJECTDIR}/GtpInterface.o: GtpInterface.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GtpInterface.o GtpInterface.cpp

${OBJECTDIR}/InfluenceMap.o: InfluenceMap.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/InfluenceMap.o InfluenceMap.cpp

${OBJECTDIR}/Logger.o: Logger.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Logger.o Logger.cpp

${OBJECTDIR}/LooseGroup.o: LooseGroup.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/LooseGroup.o LooseGroup.cpp

${OBJECTDIR}/RyderInfluenceMap.o: RyderInfluenceMap.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RyderInfluenceMap.o RyderInfluenceMap.cpp

${OBJECTDIR}/ScoreEstimator.o: ScoreEstimator.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ScoreEstimator.o ScoreEstimator.cpp

${OBJECTDIR}/SearchTree.o: SearchTree.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SearchTree.o SearchTree.cpp

${OBJECTDIR}/SgfAnnotations.o: SgfAnnotations.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SgfAnnotations.o SgfAnnotations.cpp

${OBJECTDIR}/SgfBouzyMapLogger.o: SgfBouzyMapLogger.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SgfBouzyMapLogger.o SgfBouzyMapLogger.cpp

${OBJECTDIR}/SgfInfluenceLogger.o: SgfInfluenceLogger.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SgfInfluenceLogger.o SgfInfluenceLogger.cpp

${OBJECTDIR}/SgfLogger.o: SgfLogger.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SgfLogger.o SgfLogger.cpp

${OBJECTDIR}/SgfLooseGroupsLogger.o: SgfLooseGroupsLogger.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SgfLooseGroupsLogger.o SgfLooseGroupsLogger.cpp

${OBJECTDIR}/SgfTerritoryLogger.o: SgfTerritoryLogger.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SgfTerritoryLogger.o SgfTerritoryLogger.cpp

${OBJECTDIR}/SimpleMovePicker.o: SimpleMovePicker.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SimpleMovePicker.o SimpleMovePicker.cpp

${OBJECTDIR}/TreeSearchType.o: TreeSearchType.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../Core -I./ -I../Thirdparty/Json -I../GtpUtility -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/TreeSearchType.o TreeSearchType.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
