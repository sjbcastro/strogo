#include "GoGameScorer.h"

#include "Board.h"
#include "Stone.h"

#include <vector>

void CheckNeighbours(const std::vector<std::vector<int8_t>>& board, size_t x, size_t y, bool& found_neighbouring_white_point, bool& found_neighbouring_black_point)
{
	found_neighbouring_black_point = false;
	found_neighbouring_white_point = false;

	if (x > 0)
	{
		if (board.at(x - 1).at(y) == static_cast<int8_t>(GoLogic::StoneColor::WHITE)) found_neighbouring_white_point = true;
		else if (board.at(x - 1).at(y) == static_cast<int8_t>(GoLogic::StoneColor::BLACK)) found_neighbouring_black_point = true;
	}

	if (y > 0)
	{
		if (board.at(x).at(y - 1) == static_cast<int8_t>(GoLogic::StoneColor::WHITE)) found_neighbouring_white_point = true;
		else if (board.at(x).at(y - 1) == static_cast<int8_t>(GoLogic::StoneColor::BLACK)) found_neighbouring_black_point = true;
	}

	if (x < board.size() - 1)
	{
		if (board.at(x + 1).at(y) == static_cast<int8_t>(GoLogic::StoneColor::WHITE)) found_neighbouring_white_point = true;
		else if (board.at(x + 1).at(y) == static_cast<int8_t>(GoLogic::StoneColor::BLACK)) found_neighbouring_black_point = true;
	}

	if (y < board[0].size() - 1)
	{
		if (board.at(x).at(y + 1) == static_cast<int8_t>(GoLogic::StoneColor::WHITE)) found_neighbouring_white_point = true;
		else if (board.at(x).at(y + 1) == static_cast<int8_t>(GoLogic::StoneColor::BLACK)) found_neighbouring_black_point = true;
	}
}

bool FloodFill(const std::vector<std::vector<int8_t>>& original, std::vector<std::vector<int8_t>>& territory_map, size_t x, size_t y, int8_t desired_value)
{
	bool territory_is_for_one_player = true;

	if (territory_map[x][y] == desired_value)
	{
		return true;
	}
	else
	{
		territory_map[x][y] = desired_value;
	}

	if (desired_value != 0)
	{
		auto opposing_colour = -desired_value;
		if (x > 0 && original.at(x - 1).at(y) == opposing_colour) territory_is_for_one_player = false;
		else if (x < original.size() - 1 && original.at(x + 1).at(y) == opposing_colour) territory_is_for_one_player = false;
		else if (y > 0 && original.at(x).at(y - 1) == opposing_colour) territory_is_for_one_player = false;
		else if (y < original.at(0).size() - 1 && original.at(x).at(y + 1) == opposing_colour) territory_is_for_one_player = false;
	}

	if (x > 0 && original.at(x - 1).at(y) == 0)
	{
		if (!FloodFill(original, territory_map, x - 1, y, desired_value)) territory_is_for_one_player = false;
	}
	if (x < original.size() - 1 && original.at(x + 1).at(y) == 0)
	{
		if (!FloodFill(original, territory_map, x + 1, y, desired_value)) territory_is_for_one_player = false;
	}
	if (y > 0 && original.at(x).at(y - 1) == 0)
	{
		if (!FloodFill(original, territory_map, x, y - 1, desired_value)) territory_is_for_one_player = false;
	}
	if (y < original.at(0).size() - 1 && original.at(x).at(y + 1) == 0)
	{
		if (!FloodFill(original, territory_map, x, y + 1, desired_value)) territory_is_for_one_player = false;
	}

	return territory_is_for_one_player;
}

bool GoGameScorer::scoreGame(const GoLogic::Board& board, float komi, float& score)
{
	bool found_unresolved_territory = false;

	_current_map = board.boardState();
	_territory_map = _current_map;
	_temp_map = _current_map;

	// Check we have at least one white and black stone on the border
	bool found_black = false, found_white = false;
	for (auto x = 0; x < _current_map.size(); ++x)
	{
		if (_current_map.at(x).at(0) == static_cast<int8_t>(GoLogic::StoneColor::BLACK)) found_black = true;
		else if (_current_map.at(x).at(0) == static_cast<int8_t>(GoLogic::StoneColor::WHITE)) found_white = true;

		if (_current_map.at(x).at(_current_map.size() - 1) == static_cast<int8_t>(GoLogic::StoneColor::BLACK)) found_black = true;
		else if (_current_map.at(x).at(_current_map.size() - 1) == static_cast<int8_t>(GoLogic::StoneColor::WHITE)) found_white = true;
	}

	for (auto y = 0; y < _current_map.size(); ++y)
	{
		if (_current_map.at(0).at(y) == static_cast<int8_t>(GoLogic::StoneColor::BLACK)) found_black = true;
		else if (_current_map.at(0).at(y) == static_cast<int8_t>(GoLogic::StoneColor::WHITE)) found_white = true;

		if (_current_map.at(_current_map.size() - 1).at(y) == static_cast<int8_t>(GoLogic::StoneColor::BLACK)) found_black = true;
		else if (_current_map.at(_current_map.size() - 1).at(y) == static_cast<int8_t>(GoLogic::StoneColor::WHITE)) found_white = true;
	}

	// Return early if we didn't as save us some work
	if (!found_black || !found_white) return false;

	for (auto x = 0; x < _current_map.size(); ++x)
		for (auto y = 0; y < _current_map.at(x).size(); ++y)
			_territory_map[x][y] = 0;

	for (auto x = 0; x < _current_map.size(); ++x)
	{
		for (auto y = 0; y < _current_map.at(x).size(); ++y)
		{
			if (_current_map.at(x).at(y) == 0)
			{
				bool found_neighbouring_white_point, found_neighbouring_black_point = false;
				CheckNeighbours(_current_map, x, y, found_neighbouring_white_point, found_neighbouring_black_point);

				std::unique_ptr<GoLogic::StoneColor> stone_colour = nullptr;
				if (found_neighbouring_black_point && found_neighbouring_white_point)
				{
					found_unresolved_territory = true;
				}
				else if (found_neighbouring_black_point)
				{
					stone_colour = std::make_unique<GoLogic::StoneColor>(GoLogic::StoneColor::BLACK);
				}
				else if (found_neighbouring_white_point)
				{
					stone_colour = std::make_unique<GoLogic::StoneColor>(GoLogic::StoneColor::WHITE);
				}

				if (stone_colour)
				{
					_temp_map = _territory_map;
					bool territory_ok = FloodFill(_current_map, _temp_map, x, y, static_cast<int8_t>(*stone_colour));
					if (territory_ok)
					{
						_territory_map = _temp_map;
					}
					else
					{
						found_unresolved_territory = true;

						_temp_map = _current_map;
						FloodFill(_current_map, _temp_map, x, y, 2);
						_current_map = _temp_map;
					}
				}
			}
		}
	}

	bool game_over = !found_unresolved_territory;
	if (game_over)
	{
		score = -komi + board.whiteCapturedStones() - board.blackCapturedStones();

		for (auto x = 0; x < _current_map.size(); ++x)
		{
			for (auto y = 0; y < _current_map.at(x).size(); ++y)
			{
				score += _territory_map.at(y).at(x);

				// to do : we're assuming Chinese rules here, this should be an input variable
				score += board.boardState().at(y).at(x);
			}
		}
	}
	else
	{
		score = 0.0;
	}

	return game_over;
}
