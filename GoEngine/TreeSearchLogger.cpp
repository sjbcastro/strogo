#include "TreeSearchLogger.h"
#include <sstream>

void TreeSearchLogger::logMove(const GoLogic::Move& m, int32_t search_depth, int32_t n_moves_left_to_search)
{
    if (_enabled)
    {
        int32_t n_spaces = search_depth - n_moves_left_to_search;

        std::ostringstream oss;

        for (int32_t i = 0; i < n_spaces; ++i) oss << " ";

        oss << m.asString();

        writeLine(oss.str());
    }
}

void TreeSearchLogger::logMoveResult(const GoLogic::Move& m, int32_t search_depth, double score)
{
    if (_enabled)
    {
        std::ostringstream oss;

        for (int32_t i = 0; i < search_depth; ++i) oss << " ";

        oss << m.asString() << " = " << score;

        writeLine(oss.str());
    }
}