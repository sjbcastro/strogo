#pragma once

#include "Logger.h"
#include "Singleton.h"
#include "Stone.h"

class EngineMoveLogger : public Logger, public Singleton<EngineMoveLogger>
{
public:
    virtual std::string filename() const override { return "EngineMoveLog.log"; }
    
    void logMove(const GoLogic::Move& m);
};
