#pragma once

#include "Logger.h"
#include "Singleton.h"
#include "Stone.h"
#include <inttypes.h>

class TreeSearchLogger : public Logger, public Singleton<TreeSearchLogger>
{
public:
    virtual std::string filename() const override { return "TreeSearch.log"; }
    void logMove(const GoLogic::Move& m, int32_t search_depth, int32_t n_moves_left_to_search);
    void logMoveResult(const GoLogic::Move& m, int32_t search_depth, double score);
};