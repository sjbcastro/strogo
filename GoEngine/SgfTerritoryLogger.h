#pragma once

#include "SgfBouzyMapLogger.h"

class SgfTerritoryLogger : public SgfBouzyMapLoggerCore, public Singleton<SgfTerritoryLogger>
{
public:
    virtual std::string filename() const override { return "SgfTerritoryLog.sgf"; }

    void logTerritoryInfo(const BouzyMap<int8_t> bouzy_map, double score, bool no_more_moves);
    void logTerritoryInfo(const std::vector<std::vector<int8_t>>& territory);
};
