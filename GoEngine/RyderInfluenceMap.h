#pragma once

#include "InfluenceMap.h"
#include "Stone.h"
#include <vector>

class RyderInfluenceMap : public InfluenceMap
{
public:
    RyderInfluenceMap(size_t board_size);
    
    //virtual void updateWithLastMove(const GoLogic::Move& last_move, const std::vector<GoLogic::ConnectedGroup>& last_captured_groups) override;
    
    virtual std::unique_ptr<InfluenceMap> clone() const override;
    
    virtual TerritoryMap createTerritoryMap(const GoLogic::Board& board_state) const;
    
private:
    
    void updateWithStone(const GoLogic::Stone& stone, bool stone_removed = false);
    
    std::vector<std::vector<int8_t>> _ryder_function;
};