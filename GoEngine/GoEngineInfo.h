#pragma once

#include <sstream>

namespace GoEngine
{
    class GoEngineInfo
    {
    public:
        std::string gtpProtocolVersion() const;
        std::string goEngineVersion() const;

        std::string goEngineName() const { return GOENGINE_NAME; }

    private:
        // Go text protocol version currently used
        const uint16_t GTP_PROTOCOL_VERSION = 2;

        // Version of the go engine
        const uint16_t GOENGINE_VERSION_MAJOR = 0;
        const uint16_t GOENGINE_VERSION_MINOR = 1;
        const uint16_t GOENGINE_VERSION_REVISION = 0;

        // Name of the engine
        const std::string GOENGINE_NAME = "StroGo";
    };
}