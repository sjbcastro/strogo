#pragma once

#include "GoEngineMove.h"
#include "MCTSPossibleMoves.h"

#include <map>
#include <vector>

namespace GoEngine
{
	class GameState;

	// Very much a work in progress. The initial strategy is as follows:
	// 1. GoPossibleMoves is just a very simple class - unlike other attempts which have a copy of the board and iterate through possible moves (which has a lower memory
	//		footprint, or so I'd imagine), we just store a map of player moves to opponent responses
	// 2. It is up to GameState to populate the possible moves and responses - in the first version this will just populate every possible legal move
	// 3. Future versions will most likely incorporate some heuristics to limit the moves we search e.g. early game we'd avoid playing at the edges and centre, late game we'd
	//		avoid playing into certain enemy territory
	//
	// Simple is best to begin with as we won't know where the bottlenecks lie, and we will need to do some profiling before we embark on the next steps. However, this is how
	// I envisage stuff proceeding:
	// - we will need to incorporate heuristics to avoid an explosion of the search tree. For a 19x19 board, because we consider the opponent response, there would be 129,960 
	//		nodes from the parent node!
	// - as more and more heuristics are incorporated, the more expensive the calculation of GoPossibleMoves will be. We will have to see whether we need to move away from a map
	//		structure.
	// - GoPossibleMoves will be calculated multiple times for a search tree node. This is because we expand a node one child at a time. So if a node had 80 possible moves, we 
	//		would end up calculating GoPossibleMoves 80 times before it is fully expanded.
	// - therefore there is potentially an optimisation to be had for the Go specific MCTS algorithm. This would be to make each node fully expanded when it is reached, so we 
	//		don't have to needlessly calculate GoPossibleMoves
	// - however, this might be a drop in the ocean - we still have to calculate GoPossibleMoves for every node we traverse during a simulation
	// - if redundant calculations of GoPossibleMoves does become a problem, then an alternative would be to cache them in a dedicated object so we can instead look up using the
	//		board (using a hash or some other means). This is probably a better approach, but difficult to implement - we'd need to remove all the redundant instances e.g.
	//		it would be pointless having lots of GoPossibleMove stuff for the Chinese opening when the Shusaku opening has been played.
	class GoPossibleMoves : public MCTS::PossibleMoves
	{
	public:

		void set(const std::vector<Move>& possible_moves);

		// Main function used for simulating random playouts
		virtual void generateRandomMove(std::mt19937& rng, std::shared_ptr<MCTS::Move>& move_made) const override;

		// This is used by the MCTS algorithm when considering whether a node has been fully expanded or not
		virtual size_t numPossibleNodes() const override;

		// This and the next method are used when expanding a node (i.e. populating it with subsequent child nodes). This should get the first possible move set (what 'first'
		// means is up to the subclass to decide)
		virtual std::unique_ptr<MCTS::Move> getFirstMove() override;

		// Used following the previous method when expanding a node. Basically used to iterate through the possible move sets, so that the SearchTreeNode can populate the
		// next child node
		virtual bool getNextMove(std::shared_ptr<MCTS::Move>& next_move) override;

	private:
		// Keep it simple for now - see class comment
		std::vector<Move> _possible_moves;
		std::vector<Move>::iterator _possible_move_iterator;
	};
}
