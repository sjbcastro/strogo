#include "SgfTerritoryLogger.h"
#include <sstream>

void SgfTerritoryLogger::logTerritoryInfo(const BouzyMap<int8_t> bouzy_map, double score, bool no_more_moves)
{
    logBinaryBouzyMap(bouzy_map);

    std::ostringstream oss;
    oss << "Score = " << score;
    oss << "\nNo more moves is " << (no_more_moves ? "true\n" : "false\n");

    std::unique_ptr<SgfAnnotation> comment
            = std::unique_ptr<SgfCommentAnnotation>(new SgfCommentAnnotation(oss.str()));
        annotateLastMove(std::move(comment));
}

void SgfTerritoryLogger::logTerritoryInfo(const std::vector<std::vector<int8_t>>& territory)
{
    if ( _enabled )
    {
        std::list<GoLogic::Point> black_points;
        std::list<GoLogic::Point> white_points;

        for ( uint8_t x = 0; x < territory.size(); ++x)
        {
            for ( uint8_t y = 0; y < territory.size(); ++y)
            {
                if ( territory[x][y] == 1 )
                    black_points.emplace_back(GoLogic::Point(GoLogic::PointLetter(x+1),y+1));
                else if ( territory[x][y] == -1 )
                    white_points.emplace_back(GoLogic::Point(GoLogic::PointLetter(x+1),y+1));
            }
        }

        std::unique_ptr<SgfAnnotation> black_annotations
            = std::unique_ptr<SgfMarkerAnnotation>(new SgfMarkerAnnotation(
            SgfMarkerAnnotation::MarkerType::CROSS, black_points));
        std::unique_ptr<SgfAnnotation> white_annotations
            = std::unique_ptr<SgfMarkerAnnotation>(new SgfMarkerAnnotation(
            SgfMarkerAnnotation::MarkerType::CIRCLE, white_points));

        annotateLastMove(std::move(black_annotations));
        annotateLastMove(std::move(white_annotations));
    }
}
