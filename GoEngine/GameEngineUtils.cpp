#include "GameEngineUtils.h"
#include <iostream>
#include "Utility.h"
#include "ScoreEstimator.h"

std::vector<GoLogic::Point> GameEngineUtils::emptyPoints(const GoLogic::Board& board)
{
    std::vector<GoLogic::Point> empty_points;
    for ( size_t x = 0; x < board._board_state.size(); ++x )
    {
        for ( size_t y = 0; y < board._board_state[x].size(); ++y )
        {
            if ( board._board_state[x][y] == 0)
            {
                GoLogic::PointLetter letter = (GoLogic::PointLetter)(x+1);
                empty_points.push_back(GoLogic::Point(letter, y+1));
            }
        }
    }

    return empty_points;
}

std::list<LooseGroup> GameEngineUtils::looseGroups(const GoLogic::Board& board)
{
    std::list<LooseGroup> loose_groups;
    std::list<GoLogic::ConnectedGroup> remaining_groups = board.groups();

    while (!remaining_groups.empty())
    {
        auto current_loose_group_it = loose_groups.end();

        // Check if the first remaining group is or isn't already part of a loose group
        for (auto it = loose_groups.begin(); it != loose_groups.end(); ++it)
        {
            if ( it->containsGroup(remaining_groups.front()) )
            {
                current_loose_group_it = it;
                break;
            }
        }

        // We didn't find a loose group already containing the current group so create a new loose group
        if ( current_loose_group_it == loose_groups.end() )
        {
            loose_groups.push_back(LooseGroup(remaining_groups.front()));
            current_loose_group_it = loose_groups.end();
            --current_loose_group_it;
        }

        // Loop over the other remaining groups and see if any are loosely connected
        auto it = remaining_groups.cbegin();
        for (++it; it != remaining_groups.cend(); ++it)
        {
            if ( areGroupsLooselyConnected(remaining_groups.front(), *it) )
            {
                current_loose_group_it->addGroup(*it);
            }
        }

        // Check if the stone has merged any loose groups
        auto first_found = loose_groups.end();
        for (auto it = loose_groups.begin(); it != loose_groups.end(); ++it)
        {
            if ( it->containsGroup(remaining_groups.front()) )
            {
                if ( first_found == loose_groups.end())
                {
                    first_found = it;
                }
                else
                {
                    // Merge next found and then erase
                    first_found->mergeGroupWithThis(*it);
                    it = loose_groups.erase(it);

                    if ( it == loose_groups.end()) break;
                }
            }
        }
        current_loose_group_it->ensureNoDuplicates(remaining_groups.front());

        // Remove from the remaining groups list
        remaining_groups.pop_front();
    }

    return loose_groups;
}

bool GameEngineUtils::areGroupsLooselyConnected(const GoLogic::ConnectedGroup& lhs, const GoLogic::ConnectedGroup& rhs)
{
    if ( lhs.stones().front().color() != rhs.stones().front().color()) return false;

    for (auto lhs_it = lhs.stones().begin(); lhs_it != lhs.stones().end(); ++lhs_it)
    {
        for (auto rhs_it = rhs.stones().begin(); rhs_it != rhs.stones().end(); ++rhs_it)
        {
            if (areStonesLooselyConnected(*lhs_it, *rhs_it))
            {
                return true;
            }
        }
    }

    return false;
}

bool GameEngineUtils::areStonesLooselyConnected(const GoLogic::Stone& lhs, const GoLogic::Stone& rhs)
{
    // The absolute differences in x and y must either be 0 and 2, 1 and 1, or 2 and 0
    // In either case the sum of the differences add up to 2 and is the only way for them to add up to 2
    uint8_t val = std::abs((uint8_t)lhs.x()-(uint8_t)rhs.x()) + std::abs(lhs.y()-rhs.y());
    return val == 2;
}
