#pragma once

#include <vector>
#include <iostream>
#include <memory>
#include "Stone.h"
#include "Board.h"

using TerritoryMap = std::vector<std::vector<int8_t>>;

class InfluenceMap
{
public:
    
    InfluenceMap(size_t board_size);
    
    void assign(const std::vector<std::vector<double>>& map);
    
    //virtual void updateWithLastMove(const GoLogic::Move& last_move, const std::vector<GoLogic::ConnectedGroup>& last_captured_groups) = 0;
    
    virtual std::unique_ptr<InfluenceMap> clone() const = 0;
    
    virtual TerritoryMap createTerritoryMap(const GoLogic::Board& board_state) const = 0;

    const std::vector<std::vector<double>>& influence() const { return _influence; }
    
    double territoryThreshold() const { return _territory_threshold; }
    double captureThreshold() const { return _capture_threshold; }

protected:
    std::vector<std::vector<double>> _influence;
    double _territory_threshold;
    double _capture_threshold;
};
