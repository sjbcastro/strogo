#pragma once

#include "Logger.h"
#include "Stone.h"
#include "SgfAnnotations.h"
#include <list>
#include <memory>

class SgfMove
{
public:
    SgfMove(const GoLogic::Move& m) : _move(m) {}
    std::string toSgfString() const;

    void addAnnotation(std::unique_ptr<SgfAnnotation>&& annotation);

private:
    GoLogic::Move _move;
    std::list<std::unique_ptr<SgfAnnotation>> _annotations;
};


class SgfLogger : public Logger
{
public:

    void logMove(const GoLogic::Move& m);
    void setBoardSize(uint16_t val) { _size = val; }
    void setKomi(double val) { _komi = val; }

protected:

    void annotateLastMove(std::unique_ptr<SgfAnnotation>&& annotation);

private:

    void write();

    std::list<SgfMove>  _moves;
    uint16_t            _size = 0;
    double              _komi = 0;
};
