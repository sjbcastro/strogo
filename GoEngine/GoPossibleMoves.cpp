#include "GoPossibleMoves.h"

namespace GoEngine
{
	void GoPossibleMoves::set(const std::vector<Move>& possible_moves)
	{
		_possible_moves = possible_moves;
		_possible_move_iterator = _possible_moves.begin();
	}
	void GoPossibleMoves::generateRandomMove(std::mt19937& rng, std::shared_ptr<MCTS::Move>& move_made) const
	{
		if (numPossibleNodes() > 0)
		{
			size_t move_index = rng() % numPossibleNodes();
			move_made = std::make_shared<Move>(_possible_moves.at(move_index));
		}
	}
	size_t GoPossibleMoves::numPossibleNodes() const
	{
		return _possible_moves.size();
	}

	std::unique_ptr<MCTS::Move> GoPossibleMoves::getFirstMove()
	{
		return std::make_unique<Move>(_possible_moves.front());
	}

	bool GoPossibleMoves::getNextMove(std::shared_ptr<MCTS::Move>& next_move)
	{
		if (++_possible_move_iterator == _possible_moves.end())
		{
			return false;
		}
		next_move = std::make_shared<Move>(*_possible_move_iterator);

		return true;
	}
}