#include "TreeSearchType.h"

std::string TreeSearchTypeToString(TreeSearchType val)
{
    switch (val)
    {
        case TreeSearchType::BRUTE_FORCE: return "BRUTE_FORCE";
        case TreeSearchType::ALPHA_BETA: return "ALPHA_BETA";
        default:
        {
            assert(false);
            return "BRUTE_FORCE";
        }
    }
}

TreeSearchType TreeSearchTypeFromString(const std::string& str)
{
    if (str == "BRUTE_FORCE") return TreeSearchType::BRUTE_FORCE;
    else if (str == "ALPHA_BETA") return TreeSearchType::ALPHA_BETA;
    else
    {
        assert(false);
        return TreeSearchType::BRUTE_FORCE;
    }
}