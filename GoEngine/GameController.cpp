#include "GameController.h"
#include "SgfInfluenceLogger.h"
#include "SgfLooseGroupsLogger.h"
#include "SgfBouzyMapLogger.h"
#include "SgfTerritoryLogger.h"
#include "EngineMoveLogger.h"
#include "GoEngineLogger.h"

namespace GoEngine
{
    GameController::GameController(GoLogic::Ruleset r, GoLogic::SuperkoRuleset sk, std::shared_ptr<GoEngineListener> engine_listener)
        : _game_state(19, r, sk)
        , _ruleset(r)
        , _superko_ruleset(sk)
        , _engine_listener(engine_listener)
    {
        if (_engine_listener)
            _engine_listener->pushGameState(_game_state);
    }

    bool GameController::makeMove(const GoLogic::Move& m)
    {
        EngineMoveLogger::instance()->logMove(m);

        // SGF related loggers
        SgfInfluenceLogger::instance()->logMove(m);
        SgfLooseGroupsLogger::instance()->logMove(m);
        SgfBouzyMapLogger::instance()->logMove(m);
        SgfTerritoryLogger::instance()->logMove(m);

        bool ok = true;
        if (m.isPass())
        {
            _game_state.pass();
        }
        else if (_game_state.addStone(m.stone()))
        {
            _game_state.logMetaData();
        }
        else
        {
            ok = false;
        }

        if (_engine_listener)
            _engine_listener->pushGameState(_game_state);

        return ok;
    }

    bool GameController::boardsize(uint8_t new_size)
    {
        GameState new_state(new_size, _ruleset, _superko_ruleset);

        if (new_state.currentBoard().boardSize() == 0)
        {
            return false;
        }

        _game_state = new_state;

        if (_engine_listener)
            _engine_listener->pushGameState(_game_state);

        return true;
    }

    void GameController::clearBoard()
    {
        _game_state = GameState(_game_state.currentBoard().boardSize(), _ruleset, _superko_ruleset);

        if (_engine_listener)
            _engine_listener->pushGameState(_game_state);
    }

    GoLogic::Move GameController::generateMove(GoLogic::StoneColor color, bool auto_add_stone)
    {
        GoEngineLogger::instance()->writeLine("Entering generate move...");
        GoLogic::Move m = _engine.generateMove(color, _game_state);
        GoEngineLogger::instance()->writeLine("Exited generate move...");

        if (!m.isPass() && !m.isResign() && auto_add_stone)
        {

            if (!_game_state.addStone(m.stone()))
            {
                // If for some reason we have failed to produce a legal move
                // do the honourable thing and resign - it's up to the engine to make sure
                // that this doesn't happen
                m.resign();
            }
        }

        EngineMoveLogger::instance()->logMove(m);

        // SGF related loggers
        SgfInfluenceLogger::instance()->logMove(m);
        SgfLooseGroupsLogger::instance()->logMove(m);
        SgfBouzyMapLogger::instance()->logMove(m);
        SgfTerritoryLogger::instance()->logMove(m);

        _game_state.logMetaData();

        if (_engine_listener)
            _engine_listener->pushGameState(_game_state);

        return m;
    }

    void GameController::generateDebugInfo()
    {
        _engine.generateDebugInfo(_game_state);
    }
}