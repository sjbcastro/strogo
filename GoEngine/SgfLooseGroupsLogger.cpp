#include "SgfLooseGroupsLogger.h"
#include <sstream>

//void SgfLooseGroupsLogger::log(const std::list<LooseGroup>& loose_groups)
//{
//    if ( _enabled )
//    {
//        std::list<std::pair<GoLogic::Point, int>> points;
//
//        int label = 1;
//        for (auto loose_group_it = loose_groups.cbegin(); loose_group_it != loose_groups.cend(); ++ loose_group_it)
//        {
//            for ( auto group_it = loose_group_it->groups().cbegin();
//                group_it != loose_group_it->groups().cend(); ++group_it )
//            {
//                for ( auto stone_it = group_it->stones().cbegin();
//                    stone_it != group_it->stones().cend(); ++stone_it)
//                {
//                    points.emplace_back(
//                        std::pair<GoLogic::Point,int>(stone_it->point(), label));
//                }
//            }
//
//            ++label;
//        }
//
//        std::unique_ptr<SgfAnnotation> annotation
//            = std::unique_ptr<SgfIntegerLabelAnnotation>(new SgfIntegerLabelAnnotation(points));
//        annotateLastMove(std::move(annotation));
//
//        std::ostringstream oss;
//        for (const LooseGroup& loose_group : loose_groups)
//        {
//            oss << loose_group.groups().begin()->stones().begin()->asString();
//            oss << " : " << loose_group.spacePoints() << "\n";
//        }
//
//        std::unique_ptr<SgfAnnotation> comment
//            = std::unique_ptr<SgfCommentAnnotation>(new SgfCommentAnnotation(oss.str()));
//        annotateLastMove(std::move(comment));
//    }
//}
