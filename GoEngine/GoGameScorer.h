#pragma once

#include <vector>
#include <cinttypes>

namespace GoLogic
{
	class Board;
}

class GoGameScorer
{
public:

	bool scoreGame(const GoLogic::Board& board, float komi, float& score);

private:
	std::vector<std::vector<int8_t>> _current_map;
	std::vector<std::vector<int8_t>> _territory_map;
	std::vector<std::vector<int8_t>> _temp_map;
};
