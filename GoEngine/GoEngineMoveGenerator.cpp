#include "GoEngineMoveGenerator.h"

namespace GoEngine
{
    std::unique_ptr<MCTS::PossibleMoves> MoveGenerator::possibleMoves() const
    {
        return std::make_unique< GoPossibleMoves>();
    }

    void MoveGenerator::possibleMoves(std::shared_ptr<MCTS::PossibleMoves>& possible_moves)
    {
        if (!possible_moves)
        {
            possible_moves = std::make_shared<GoPossibleMoves>();
        }
        auto concrete_possible_moves = std::dynamic_pointer_cast<GoPossibleMoves>(possible_moves);

        std::vector<Move> candidate_moves;

        GoLogic::Stone candidate_move(GoLogic::PointLetter::A, 1, _next_move_color);
        auto board_size = _game_state.currentBoard().boardSize();
        for (uint8_t x = 0; x < board_size; ++x)
        {
            GoLogic::PointLetter new_letter = static_cast<GoLogic::PointLetter>(x + 1);
            candidate_move.x(new_letter);
            for (uint8_t y = 0; y < board_size; ++y)
            {
                candidate_move.y(y + 1);

                if (_game_state.canAddStone(candidate_move))
                {
                    candidate_moves.push_back(candidate_move);
                }
            }
        }

        concrete_possible_moves->set(candidate_moves);
    }

    void MoveGenerator::set(const MCTS::MoveGenerator& game_state)
    {
        *this = dynamic_cast<const MoveGenerator&>(game_state);
    }

    std::unique_ptr<MCTS::MoveGenerator> MoveGenerator::clone() const
    {
        return std::make_unique<MoveGenerator>(*this);
    }

    void MoveGenerator::processMove(const MCTS::Move& move_made)
    {
        int_fast8_t current_player_id = static_cast<int_fast8_t>(_next_move_color);
        int_fast8_t opponent_player_id = -current_player_id;

        auto move = dynamic_cast<const Move&>(move_made);
        if (!move.isPass() && !move.isResign())
        {
            _game_state.addStone(move.stone());
        }

        flipColour();
    }

    std::shared_ptr<MCTS::Move> MoveGenerator::defaultMoveWhenNoneFound() const
    {
        return std::shared_ptr<MCTS::Move>(new GoEngine::Move(GoLogic::MoveType::PASS));
    }

    bool MoveGenerator::gameOver(float& score) const
    {
        GoGameScorer game_scorer;
        bool game_over = game_scorer.scoreGame(_game_state.currentBoard(), _game_state.komi(), score);

        if (game_over)
        {
            if (std::abs(score) > 0.0001)
            {
                score = (score > 0.0) ? 1.0 : 0.0;
            }
            else
            {
                score = 0.5;
            }

            if (_requested_next_move_color == GoLogic::StoneColor::WHITE)
            {
                score = 1.0 - score;
            }
        }

        return game_over;
    }

    std::shared_ptr<MCTS::Move> MoveGenerator::generateRandomMove(std::mt19937& rng)
    {
        int n_empty_points = _game_state.currentBoard().numberOfEmptyPoints();

        _occupied_points = _game_state.currentBoard().boardState();
        GoLogic::Stone first_move(GoLogic::PointLetter::INVALID, 0, _next_move_color);
        generateRandomMove(rng, first_move, n_empty_points);

        while (!_game_state.canAddStone(first_move))
        {
            if (--n_empty_points <= 0)
            {
                return std::make_shared<Move>(GoLogic::MoveType::PASS);
            }

            _occupied_points[first_move.i()][first_move.j()] = 1;
            generateRandomMove(rng, first_move, n_empty_points);
        }

        return std::make_shared<Move>(first_move);
    }

    void MoveGenerator::flipColour()
    {
        _next_move_color = (_next_move_color == GoLogic::StoneColor::BLACK) ? GoLogic::StoneColor::WHITE : GoLogic::StoneColor::BLACK;
    }

    GoLogic::StoneColor MoveGenerator::nextMoveColor() const
    {
        return _next_move_color;
    }

    void MoveGenerator::nextMoveColor(GoLogic::StoneColor color) const
    {
        _next_move_color = color;
        _requested_next_move_color = color;
    }

    void MoveGenerator::generateRandomMove(std::mt19937& rng, GoLogic::Stone& candidate_move, int& n_empty_points) const
    {
        int move_index = rng() % n_empty_points;
        int current_index = -1;

        while (true)
        {
            for (int x = 0; x < _occupied_points.size(); ++x)
            {
                for (int y = 0; y < _occupied_points.size(); ++y)
                {
                    if (_occupied_points.at(x).at(y) == 0)
                    {
                        candidate_move.x((GoLogic::PointLetter)(x + 1));
                        candidate_move.y(y + 1);
                        if (++current_index >= move_index) return;
                    }
                }
            }
        }
    }
}