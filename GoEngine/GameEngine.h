#pragma once

#include "MCTSAlgorithm.h"

#include "GameState.h"
#include "GameEngineProperties.h"
#include "GoEngineMoveGenerator.h"

#include <cstdlib>
#include <cmath>

namespace GoEngine
{
    class GameEngine
    {
    public:

        GameEngine();

        GoLogic::Move generateMove(GoLogic::StoneColor color, const GameState& current_state);

        void generateDebugInfo(GameState& current_state) const;

        void setGameEngineProperties(const GameEngineProperties& properties) { _properties = properties; }

    private:
        MoveGenerator _move_generator;
        GameEngineProperties _properties;
        MCTS::Algorithm _mcts_algorithm;
    };
}