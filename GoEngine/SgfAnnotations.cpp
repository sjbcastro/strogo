#include "SgfAnnotations.h"
#include "Utility.h"
#include <sstream>
#include <iomanip>

SgfMarkerAnnotation::SgfMarkerAnnotation(MarkerType type)
    : _marker_type(type)
{
}

SgfMarkerAnnotation::SgfMarkerAnnotation(MarkerType type, const std::list<GoLogic::Point>& points)
    : SgfBoardAnnotation(points)
    , _marker_type(type)
{
}

std::string SgfMarkerAnnotation::toSgfString() const
{
    if (_points.empty()) return "";

    std::string output;

    switch (_marker_type)
    {
    case MarkerType::CIRCLE:
    {
        output += "CR";
        break;
    }
    case MarkerType::SQUARE:
    {
        output += "SQ";
        break;
    }
    case MarkerType::TRIANGLE:
    {
        output += "TR";
        break;
    }
    case MarkerType::CROSS:
    {
        output += "MA";
        break;
    }
    default:
        return output;
    }

    for (auto it = _points.begin(); it != _points.end(); ++it)
    {
        output += "[" + GoLogic::Utility::PointAsSgfString(*it) + "]";
    }

    return output;
}

SgfDecimalLabelAnnotation::SgfDecimalLabelAnnotation(const std::list<std::pair<GoLogic::Point, double>>& pairs)
    : _point_label_pairs(pairs)
{
}

std::string SgfDecimalLabelAnnotation::toSgfString() const
{
    if (_point_label_pairs.empty()) return "";

    std::string output = "LB";

    for (auto it = _point_label_pairs.begin(); it != _point_label_pairs.end(); ++it)
    {
        output += "[" + GoLogic::Utility::PointAsSgfString(it->first) + ":";

        std::ostringstream oss;
        oss << std::setprecision(2) << it->second;

        output += oss.str() + "]";
    }

    return output;
}

SgfIntegerLabelAnnotation::SgfIntegerLabelAnnotation(const std::list<std::pair<GoLogic::Point, int>>& pairs)
    : _point_label_pairs(pairs)
{
}

std::string SgfIntegerLabelAnnotation::toSgfString() const
{
    if (_point_label_pairs.empty()) return "";

    std::string output = "LB";

    for (auto it = _point_label_pairs.begin(); it != _point_label_pairs.end(); ++it)
    {
        output += "[" + GoLogic::Utility::PointAsSgfString(it->first) + ":";
        output += std::to_string(it->second) + "]";
    }

    return output;
}

std::string SgfCommentAnnotation::toSgfString() const
{
    return "C[" + _comment + "]";
}
