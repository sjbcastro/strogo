#pragma once

#include "Board.h"
#include "LooseGroup.h"
#include "BouzyMap.h"
#include "GameState.h"
#include <vector>
#include <list>
#include <map>

class GameEngineUtils
{
public:
    static std::vector<GoLogic::Point> emptyPoints(const GoLogic::Board& board);
    static std::list<LooseGroup> looseGroups(const GoLogic::Board& board);

    template <typename T>
    static void updateLooseGroupsWithBouzyMap(std::list<LooseGroup>& loose_groups, const BouzyMap<T>& bouzy_map);

private:
    static bool areGroupsLooselyConnected(const GoLogic::ConnectedGroup& lhs, const GoLogic::ConnectedGroup& rhs);
    static bool areStonesLooselyConnected(const GoLogic::Stone& lhs, const GoLogic::Stone& rhs);
};

template <typename T>
void GameEngineUtils::updateLooseGroupsWithBouzyMap(std::list<LooseGroup>& loose_groups, const BouzyMap<T>& bouzy_map)
{
    size_t board_size = bouzy_map.boardMap().size();

    std::vector<std::vector<uint16_t>> region_array =
        std::vector<std::vector<uint16_t>>(board_size,std::vector<uint16_t>(board_size,0));

    std::map<uint16_t, std::list<std::pair<int16_t, int16_t>>> map_of_regions;
    uint16_t index = 0;

    // Loop over all points
    for ( size_t x = 0; x < region_array.size(); ++x )
    {
        for ( size_t y = 0; y < region_array[x].size(); ++y )
        {
            // Search for a POI that has not been dealt with yet
            if ( region_array[x][y] == 0 && bouzy_map.boardMap()[x][y] != 0 )
            {
                // Increment the index straight away for clarity's sake (we start from 1)
                ++index;

                T expected_bouzy_value = bouzy_map.boardMap()[x][y];

                map_of_regions[index].push_back(std::pair<int16_t, int16_t>(x,y));
                region_array[x][y] = index;

                while(true)
                {
                    bool added = false;

                    // Iteratively update region_array and map_of_regions with dilation of index
                    for (const auto& point : map_of_regions[index])
                    {
                        int16_t px = point.first;
                        int16_t py = point.second;

                        // Check px-1, py
                        --px;

                        if (px >= 0 && region_array[px][py] == 0 && bouzy_map.boardMap()[px][py] == expected_bouzy_value)
                        {
                            map_of_regions[index].push_back(std::pair<int16_t, int16_t>(px,py));
                            region_array[px][py] = index;
                            added = true;
                        }

                        // Check px, py-1
                        ++px;
                        --py;
                        if (py >= 0 && region_array[px][py] == 0 && bouzy_map.boardMap()[px][py] == expected_bouzy_value)
                        {
                            map_of_regions[index].push_back(std::pair<uint16_t, uint16_t>(px,py));
                            region_array[px][py] = index;
                            added = true;
                        }

                        // Check px+1, py
                        ++px;
                        ++py;
                        if (px < static_cast<int16_t>(board_size) && region_array[px][py] == 0 && bouzy_map.boardMap()[px][py] == expected_bouzy_value)
                        {
                            map_of_regions[index].push_back(std::pair<int16_t, int16_t>(px,py));
                            region_array[px][py] = index;
                            added = true;
                        }

                        // Check px, py+1
                        --px;
                        ++py;
                        if (py < static_cast<int16_t>(board_size) && region_array[px][py] == 0 && bouzy_map.boardMap()[px][py] == expected_bouzy_value)
                        {
                            map_of_regions[index].push_back(std::pair<int16_t, int16_t>(px,py));
                            region_array[px][py] = index;
                            added = true;
                        }
                    }
                    if (!added) break;
                }
            }
        }
    }

    // Loop over all loose groups
    for (LooseGroup& loose_group : loose_groups)
    {
        // Get the first stone of the first connected group
        auto s = loose_group.groups().begin()->stones().begin();

        uint16_t x = static_cast<uint16_t>(s->x()) - 1;
        uint16_t y = s->y() - 1;

        uint16_t current_index = region_array[x][y];

        loose_group.spacePoints(map_of_regions[current_index].size());
    }
}
