#pragma once

#include <string>
#include <utility>
#include "GtpCommandFactory.h"
#include "GameController.h"
#include "GoEngineListener.h"
#include "GameEngineProperties.h"

namespace ErrorMessages
{
    const std::string INVALID_KOMI = "Invalid komi, must be a real number";
    const std::string INVALID_BOARDSIZE = "Invalid boardsize, must be a number between 5 and 19";
}

namespace GoEngine
{
    class GtpInterface
    {
    public:
        GtpInterface(std::shared_ptr<GoEngineListener> engine_listener = nullptr);
        ~GtpInterface();

        std::string executeGtpCommand(const std::string& input);
        std::string removeWhitespace(const std::string& input);
        void extractCommandAndId(const std::string& input, std::string& id, std::string& command, std::vector<std::string>& args);
        bool handleGtpCommand(std::unique_ptr<GtpCommand> gtp_command, std::string& output_message);
        std::string formattedResponse(bool success, const std::string& id, const std::string& message) const;

        const GameController& gameController() const { return _game_controller; }

        bool quitRequested() const { return _quit_requested; }

        void setGameEngineProperties(const GameEngineProperties& properties) { _game_controller.setGameEngineProperties(properties); }

    private:

        std::string commandsAsStringList(const std::vector<std::string>& commands) const;

        GtpCommandFactory                   _gtp_command_factory;
        GameController                      _game_controller;
        std::shared_ptr<GoEngineListener>   _engine_listener;
        bool                                _quit_requested = false;
    };
}