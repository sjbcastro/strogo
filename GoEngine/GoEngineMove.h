#pragma once

#include "Stone.h"
#include "MCTSMove.h"

namespace GoEngine
{
	class Move : public MCTS::Move, public GoLogic::Move
	{
	public:

		using GoLogic::Move::Move;

		virtual std::string toString() const { return GoLogic::Move::asString(); }

		virtual bool isEqual(const MCTS::Move& input) const override
		{ 
			const GoLogic::Move& lhs = *this;
			const GoLogic::Move& rhs = dynamic_cast<const Move&>(input);
			return lhs == rhs;
		}

		bool operator<(const Move& rhs) const
		{
			return _stone < rhs._stone;
		}

	};
}
