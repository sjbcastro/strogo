#pragma once

#include <list>
#include "Stone.h"
#include <utility>

class SgfAnnotation
{
public:
    virtual std::string toSgfString() const = 0;
};

class SgfBoardAnnotation : public SgfAnnotation
{
public:

    SgfBoardAnnotation() = default;
    SgfBoardAnnotation(const std::list<GoLogic::Point> points) : _points(points) {}

protected:
    std::list<GoLogic::Point> _points;
};

class SgfMarkerAnnotation : public SgfBoardAnnotation
{
public:
    enum class MarkerType : int8_t
    {
        SQUARE,
        TRIANGLE,
        CIRCLE,
        CROSS
    };

    SgfMarkerAnnotation(MarkerType type);
    SgfMarkerAnnotation(MarkerType type, const std::list<GoLogic::Point>& points);

    virtual std::string toSgfString() const override;

private:
    MarkerType _marker_type;
};

class SgfDecimalLabelAnnotation : public SgfAnnotation
{
public:
    SgfDecimalLabelAnnotation(const std::list<std::pair<GoLogic::Point, double>>& pairs);

    virtual std::string toSgfString() const override;

private:
    std::list<std::pair<GoLogic::Point, double>> _point_label_pairs;
};

class SgfIntegerLabelAnnotation : public SgfAnnotation
{
public:
    SgfIntegerLabelAnnotation(const std::list<std::pair<GoLogic::Point, int>>& pairs);

    virtual std::string toSgfString() const override;

private:
    std::list<std::pair<GoLogic::Point, int>> _point_label_pairs;
};

class SgfCommentAnnotation : public SgfAnnotation
{
public:
    SgfCommentAnnotation(const std::string& str) : _comment(str) {}

    virtual std::string toSgfString() const override;

private:
    std::string _comment;
};
