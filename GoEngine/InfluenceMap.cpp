#include "InfluenceMap.h"

InfluenceMap::InfluenceMap(size_t board_size)
{
    _influence = std::vector<std::vector<double>>(board_size,std::vector<double>(board_size,0));
}

void InfluenceMap::assign(const std::vector<std::vector<double>>& map)
{
    _influence = map;
}
