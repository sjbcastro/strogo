#pragma once

#include "GameState.h"
#include "GameEngine.h"
#include "GoEngineListener.h"

namespace GoEngine
{
    class GameController
    {
    public:
        GameController(GoLogic::Ruleset r, GoLogic::SuperkoRuleset sk, std::shared_ptr<GoEngineListener> engine_listener = nullptr);

        bool makeMove(const GoLogic::Move& m);

        float komi() const { return _game_state.komi(); }
        void komi(float val) { _game_state.komi(val); }

        uint8_t boardsize() const { return _game_state.currentBoard().boardSize(); }
        bool boardsize(uint8_t new_size);

        void clearBoard();
        GoLogic::Move generateMove(GoLogic::StoneColor color, bool auto_add_stone);

        const GameState& gameState() const { return _game_state; }

        void generateDebugInfo();

        void setGameEngineProperties(const GameEngineProperties& properties) { _engine.setGameEngineProperties(properties); }

    private:
        GameState                           _game_state;
        GoLogic::Ruleset                    _ruleset;
        GoLogic::SuperkoRuleset             _superko_ruleset;
        GameEngine                          _engine;
        std::shared_ptr<GoEngineListener>   _engine_listener;
    };
}