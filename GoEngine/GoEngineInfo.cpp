#include "GoEngineInfo.h"

namespace GoEngine
{
    std::string GoEngineInfo::gtpProtocolVersion() const
    {
        std::ostringstream oss;
        oss << GTP_PROTOCOL_VERSION;
        return oss.str();
    }

    std::string GoEngineInfo::goEngineVersion() const
    {
        std::ostringstream oss;
        oss << GOENGINE_VERSION_MAJOR
            << "." << GOENGINE_VERSION_MINOR
            << "." << GOENGINE_VERSION_REVISION;
        return oss.str();
    }
}