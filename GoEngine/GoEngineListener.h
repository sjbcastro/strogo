#pragma once

#include "GameState.h"
#include <string>

namespace GoEngine
{
    class GoEngineListener
    {
    public:

        virtual void pushGameState(const GameState& game_state) = 0;
        virtual void receivedGtpResonse(const std::string& response) = 0;
    };
}