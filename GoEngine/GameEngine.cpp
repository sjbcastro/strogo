#include "GameEngine.h"
#include "MCTSMove.h"
#include <cassert>

namespace GoEngine
{
    GameEngine::GameEngine()
    {
        _mcts_algorithm.setThresholdForAbandoningSimulation(100);
    }
    GoLogic::Move GameEngine::generateMove(GoLogic::StoneColor colour, const GameState& current_state)
    {
        std::shared_ptr<GoLogic::Move> move;
        _move_generator.nextMoveColor(colour);
        _move_generator.setState(current_state);
        move = std::dynamic_pointer_cast<GoLogic::Move>(_mcts_algorithm.findOptimalMove(_move_generator, _properties.secondsPerMove() * 1000));

        assert(move);
        if (move)
        {
            return *move;
        }
        else
        {
            return GoLogic::Move(GoLogic::MoveType::RESIGN);
        }
    }

    void GameEngine::generateDebugInfo(GameState& current_state) const
    {
        current_state.logMetaData();
    }
}