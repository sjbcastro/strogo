#include "GtpInterface.h"
#include <locale>
#include "GoEngineInfo.h"
#include <algorithm>
#include <iostream>
#include "Utility.h"

// Going to avoid using this namespace liberally, but this file contains a lot of stuff from GoLogic, and there is minimal risk of naming conflicts here
using namespace GoLogic;

namespace GoEngine
{
    GtpInterface::GtpInterface(std::shared_ptr<GoEngineListener> engine_listener)
        : _game_controller(Ruleset::CHINESE, SuperkoRuleset::PSK, engine_listener)
        , _engine_listener(engine_listener)
    {
    }

    GtpInterface::~GtpInterface()
    {
    }

    std::string GtpInterface::executeGtpCommand(const std::string& input)
    {
        std::string id = "";
        std::string command = "";
        std::vector<std::string> args;

        extractCommandAndId(removeWhitespace(input), id, command, args);

        std::unique_ptr<GtpCommand> gtp_command = _gtp_command_factory.createGtpCommand(command, args);

        std::string message;
        bool result = handleGtpCommand(std::move(gtp_command), message);

        std::string response = formattedResponse(result, id, message);

        if (_engine_listener)
        {
            _engine_listener->receivedGtpResonse(response);
        }

        return response;
    }

    std::string GtpInterface::removeWhitespace(const std::string& input)
    {
        std::size_t pos = input.find_first_not_of(" \t\n");
        return input.substr(pos);
    }

    void GtpInterface::extractCommandAndId(const std::string& input, std::string& id, std::string& command, std::vector<std::string>& args)
    {
        size_t last_digit_loc = 0;
        while (std::isdigit(input[last_digit_loc]))
        {
            ++last_digit_loc;
        }

        std::size_t start_of_command = input.find_first_not_of(" \t\n", last_digit_loc);
        std::size_t next_space = input.find_first_of(" \t\n", start_of_command);

        id = input.substr(0, last_digit_loc);
        command = input.substr(start_of_command, next_space - start_of_command);

        while (next_space != std::string::npos)
        {
            std::size_t start = input.find_first_not_of(" \t\n", next_space);
            if (start == std::string::npos) break;

            next_space = input.find_first_of(" \t\n", start);
            args.push_back(input.substr(start, next_space - start));
        }
    }

    bool GtpInterface::handleGtpCommand(std::unique_ptr<GtpCommand> gtp_command, std::string& output_message)
    {
        const std::string& command_name = gtp_command->commandName();
        std::string successful_result = "= ";

        if (command_name == ProtocolVersionCommand::staticCommandName())
        {
            output_message = GoEngineInfo().gtpProtocolVersion();
            return true;
        }
        else if (command_name == NameCommand::staticCommandName())
        {
            output_message = GoEngineInfo().goEngineName();
            return true;
        }
        else if (command_name == EngineVersionCommand::staticCommandName())
        {
            output_message = GoEngineInfo().goEngineVersion();
            return true;
        }
        else if (command_name == PlayCommand::staticCommandName())
        {
            std::unique_ptr<PlayCommand> play_command(static_cast<PlayCommand*>(gtp_command.release()));
            if (_game_controller.makeMove(play_command->move()))
                return true;
            else
            {
                output_message = "Invalid colour or coordinate";
                return false;
            }
        }
        else if (command_name == KomiCommand::staticCommandName())
        {
            std::unique_ptr<KomiCommand> komi(static_cast<KomiCommand*>(gtp_command.release()));
            if (komi->successfullyParsed())
            {
                _game_controller.komi(komi->value());
                return true;
            }
            else
            {
                output_message = ErrorMessages::INVALID_KOMI;
                return false;
            }
        }
        else if (command_name == BoardsizeCommand::staticCommandName())
        {
            std::unique_ptr<BoardsizeCommand> boardsize(static_cast<BoardsizeCommand*>(gtp_command.release()));
            if (boardsize->successfullyParsed())
            {

                if (_game_controller.boardsize(boardsize->value()))
                    return true;
            }

            output_message = ErrorMessages::INVALID_BOARDSIZE;
            return false;
        }
        else if (command_name == ClearBoardCommand::staticCommandName())
        {
            _game_controller.clearBoard();
            return true;
        }
        else if (command_name == ListCommands::staticCommandName())
        {
            output_message = commandsAsStringList(_gtp_command_factory.commandList());
            return true;
        }
        else if (command_name == KnownCommand::staticCommandName())
        {
            std::unique_ptr<KnownCommand> known_command(static_cast<KnownCommand*>(gtp_command.release()));
            std::string command_to_find = known_command->commandToFind();
            std::vector<std::string> command_list = _gtp_command_factory.commandList();

            auto it = std::find(command_list.begin(), command_list.end(), command_to_find);

            if (it != command_list.end())
            {
                output_message = "true";
            }
            else
            {
                output_message = "false";
            }
            return true;
        }
        else if (command_name == GenmoveCommand::staticCommandName() || command_name == RegGenmoveCommand::staticCommandName())
        {
            std::unique_ptr<GenmoveCommand> genmove_command(static_cast<GenmoveCommand*>(gtp_command.release()));

            bool auto_add_stone = command_name == GenmoveCommand::staticCommandName();

            if (genmove_command->successfullyParsed())
            {
                auto m = _game_controller.generateMove(genmove_command->color(), auto_add_stone);
                output_message = Utility::MoveAsString(m);
                return true;
            }
            else
            {
                output_message = "invalid colour";
                return false;
            }
        }
        else if (command_name == SetFreeHandicapCommand::staticCommandName())
        {
            if (_game_controller.gameState().currentBoard().empty())
            {
                std::unique_ptr<SetFreeHandicapCommand>
                    set_free_handicap_command(static_cast<SetFreeHandicapCommand*>(gtp_command.release()));

                if (set_free_handicap_command->successfullyParsed())
                {
                    bool ok = true;
                    for (const auto& stone : set_free_handicap_command->handicapStones())
                    {
                        Move m(stone);
                        ok = ok && _game_controller.makeMove(m);
                    }

                    if (!ok)
                    {
                        output_message = "set_free_handicap failed: one or more vertexes may be invalid";
                    }

                    return ok;
                }
                else
                {
                    output_message = "invalid arguments";
                    return false;
                }
            }
            else
            {
                output_message = "cannot add handicap to a board that is not empty";
                return false;
            }
        }
        else if (command_name == SetFixedHandicapCommand::staticCommandName())
        {
            if (_game_controller.gameState().currentBoard().empty())
            {
                std::unique_ptr<SetFixedHandicapCommand>
                    set_fixed_handicap_command(static_cast<SetFixedHandicapCommand*>(gtp_command.release()));

                bool board_size_even = (_game_controller.boardsize() % 2 == 0);
                uint32_t max_handicap = board_size_even ? 4 : 9;
                uint32_t handicap_requested = set_fixed_handicap_command->numberOfHandicapStones();

                if (handicap_requested >= 2 && handicap_requested <= max_handicap)
                {
                    std::list<Stone> handicap_stones;
                    uint8_t lower_number = (_game_controller.boardsize() >= 13) ? 4 : 3;
                    uint8_t higher_number = _game_controller.boardsize() - lower_number + 1;
                    uint8_t middle_number = (_game_controller.boardsize() + 1) / 2;

                    PointLetter lower_letter = static_cast<PointLetter>(lower_number);
                    PointLetter middle_letter = static_cast<PointLetter>(middle_number);
                    PointLetter higher_letter = static_cast<PointLetter>(higher_number);

                    switch (handicap_requested)
                    {
                    case 9: handicap_stones.push_front(Stone(middle_letter, middle_number, StoneColor::BLACK));
                    case 8: handicap_stones.push_front(Stone(middle_letter, higher_number, StoneColor::BLACK));
                    case 7: handicap_stones.push_front(Stone(middle_letter, lower_number, StoneColor::BLACK));
                    case 6: handicap_stones.push_front(Stone(higher_letter, middle_number, StoneColor::BLACK));
                    case 5: handicap_stones.push_front(Stone(lower_letter, middle_number, StoneColor::BLACK));
                    case 4: handicap_stones.push_front(Stone(higher_letter, lower_number, StoneColor::BLACK));
                    case 3: handicap_stones.push_front(Stone(lower_letter, higher_number, StoneColor::BLACK));
                    case 2:
                    {
                        handicap_stones.push_front(Stone(higher_letter, higher_number, StoneColor::BLACK));
                        handicap_stones.push_front(Stone(lower_letter, lower_number, StoneColor::BLACK));
                    }
                    }

                    bool first = true;
                    for (const auto& stone : handicap_stones)
                    {
                        if (first)
                            first = false;
                        else
                            output_message += " ";
                        output_message += stone.asString();
                        Move m(stone);
                        _game_controller.makeMove(m);
                    }
                }
                else
                {
                    output_message = "invalid number of handicap stones requested";
                    return false;
                }
            }
            else
            {
                output_message = "cannot add handicap to a board that is not empty";
                return false;
            }
        }
        else if (command_name == QuitCommand::staticCommandName())
        {
            _quit_requested = true;
            return true;
        }
        else
        {
            output_message = "";
            return false;
        }
    }

    std::string GtpInterface::formattedResponse(bool success, const std::string& id, const std::string& message) const
    {
        std::string response = success ? "=" : "?";
        response += id;
        response += " ";
        response += message;
        response += "\n\n";
        return response;
    }

    std::string GtpInterface::commandsAsStringList(const std::vector<std::string>& commands) const
    {
        std::string response = "";
        for (size_t i = 0; i < commands.size(); ++i)
        {
            response += commands[i] + "\n";
        }

        return response;
    }
}