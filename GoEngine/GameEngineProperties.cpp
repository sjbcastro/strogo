#include "GameEngineProperties.h"

std::string ParseForStringValue(const std::string& arg, const std::string& prefix)
{
    auto loc = arg.find(prefix);
    if (loc != std::string::npos)
    {
        auto start = loc + prefix.size();
        return arg.substr(start, arg.size() - start);
    }
    else
    {
        return std::string();
    }
}

GameEngineProperties::GameEngineProperties(const std::vector<std::string>& command_args)
{
    for (const auto& arg : command_args)
    {
        parseForSecondsPerMove(arg);
    }
}

void GameEngineProperties::parseForSecondsPerMove(const std::string& arg)
{
    {
        std::string val = ParseForStringValue(arg, "--time=");
        if (!val.empty())
        {
            _seconds_per_move = std::stoul(val);
        }
    }
    {
        std::string val = ParseForStringValue(arg, "-t=");
        if (!val.empty())
        {
            _seconds_per_move = std::stoul(val);
        }
    }
}