#pragma once

#include <vector>
#include "Board.h"

enum class BouzyMapType : uint8_t
{
    BINARY
};

template <typename T>
class BouzyMap
{
public:

    BouzyMap(const GoLogic::Board& board, uint8_t n_dilations, uint8_t n_erosions, BouzyMapType type);
    BouzyMap(const std::vector<std::vector<T>>& board_state, uint8_t n_dilations, uint8_t n_erosions, BouzyMapType type);
    BouzyMap(const std::vector<std::vector<T>>& board_state, BouzyMapType type);

    const std::vector<std::vector<T>> boardMap() const { return _map; }

    void dilate();
    void erode();
    void restoreStones();

private:

    void initialise(const GoLogic::Board& board);

    BouzyMapType _type;
    std::vector<std::vector<T>> _map;
    std::vector<std::vector<T>> _original_map;
};

template <typename T>
BouzyMap<T>::BouzyMap(const GoLogic::Board& board, uint8_t n_dilations, uint8_t n_erosions, BouzyMapType type)
    : BouzyMap(board.boardState(), type)
{
    for (uint8_t n = 0; n < n_dilations; ++n)
    {
        dilate();
    }

    for (uint8_t n = 0; n < n_erosions; ++n)
    {
        erode();
    }
}

template <typename T>
BouzyMap<T>::BouzyMap(const std::vector<std::vector<T>>& board_state, BouzyMapType type)
    : _type(type)
    , _map(board_state)
    , _original_map(board_state)
{
}

template <typename T>
BouzyMap<T>::BouzyMap(const std::vector<std::vector<T>>& board_state, uint8_t n_dilations, uint8_t n_erosions, BouzyMapType type)
    : BouzyMap(board_state, type)
{
    for (uint8_t n = 0; n < n_dilations; ++n)
    {
        dilate();
    }

    for (uint8_t n = 0; n < n_erosions; ++n)
    {
        erode();
    }
}

template <typename T>
void BouzyMap<T>::dilate()
{
    for ( uint8_t x = 0; x < _map.size(); ++x)
    {
        for ( uint8_t y = 0; y < _map[x].size(); ++y)
        {
            if ( _map[x][y] == 0 )
            {
                if ( x > 0 && _map[x-1][y] == 1 ) _map[x][y] = 2;
                if ( y > 0 && _map[x][y-1] == 1 ) _map[x][y] = 2;
                if ( x < _map.size()-1 && _map[x+1][y] == 1 ) _map[x][y] = 2;
                if ( y < _map.size()-1 && _map[x][y+1] == 1 ) _map[x][y] = 2;

                if ( x > 0 && _map[x-1][y] == -1 ) _map[x][y] = -2;
                if ( y > 0 && _map[x][y-1] == -1 ) _map[x][y] = -2;
                if ( x < _map.size()-1 && _map[x+1][y] == -1 ) _map[x][y] = -2;
                if ( y < _map.size()-1 && _map[x][y+1] == -1 ) _map[x][y] = -2;
            }
        }
    }

    for ( uint8_t x = 0; x < _map.size(); ++x)
    {
        for ( uint8_t y = 0; y < _map[x].size(); ++y)
        {
            if ( _map[x][y] == 2 )
            {
                if ( x > 0 && _map[x-1][y] == -1 ) _map[x][y] = 0;
                if ( y > 0 && _map[x][y-1] == -1 ) _map[x][y] = 0;
                if ( x < _map.size()-1 && _map[x+1][y] == -1 ) _map[x][y] = 0;
                if ( y < _map.size()-1 && _map[x][y+1] == -1 ) _map[x][y] = 0;
            }
            if ( _map[x][y] == -2 )
            {
                if ( x > 0 && _map[x-1][y] == 1 ) _map[x][y] = 0;
                if ( y > 0 && _map[x][y-1] == 1 ) _map[x][y] = 0;
                if ( x < _map.size()-1 && _map[x+1][y] == 1 ) _map[x][y] = 0;
                if ( y < _map.size()-1 && _map[x][y+1] == 1 ) _map[x][y] = 0;
            }
        }
    }

    for ( uint8_t x = 0; x < _map.size(); ++x)
    {
        for ( uint8_t y = 0; y < _map[x].size(); ++y)
        {
            if ( _map[x][y] == 2 ) _map[x][y] = 1;
            if ( _map[x][y] == -2 ) _map[x][y] = -1;
        }
    }
}

template <typename T>
void BouzyMap<T>::erode()
{
    for ( uint8_t x = 0; x < _map.size(); ++x)
    {
        for ( uint8_t y = 0; y < _map[x].size(); ++y)
        {
            if ( _map[x][y] == 1 )
            {
                if ( x > 0 && _map[x-1][y] == 0 ) _map[x][y] = 2;
                if ( y > 0 && _map[x][y-1] == 0 ) _map[x][y] = 2;
                if ( x < _map.size()-1 && _map[x+1][y] == 0 ) _map[x][y] = 2;
                if ( y < _map.size()-1 && _map[x][y+1] == 0 ) _map[x][y] = 2;

                if ( x > 0 && _map[x-1][y] == -1 ) _map[x][y] = 2;
                if ( y > 0 && _map[x][y-1] == -1 ) _map[x][y] = 2;
                if ( x < _map.size()-1 && _map[x+1][y] == -1 ) _map[x][y] = 2;
                if ( y < _map.size()-1 && _map[x][y+1] == -1 ) _map[x][y] = 2;
            }

            if ( _map[x][y] == -1 )
            {
                if ( x > 0 && _map[x-1][y] == 0 ) _map[x][y] = -2;
                if ( y > 0 && _map[x][y-1] == 0 ) _map[x][y] = -2;
                if ( x < _map.size()-1 && _map[x+1][y] == 0 ) _map[x][y] = -2;
                if ( y < _map.size()-1 && _map[x][y+1] == 0 ) _map[x][y] = -2;

                if ( x > 0 && _map[x-1][y] == 1 ) _map[x][y] = -2;
                if ( y > 0 && _map[x][y-1] == 1 ) _map[x][y] = -2;
                if ( x < _map.size()-1 && _map[x+1][y] == 1 ) _map[x][y] = -2;
                if ( y < _map.size()-1 && _map[x][y+1] == 1 ) _map[x][y] = -2;
            }
        }
    }

    for ( uint8_t x = 0; x < _map.size(); ++x)
    {
        for ( uint8_t y = 0; y < _map[x].size(); ++y)
        {
            if ( _map[x][y] == 2 ) _map[x][y] = 0;
            if ( _map[x][y] == -2 ) _map[x][y] = 0;
        }
    }
}


template <typename T>
void BouzyMap<T>::restoreStones()
{
    assert(_map.size() == _original_map.size());

    for ( uint8_t x = 0; x < _map.size(); ++x)
    {
        assert(_map[x].size() == _original_map[x].size());
        for ( uint8_t y = 0; y < _map[x].size(); ++y)
        {
            if (_original_map[x][y] != 0 && _map[x][y] == 0)
            {
                _map[x][y] = _original_map[x][y];
            }
        }
    }
}
