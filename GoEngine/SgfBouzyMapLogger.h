#pragma once

#include "SgfLogger.h"
#include "Singleton.h"
#include "BouzyMap.h"

class SgfBouzyMapLoggerCore : public SgfLogger
{
public:
    void logBinaryBouzyMap(const BouzyMap<int8_t>& result);
};

class SgfBouzyMapLogger : public SgfBouzyMapLoggerCore, public Singleton<SgfBouzyMapLogger>
{
public:
    virtual std::string filename() const override { return "SgfBouzyMapLog.sgf"; }
};

