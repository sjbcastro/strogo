#pragma once

#include "Logger.h"
#include "Singleton.h"

class GoEngineLogger : public Logger, public Singleton<GoEngineLogger>
{
public:
    virtual std::string filename() const override { return "GoEngine.log"; }
};
