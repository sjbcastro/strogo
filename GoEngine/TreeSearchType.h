#pragma once

#include <inttypes.h>
#include <cassert>
#include <string>

enum class TreeSearchType : int16_t
{
    BRUTE_FORCE,
    ALPHA_BETA
};

std::string TreeSearchTypeToString(TreeSearchType val);

TreeSearchType TreeSearchTypeFromString(const std::string& str);