#include "SgfInfluenceLogger.h"
#include <iostream>

void SgfInfluenceLogger::logInfluence(InfluenceMap* result)
{
    if ( _enabled )
    {
        std::list<std::pair<GoLogic::Point, double>> points;

        for ( uint8_t x = 0; x < result->influence().size(); ++x)
        {
            for ( uint8_t y = 0; y < result->influence()[x].size(); ++y)
            {
                double value = result->influence()[x][y];
                if ( value >= 0.01 || value <= -0.01 )
                {
                    points.emplace_back(
                        std::pair<GoLogic::Point,double>(GoLogic::Point(GoLogic::PointLetter(x+1),y+1),
                        value));
                }
            }
        }

        std::unique_ptr<SgfAnnotation> annotation
            = std::unique_ptr<SgfDecimalLabelAnnotation>(new SgfDecimalLabelAnnotation(points));
        annotateLastMove(std::move(annotation));
    }
}

