#include "SgfBouzyMapLogger.h"

void SgfBouzyMapLoggerCore::logBinaryBouzyMap(const BouzyMap<int8_t>& result)
{
    if ( _enabled )
    {
        std::list<GoLogic::Point> black_points;
        std::list<GoLogic::Point> white_points;

        for ( uint8_t x = 0; x < result.boardMap().size(); ++x)
        {
            for ( uint8_t y = 0; y < result.boardMap()[x].size(); ++y)
            {
                if ( result.boardMap()[x][y] == 1 )
                    black_points.emplace_back(GoLogic::Point(GoLogic::PointLetter(x+1),y+1));
                else if ( result.boardMap()[x][y] == -1 )
                    white_points.emplace_back(GoLogic::Point(GoLogic::PointLetter(x+1),y+1));
            }
        }

        std::unique_ptr<SgfAnnotation> black_annotations
            = std::unique_ptr<SgfMarkerAnnotation>(new SgfMarkerAnnotation(
            SgfMarkerAnnotation::MarkerType::CROSS, black_points));
        std::unique_ptr<SgfAnnotation> white_annotations
            = std::unique_ptr<SgfMarkerAnnotation>(new SgfMarkerAnnotation(
            SgfMarkerAnnotation::MarkerType::CIRCLE, white_points));

        annotateLastMove(std::move(black_annotations));
        annotateLastMove(std::move(white_annotations));
    }
}
