#pragma once

#include "TreeSearchType.h"
#include <inttypes.h>
#include <string>
#include <vector>

class GameEngineProperties
{
public:
    GameEngineProperties() = default;
    GameEngineProperties(const std::vector<std::string>& command_args);

    uint32_t secondsPerMove() const { return _seconds_per_move; }
    void secondsPerMove(uint32_t val) { _seconds_per_move = val; }

private:
    
    void parseForSecondsPerMove(const std::string& arg);
    
    // How long we get per move
    uint32_t _seconds_per_move = 15;
};
