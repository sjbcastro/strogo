#include "GoEngineInfo.h"
#include "catch.hpp"
#include <iostream>

TEST_CASE("GTP protocol version", "[Gtp, Version]")
{
    CHECK(GoEngine::GoEngineInfo().gtpProtocolVersion() == "2");
}

TEST_CASE("Engine version", "[Engine, Version]")
{
    CHECK(GoEngine::GoEngineInfo().goEngineVersion() == "0.1.0");
}

TEST_CASE("Engine name", "[Engine, Name]")
{
    CHECK(GoEngine::GoEngineInfo().goEngineName() == "StroGo");
}
