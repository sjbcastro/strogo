#include "BouzyMap.h"
#include "catch.hpp"
#include <iostream>

using namespace GoLogic;

TEST_CASE("BouzyMap empty board", "[BouzyMap]")
{
    Board board(19);
    BouzyMap<int8_t> bouzy_map(board, 1, 1, BouzyMapType::BINARY);

    int s = bouzy_map.boardMap().size();
    CHECK(s == 19);
    for ( int x = 0; x < s; ++x )
    {
        for ( int y = 0; y < s; ++y )
        {
            CHECK(bouzy_map.boardMap()[x][y] == 0);
        }
    }
}

TEST_CASE("BouzyMap one stone of each", "[BouzyMap]")
{
    Board board(19);
    board.addStone(Stone(PointLetter::A, 1, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::D, 1, StoneColor::WHITE));
    BouzyMap<int8_t> bouzy_map(board, 0, 0, BouzyMapType::BINARY);

    int s = bouzy_map.boardMap().size();
    CHECK(s == 19);
    for ( int x = 0; x < s; ++x )
    {
        for ( int y = 0; y < s; ++y )
        {
            if ( x == 0 && y == 0)
                CHECK(bouzy_map.boardMap()[x][y] == 1);
            else if ( x == 3 && y == 0)
                CHECK(bouzy_map.boardMap()[x][y] == -1);
            else
                CHECK(bouzy_map.boardMap()[x][y] == 0);
        }
    }
}

TEST_CASE("BouzyMap one dilation", "[BouzyMap]")
{
    Board board(19);
    board.addStone(Stone(PointLetter::A, 1, StoneColor::BLACK));
    BouzyMap<int8_t> bouzy_map(board, 1, 0, BouzyMapType::BINARY);

    int s = bouzy_map.boardMap().size();
    CHECK(s == 19);
    for ( int x = 0; x < s; ++x )
    {
        for ( int y = 0; y < s; ++y )
        {
            if ( x + y <= 1)
                CHECK(bouzy_map.boardMap()[x][y] == 1);
            else
                CHECK(bouzy_map.boardMap()[x][y] == 0);
        }
    }
}

TEST_CASE("BouzyMap three dilations one erosion", "[BouzyMap]")
{
    Board board(19);
    board.addStone(Stone(PointLetter::B, 2, StoneColor::BLACK));
    BouzyMap<int8_t> bouzy_map(board, 3, 1, BouzyMapType::BINARY);

    int s = bouzy_map.boardMap().size();
    CHECK(s == 19);
    for ( int x = 0; x < s; ++x )
    {
        for ( int y = 0; y < s; ++y )
        {
            if ( (x <= 2 && y <= 2) || x == 1 && y == 3 || x == 3 && y == 1 )
                CHECK(bouzy_map.boardMap()[x][y] == 1);
            else
                CHECK(bouzy_map.boardMap()[x][y] == 0);
        }
    }
}

TEST_CASE("BouzyMap diagonal enemy stones", "[BouzyMap]")
{
    Board board(19);
    board.addStone(Stone(PointLetter::B, 2, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::C, 3, StoneColor::WHITE));

    BouzyMap<int8_t> bouzy_map(board, 2, 0, BouzyMapType::BINARY);

    int s = bouzy_map.boardMap().size();
    CHECK(s == 19);
    for ( int x = 0; x < s; ++x )
    {
        for ( int y = 0; y < s; ++y )
        {
            if ( (x <= 1 && y <= 1) || x == 0 && y == 2 || x == 2 && y == 0 )
                CHECK(bouzy_map.boardMap()[x][y] == 1);
            else if ( x == 1 && y == 3
                || x == 2 && y == 2
                || x == 3 && y == 1
                || x == 2 && y == 4
                || x == 3 && y == 3
                || x == 4 && y == 2
                || x == 2 && y == 3
                || x == 3 && y == 2 )
                CHECK(bouzy_map.boardMap()[x][y] == -1);
            else
                CHECK(bouzy_map.boardMap()[x][y] == 0);
        }
    }
}
