#include <catch.hpp>
#include "Utility.h"
#include <iostream>

using namespace GoLogic;

TEST_CASE("Test PointLetter to string conversion", "[Utility, PointLetter]")
{
    CHECK(Utility::PointLetterAsString(PointLetter::A) == "A");
    CHECK(Utility::PointLetterAsString(PointLetter::B) == "B");
    CHECK(Utility::PointLetterAsString(PointLetter::C) == "C");
    CHECK(Utility::PointLetterAsString(PointLetter::D) == "D");
    CHECK(Utility::PointLetterAsString(PointLetter::E) == "E");
    CHECK(Utility::PointLetterAsString(PointLetter::F) == "F");
    CHECK(Utility::PointLetterAsString(PointLetter::G) == "G");
    CHECK(Utility::PointLetterAsString(PointLetter::H) == "H");
    CHECK(Utility::PointLetterAsString(PointLetter::J) == "J");
    CHECK(Utility::PointLetterAsString(PointLetter::K) == "K");
    CHECK(Utility::PointLetterAsString(PointLetter::L) == "L");
    CHECK(Utility::PointLetterAsString(PointLetter::M) == "M");
    CHECK(Utility::PointLetterAsString(PointLetter::N) == "N");
    CHECK(Utility::PointLetterAsString(PointLetter::O) == "O");
    CHECK(Utility::PointLetterAsString(PointLetter::P) == "P");
    CHECK(Utility::PointLetterAsString(PointLetter::Q) == "Q");
    CHECK(Utility::PointLetterAsString(PointLetter::R) == "R");
    CHECK(Utility::PointLetterAsString(PointLetter::S) == "S");
    CHECK(Utility::PointLetterAsString(PointLetter::T) == "T");
}

TEST_CASE("Test PointLetter from string conversion", "[Utility, PointLetter]")
{
    CHECK(Utility::PointLetterFromString("A") == PointLetter::A);
    CHECK(Utility::PointLetterFromString("B") == PointLetter::B);
    CHECK(Utility::PointLetterFromString("C") == PointLetter::C);
    CHECK(Utility::PointLetterFromString("D") == PointLetter::D);
    CHECK(Utility::PointLetterFromString("E") == PointLetter::E);
    CHECK(Utility::PointLetterFromString("F") == PointLetter::F);
    CHECK(Utility::PointLetterFromString("G") == PointLetter::G);
    CHECK(Utility::PointLetterFromString("H") == PointLetter::H);
    CHECK(Utility::PointLetterFromString("J") == PointLetter::J);
    CHECK(Utility::PointLetterFromString("K") == PointLetter::K);
    CHECK(Utility::PointLetterFromString("L") == PointLetter::L);
    CHECK(Utility::PointLetterFromString("M") == PointLetter::M);
    CHECK(Utility::PointLetterFromString("N") == PointLetter::N);
    CHECK(Utility::PointLetterFromString("O") == PointLetter::O);
    CHECK(Utility::PointLetterFromString("P") == PointLetter::P);
    CHECK(Utility::PointLetterFromString("Q") == PointLetter::Q);
    CHECK(Utility::PointLetterFromString("R") == PointLetter::R);
    CHECK(Utility::PointLetterFromString("S") == PointLetter::S);
    CHECK(Utility::PointLetterFromString("T") == PointLetter::T);
}

TEST_CASE("Stones are adjacent", "[Utility, Adjacent, Stone]")
{
    CHECK(Utility::areStonesAdjacent(Stone(PointLetter::A, 1, StoneColor::BLACK),
        Stone(PointLetter::A, 2, StoneColor::BLACK)));
    CHECK(Utility::areStonesAdjacent(Stone(PointLetter::A, 1, StoneColor::BLACK),
        Stone(PointLetter::B, 1, StoneColor::BLACK)));
    CHECK(Utility::areStonesAdjacent(Stone(PointLetter::J, 5, StoneColor::BLACK),
        Stone(PointLetter::H, 5, StoneColor::BLACK)));
    CHECK(Utility::areStonesAdjacent(Stone(PointLetter::J, 5, StoneColor::BLACK),
        Stone(PointLetter::K, 5, StoneColor::BLACK)));
    CHECK(Utility::areStonesAdjacent(Stone(PointLetter::J, 5, StoneColor::BLACK),
        Stone(PointLetter::J, 4, StoneColor::BLACK)));
    CHECK(Utility::areStonesAdjacent(Stone(PointLetter::J, 5, StoneColor::BLACK),
        Stone(PointLetter::J, 6, StoneColor::BLACK)));
}

TEST_CASE("Stones are not adjacent", "[Utility, Adjacent, Stone]")
{
    CHECK(!Utility::areStonesAdjacent(Stone(PointLetter::A, 1, StoneColor::BLACK),
        Stone(PointLetter::A, 1, StoneColor::BLACK)));
    CHECK(!Utility::areStonesAdjacent(Stone(PointLetter::A, 1, StoneColor::BLACK),
        Stone(PointLetter::B, 2, StoneColor::BLACK)));
    CHECK(!Utility::areStonesAdjacent(Stone(PointLetter::J, 5, StoneColor::BLACK),
        Stone(PointLetter::H, 4, StoneColor::BLACK)));
    CHECK(!Utility::areStonesAdjacent(Stone(PointLetter::J, 5, StoneColor::BLACK),
        Stone(PointLetter::K, 6, StoneColor::BLACK)));
    CHECK(!Utility::areStonesAdjacent(Stone(PointLetter::J, 5, StoneColor::BLACK),
        Stone(PointLetter::J, 5, StoneColor::BLACK)));
    CHECK(!Utility::areStonesAdjacent(Stone(PointLetter::J, 5, StoneColor::BLACK),
        Stone(PointLetter::D, 6, StoneColor::BLACK)));
}
