#include "Board.h"
#include "catch.hpp"
#include <algorithm>
#include <iostream>

using namespace GoLogic;

bool CheckNoStonesExistOfTargetColour(const Board& board, StoneColor target_colour)
{
    const auto& state = board.boardState();
    for (const auto& row : state)
    {
        for (const auto& point : row)
        {
            if (point == static_cast<int8_t>(target_colour)) return false;
        }
    }

    return true;
}

TEST_CASE("Board constructor", "[Board, Constructor]")
{
    for ( uint16_t i = 0; i < 25; ++i )
    {
        Board board(i);
        if ( i < 5 || i > 19 )
        {
            CHECK(board.boardSize() == 0);
        }
        else
        {
            CHECK(board.boardSize() == i);
        }
    }
}

TEST_CASE("Illegal move if beyond board size", "[Board, Illegal, Move]")
{
    {
        Board board(7);
        CHECK(!board.isMoveLegalWithoutKo(Stone(PointLetter::A, 8, StoneColor::BLACK)));
        CHECK(!board.isMoveLegalWithoutKo(Stone(PointLetter::H, 1, StoneColor::BLACK)));
        CHECK(!board.isMoveLegalWithoutKo(Stone(PointLetter::A, 0, StoneColor::BLACK)));
    }
    {
        Board board(18);
        CHECK(!board.isMoveLegalWithoutKo(Stone(PointLetter::A, 19, StoneColor::BLACK)));
        CHECK(!board.isMoveLegalWithoutKo(Stone(PointLetter::T, 1, StoneColor::BLACK)));
        CHECK(!board.isMoveLegalWithoutKo(Stone(PointLetter::A, 0, StoneColor::BLACK)));
    }
}

TEST_CASE("Illegal move if point occupied", "[Board, Illegal, Move]")
{
    Board board(19);
    CHECK(board.addStone(Stone(PointLetter::J, 4, StoneColor::WHITE)));
    CHECK(!board.addStone(Stone(PointLetter::J, 4, StoneColor::WHITE)));
    CHECK(!board.addStone(Stone(PointLetter::J, 4, StoneColor::BLACK)));

    CHECK(board.addStone(Stone(PointLetter::A, 4, StoneColor::WHITE)));
    CHECK(!board.addStone(Stone(PointLetter::A, 4, StoneColor::WHITE)));
    CHECK(!board.addStone(Stone(PointLetter::A, 4, StoneColor::BLACK)));
}

TEST_CASE("Corner capture white stone", "[Board, ConnectedGroup, Liberties, Capture, addStone]")
{
    Board board(19);

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 0);

    board.addStone(Stone(PointLetter::A, 2, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::A, 1, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::B, 1, StoneColor::BLACK));

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 1);

    // No white stones on board
    CheckNoStonesExistOfTargetColour(board, StoneColor::WHITE);
}

TEST_CASE("Corner capture black stones", "[Board, ConnectedGroup, Liberties, Capture, addStone]")
{
    Board board(19);

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 0);

    board.addStone(Stone(PointLetter::A, 18, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::A, 19, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::B, 18, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::B, 19, StoneColor::BLACK));  
    board.addStone(Stone(PointLetter::C, 19, StoneColor::WHITE));

    CHECK(board.blackCapturedStones() == 2);
    CHECK(board.whiteCapturedStones() == 0);

    // No black stones on board
    CheckNoStonesExistOfTargetColour(board, StoneColor::BLACK);
}

TEST_CASE("Side capture white stone", "[Board, ConnectedGroup, Liberties, Capture, addStone]")
{
    Board board(19);

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 0);

    board.addStone(Stone(PointLetter::J, 2, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::J, 1, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::H, 1, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::K, 1, StoneColor::BLACK));

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 1);

    // No white stones on board
    CheckNoStonesExistOfTargetColour(board, StoneColor::WHITE);
}

TEST_CASE("Side capture black stones", "[Board, ConnectedGroup, Liberties, Capture, addStone]")
{
    Board board(19);

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 0);

    board.addStone(Stone(PointLetter::S, 9, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::T, 9, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::S, 10, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::T, 10, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::T, 8, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::T, 11, StoneColor::WHITE));

    CHECK(board.blackCapturedStones() == 2);
    CHECK(board.whiteCapturedStones() == 0);

    // No black stones on board
    CheckNoStonesExistOfTargetColour(board, StoneColor::BLACK);
}

TEST_CASE("Middle capture white stone", "[Board, ConnectedGroup, Liberties, Capture, addStone]")
{
    Board board(19);

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 0);

    board.addStone(Stone(PointLetter::J, 5, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::J, 4, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::J, 3, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::H, 4, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::K, 4, StoneColor::BLACK));

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 1);

    // No white stones on board
    CheckNoStonesExistOfTargetColour(board, StoneColor::WHITE);
}

TEST_CASE("Middle capture black stones", "[Board, ConnectedGroup, Liberties, Capture, addStone]")
{
    Board board(19);

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 0);

    board.addStone(Stone(PointLetter::M, 9, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::M, 10, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::N, 10, StoneColor::BLACK));

    board.addStone(Stone(PointLetter::O, 10, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::N, 9, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::N, 11, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::M, 8, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::M, 11, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::L, 9, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::L, 10, StoneColor::WHITE));

    CHECK(board.blackCapturedStones() == 3);
    CHECK(board.whiteCapturedStones() == 0);

    // No black stones on board
    CheckNoStonesExistOfTargetColour(board, StoneColor::BLACK);
}

TEST_CASE("Cumulative capture white stones", "[Board, ConnectedGroup, Liberties, Capture, addStone]")
{
    Board board(19);

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 0);

    board.addStone(Stone(PointLetter::Q, 4, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::Q, 3, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::P, 3, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::R, 2, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::Q, 2, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::S, 1, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::R, 3, StoneColor::BLACK));

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 1);

    board.addStone(Stone(PointLetter::T, 1, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::S, 2, StoneColor::BLACK));

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 1);

    board.addStone(Stone(PointLetter::R, 1, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::T, 2, StoneColor::BLACK));

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 1);

    board.addStone(Stone(PointLetter::Q, 1, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::P, 1, StoneColor::BLACK));

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 6);

    // No white stones on board
    CheckNoStonesExistOfTargetColour(board, StoneColor::WHITE);
}

TEST_CASE("Cumulative capture black stones", "[Board, ConnectedGroup, Liberties, Capture, addStone]")
{
    Board board(19);

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 0);

    board.addStone(Stone(PointLetter::A, 1, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::B, 2, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::A, 2, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::A, 3, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::C, 1, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::B, 1, StoneColor::WHITE));

    CHECK(board.blackCapturedStones() == 2);
    CHECK(board.whiteCapturedStones() == 0);

    board.addStone(Stone(PointLetter::D, 1, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::C, 2, StoneColor::WHITE));

    CHECK(board.blackCapturedStones() == 3);
    CHECK(board.whiteCapturedStones() == 0);
}

TEST_CASE("Cumulative capture black and white stones", "[Board, ConnectedGroup, Liberties, Capture, addStone]")
{
    Board board(19);

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 0);

    board.addStone(Stone(PointLetter::Q, 16, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::D, 16, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::D, 17, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::D, 15, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::C, 16, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::E, 16, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::Q, 15, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::Q, 17, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::P, 16, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::R, 16, StoneColor::WHITE));

    CHECK(board.blackCapturedStones() == 1);
    CHECK(board.whiteCapturedStones() == 1);
}

TEST_CASE("Double capture and suicide is legal", "[Board, ConnectedGroup, Suicide, Liberties, Capture, addStone]")
{
    Board board(19);

    board.addStone(Stone(PointLetter::A, 3, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::B, 2, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::C, 1, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::A, 2, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::B, 1, StoneColor::WHITE));

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 0);

    // Suicide: move should be legal since it is a capture
    CHECK(board.addStone(Stone(PointLetter::A, 1, StoneColor::BLACK)));

    CHECK(board.blackCapturedStones() == 0);
    CHECK(board.whiteCapturedStones() == 2);

    // No white stones on board
    CheckNoStonesExistOfTargetColour(board, StoneColor::WHITE);
}

TEST_CASE("Illegal move if suicide", "[Board, Illegal, Move, Suicide]")
{
    Board board(19);
    CHECK(board.addStone(Stone(PointLetter::H, 4, StoneColor::WHITE)));
    CHECK(board.addStone(Stone(PointLetter::K, 4, StoneColor::WHITE)));
    CHECK(board.addStone(Stone(PointLetter::J, 3, StoneColor::WHITE)));
    CHECK(board.addStone(Stone(PointLetter::J, 5, StoneColor::WHITE)));

    // Middle of a ponuki
    CHECK(!board.addStone(Stone(PointLetter::J, 4, StoneColor::BLACK)));
}

TEST_CASE("Board positions equivalent despite different move order", "[Board, areBoardPositionsEquivalent]")
{
    Board board1(19);
    Board board2(19);

    board1.addStone(Stone(PointLetter::H, 4, StoneColor::WHITE));
    board1.addStone(Stone(PointLetter::H, 5, StoneColor::WHITE));
    board1.addStone(Stone(PointLetter::B, 5, StoneColor::BLACK));

    board2.addStone(Stone(PointLetter::B, 5, StoneColor::BLACK));
    board2.addStone(Stone(PointLetter::H, 5, StoneColor::WHITE));
    board2.addStone(Stone(PointLetter::H, 4, StoneColor::WHITE));

    CHECK(board1.areBoardPositionsEquivalent(board2));
}

TEST_CASE("Zobrist hash values", "[Board, areBoardPositionsEquivalent]")
{
    Board board1(19);
    Board board2(19);
    Board board3(19);

    board1.addStone(Stone(PointLetter::A, 1, StoneColor::BLACK));
    board2.addStone(Stone(PointLetter::A, 2, StoneColor::WHITE));
    board3.addStone(Stone(PointLetter::B, 1, StoneColor::WHITE));
    
    CHECK(!board1.areBoardPositionsEquivalent(board2));
    CHECK(!board1.areBoardPositionsEquivalent(board3));
    CHECK(!board2.areBoardPositionsEquivalent(board3));
    
    board1.addStone(Stone(PointLetter::A, 2, StoneColor::WHITE));
    board2.addStone(Stone(PointLetter::B, 1, StoneColor::WHITE));
    board3.addStone(Stone(PointLetter::A, 2, StoneColor::WHITE));
    
    CHECK(!board1.areBoardPositionsEquivalent(board2));
    CHECK(board2.areBoardPositionsEquivalent(board3));
    
    board1.addStone(Stone(PointLetter::B, 2, StoneColor::BLACK));
    board2.addStone(Stone(PointLetter::B, 2, StoneColor::BLACK));
    board3.addStone(Stone(PointLetter::B, 2, StoneColor::BLACK));
    
    CHECK(!board1.areBoardPositionsEquivalent(board2));
    CHECK(board2.areBoardPositionsEquivalent(board3));
    
    board1.addStone(Stone(PointLetter::B, 1, StoneColor::WHITE));

    CHECK(board1.areBoardPositionsEquivalent(board2));
    CHECK(board2.areBoardPositionsEquivalent(board3));
}
