#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW-Windows
CND_DLIB_EXT=dll
CND_CONF=WinRelease
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/BoardTests.o \
	${OBJECTDIR}/BouzyMapTests.o \
	${OBJECTDIR}/CatchDeclaration.o \
	${OBJECTDIR}/ConnectedGroupTests.o \
	${OBJECTDIR}/GameEngineUtilsTests.o \
	${OBJECTDIR}/GameStateTests.o \
	${OBJECTDIR}/GoEngineInfoTests.o \
	${OBJECTDIR}/GtpCommandsTests.o \
	${OBJECTDIR}/GtpInterfaceTests.o \
	${OBJECTDIR}/UtilityTests.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../GoBoardLogic/dist/WinRelease/MinGW-Windows/libgoboardlogic.a ../GoEngine/dist/WinRelease/MinGW-Windows/libgoengine.a ../GtpUtility/dist/WinRelease/MinGW-Windows/libgtputility.a ../Core/dist/WinRelease/MinGW-Windows/libcore.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/unittests.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/unittests.exe: ../GoBoardLogic/dist/WinRelease/MinGW-Windows/libgoboardlogic.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/unittests.exe: ../GoEngine/dist/WinRelease/MinGW-Windows/libgoengine.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/unittests.exe: ../GtpUtility/dist/WinRelease/MinGW-Windows/libgtputility.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/unittests.exe: ../Core/dist/WinRelease/MinGW-Windows/libcore.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/unittests.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/unittests ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/BoardTests.o: BoardTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BoardTests.o BoardTests.cpp

${OBJECTDIR}/BouzyMapTests.o: BouzyMapTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BouzyMapTests.o BouzyMapTests.cpp

${OBJECTDIR}/CatchDeclaration.o: CatchDeclaration.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CatchDeclaration.o CatchDeclaration.cpp

${OBJECTDIR}/ConnectedGroupTests.o: ConnectedGroupTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ConnectedGroupTests.o ConnectedGroupTests.cpp

${OBJECTDIR}/GameEngineUtilsTests.o: GameEngineUtilsTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GameEngineUtilsTests.o GameEngineUtilsTests.cpp

${OBJECTDIR}/GameStateTests.o: GameStateTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GameStateTests.o GameStateTests.cpp

${OBJECTDIR}/GoEngineInfoTests.o: GoEngineInfoTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GoEngineInfoTests.o GoEngineInfoTests.cpp

${OBJECTDIR}/GtpCommandsTests.o: GtpCommandsTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GtpCommandsTests.o GtpCommandsTests.cpp

${OBJECTDIR}/GtpInterfaceTests.o: GtpInterfaceTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GtpInterfaceTests.o GtpInterfaceTests.cpp

${OBJECTDIR}/UtilityTests.o: UtilityTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/UtilityTests.o UtilityTests.cpp

# Subprojects
.build-subprojects:
	cd ../GoBoardLogic && ${MAKE}  -f Makefile CONF=WinRelease
	cd ../GoEngine && ${MAKE}  -f Makefile CONF=WinRelease
	cd ../GtpUtility && ${MAKE}  -f Makefile CONF=WinRelease
	cd ../Core && ${MAKE}  -f Makefile CONF=WinRelease

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:
	cd ../GoBoardLogic && ${MAKE}  -f Makefile CONF=WinRelease clean
	cd ../GoEngine && ${MAKE}  -f Makefile CONF=WinRelease clean
	cd ../GtpUtility && ${MAKE}  -f Makefile CONF=WinRelease clean
	cd ../Core && ${MAKE}  -f Makefile CONF=WinRelease clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
