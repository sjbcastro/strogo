#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/BoardTests.o \
	${OBJECTDIR}/BouzyMapTests.o \
	${OBJECTDIR}/CatchDeclaration.o \
	${OBJECTDIR}/ConnectedGroupTests.o \
	${OBJECTDIR}/GameEngineUtilsTests.o \
	${OBJECTDIR}/GameStateTests.o \
	${OBJECTDIR}/GoEngineInfoTests.o \
	${OBJECTDIR}/GtpCommandsTests.o \
	${OBJECTDIR}/GtpInterfaceTests.o \
	${OBJECTDIR}/UtilityTests.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../GoBoardLogic/dist/Debug/GNU-Linux/libgoboardlogic.a ../GoEngine/dist/Debug/GNU-Linux/libgoengine.a ../GtpUtility/dist/Debug/GNU-Linux/libgtputility.a ../Core/dist/Debug/GNU-Linux/libcore.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/unittests

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/unittests: ../GoBoardLogic/dist/Debug/GNU-Linux/libgoboardlogic.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/unittests: ../GoEngine/dist/Debug/GNU-Linux/libgoengine.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/unittests: ../GtpUtility/dist/Debug/GNU-Linux/libgtputility.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/unittests: ../Core/dist/Debug/GNU-Linux/libcore.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/unittests: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/unittests ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/BoardTests.o: BoardTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -I../Core -I../Thirdparty/Json -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BoardTests.o BoardTests.cpp

${OBJECTDIR}/BouzyMapTests.o: BouzyMapTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -I../Core -I../Thirdparty/Json -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BouzyMapTests.o BouzyMapTests.cpp

${OBJECTDIR}/CatchDeclaration.o: CatchDeclaration.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -I../Core -I../Thirdparty/Json -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CatchDeclaration.o CatchDeclaration.cpp

${OBJECTDIR}/ConnectedGroupTests.o: ConnectedGroupTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -I../Core -I../Thirdparty/Json -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ConnectedGroupTests.o ConnectedGroupTests.cpp

${OBJECTDIR}/GameEngineUtilsTests.o: GameEngineUtilsTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -I../Core -I../Thirdparty/Json -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GameEngineUtilsTests.o GameEngineUtilsTests.cpp

${OBJECTDIR}/GameStateTests.o: GameStateTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -I../Core -I../Thirdparty/Json -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GameStateTests.o GameStateTests.cpp

${OBJECTDIR}/GoEngineInfoTests.o: GoEngineInfoTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -I../Core -I../Thirdparty/Json -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GoEngineInfoTests.o GoEngineInfoTests.cpp

${OBJECTDIR}/GtpCommandsTests.o: GtpCommandsTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -I../Core -I../Thirdparty/Json -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GtpCommandsTests.o GtpCommandsTests.cpp

${OBJECTDIR}/GtpInterfaceTests.o: GtpInterfaceTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -I../Core -I../Thirdparty/Json -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GtpInterfaceTests.o GtpInterfaceTests.cpp

${OBJECTDIR}/UtilityTests.o: UtilityTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GoBoardLogic -I../GoEngine -I../GtpUtility -I../Thirdparty/Catch/Catch-master/include -I../Core -I../Thirdparty/Json -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/UtilityTests.o UtilityTests.cpp

# Subprojects
.build-subprojects:
	cd ../GoBoardLogic && ${MAKE}  -f Makefile CONF=Debug
	cd ../GoEngine && ${MAKE}  -f Makefile CONF=Debug
	cd ../GtpUtility && ${MAKE}  -f Makefile CONF=Debug
	cd ../Core && ${MAKE}  -f Makefile CONF=Debug

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:
	cd ../GoBoardLogic && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../GoEngine && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../GtpUtility && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../Core && ${MAKE}  -f Makefile CONF=Debug clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
