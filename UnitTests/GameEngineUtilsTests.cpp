#include <catch.hpp>
#include "GameEngineUtils.h"

using namespace GoLogic;

TEST_CASE("Loose groups #1", "[LooseGroup]")
{
    Board board(19);
    board.addStone(Stone(PointLetter::D, 16, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::D, 14, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::F, 16, StoneColor::BLACK));

    std::list<LooseGroup> loose_groups = GameEngineUtils::looseGroups(board);

    CHECK(loose_groups.size() == 1);
    CHECK(loose_groups.front().groups().size() == 3);
}

TEST_CASE("Loose groups #2", "[LooseGroup]")
{
    Board board(19);
    board.addStone(Stone(PointLetter::D, 16, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::D, 14, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::F, 16, StoneColor::BLACK));

    std::list<LooseGroup> loose_groups = GameEngineUtils::looseGroups(board);

    CHECK(loose_groups.size() == 3);
}

TEST_CASE("Loose groups #3", "[LooseGroup]")
{
    Board board(19);
    board.addStone(Stone(PointLetter::A, 19, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::A, 18, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::C, 18, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::D, 17, StoneColor::BLACK));
    board.addStone(Stone(PointLetter::D, 15, StoneColor::BLACK));

    std::list<LooseGroup> loose_groups = GameEngineUtils::looseGroups(board);

    CHECK(loose_groups.size() == 1);
    CHECK(loose_groups.front().groups().size() == 4);
}

TEST_CASE("Loose groups #4", "[LooseGroup]")
{
    Board board(19);
    board.addStone(Stone(PointLetter::D, 16, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::D, 12, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::D, 10, StoneColor::WHITE));
    board.addStone(Stone(PointLetter::D, 14, StoneColor::WHITE));

    std::list<LooseGroup> loose_groups = GameEngineUtils::looseGroups(board);

    CHECK(loose_groups.size() == 1);
    CHECK(loose_groups.front().groups().size() == 4);
}

