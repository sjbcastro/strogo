#include "Board.h"
#include "GameState.h"
#include "GoGameScorer.h"
#include "SgfReader.h"
#include "GameController.h"
#include "catch.hpp"

using namespace GoLogic;

Board ReadSGF(const std::string& filename)
{
    SgfReader reader;
    SimpleSgfRecord record = reader.readSimpleSgfFile(filename);

    GoEngine::GameController controller(Ruleset::CHINESE, SuperkoRuleset::PSK);
    controller.boardsize(record.boardSize());

    for (auto& sgf_move : record.moveHistory())
    {
        controller.makeMove(sgf_move);
    }

    return controller.gameState().currentBoard();
}

TEST_CASE("Test game not ended #1", "[GoGameScorer]")
{
    Board board = ReadSGF("../../sgf/simple.sgf");
    GoGameScorer scorer;

    bool game_over = false;
    float score = 0.0;
    float komi = 6.5;

    game_over = scorer.scoreGame(board, komi, score);

    CHECK(game_over == false);
}

TEST_CASE("Test game not ended #2", "[GoGameScorer]")
{
    Board board = ReadSGF("../../sgf/size7/game2.sgf");
    GoGameScorer scorer;

    bool game_over = false;
    float score = 0.0;
    float komi = 6.5;

    game_over = scorer.scoreGame(board, komi, score);

    CHECK(game_over == false);
}

TEST_CASE("Test game correctly scored #1", "[GoGameScorer]")
{
    Board board = ReadSGF("../../sgf/ended.sgf");
    GoGameScorer scorer;

    bool game_over = false;
    float score = 0.0;
    float komi = 6.5;

    game_over = scorer.scoreGame(board, komi, score);

    CHECK(game_over == true);
    CHECK(score == Approx(-5.5));
}

TEST_CASE("Test game correctly scored #2", "[GoGameScorer]")
{
    Board board = ReadSGF("../../sgf/size7/game1.sgf");
    GoGameScorer scorer;

    bool game_over = false;
    float score = 0.0;
    float komi = 6.5;

    game_over = scorer.scoreGame(board, komi, score);

    CHECK(game_over == true);
    CHECK(score == Approx(8.5));
}

TEST_CASE("Test game correctly scored #3", "[GoGameScorer]")
{
    Board board = ReadSGF("../../sgf/size7/game3.sgf");
    GoGameScorer scorer;

    bool game_over = false;
    float score = 0.0;
    float komi = 6.5;

    game_over = scorer.scoreGame(board, komi, score);

    CHECK(game_over == true);
    CHECK(score == Approx(-1.5));
}
