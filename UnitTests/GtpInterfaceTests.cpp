#include "GtpInterface.h"
#include "catch.hpp"
#include <iostream>

TEST_CASE( "Whitespace is discarded", "[Gtp, Trim]" )
{
    GoEngine::GtpInterface gtp_interface;
    CHECK(gtp_interface.removeWhitespace("       \nTest") == "Test");
    CHECK(gtp_interface.removeWhitespace("    \tTest") == "Test");
    CHECK(gtp_interface.removeWhitespace("  \n  \t  T \nest") == "T \nest");
}

TEST_CASE( "Extract ID, command and args from string", "[Gtp, ExtractCommand]" )
{
    GoEngine::GtpInterface gtp_interface;
    {
        std::string id = "";
        std::string command = "";
        std::vector<std::string> args;

        gtp_interface.extractCommandAndId("532Stuff\tfah 4 black", id, command, args);
        CHECK(id == "532");
        CHECK(command == "Stuff");
        REQUIRE(args.size() == 3);
        CHECK(args[0] == "fah");
        CHECK(args[1] == "4");
        CHECK(args[2] == "black");
    }
    {
        std::string id = "";
        std::string command = "";
        std::vector<std::string> args;

        gtp_interface.extractCommandAndId("10593Bahhhh asdf 51 ", id, command, args);
        CHECK(id == "10593");
        CHECK(command == "Bahhhh");
        REQUIRE(args.size() == 2);
        CHECK(args[0] == "asdf");
        CHECK(args[1] == "51");
    }
}

TEST_CASE("Protocol version", "[Gtp, protocol_version]")
{
    GoEngine::GtpInterface gtp_interface;
    CHECK(gtp_interface.executeGtpCommand("protocol_version") == "= 2\n\n");
    CHECK(gtp_interface.executeGtpCommand("4protocol_version") == "=4 2\n\n");
    CHECK(gtp_interface.executeGtpCommand("234   protocol_version  asdf;o") == "=234 2\n\n");
}

TEST_CASE("Name", "[Gtp, name]")
{
    GoEngine::GtpInterface gtp_interface;
    CHECK(gtp_interface.executeGtpCommand("name") == "= StroGo\n\n");
    CHECK(gtp_interface.executeGtpCommand("4name\t") == "=4 StroGo\n\n");
    CHECK(gtp_interface.executeGtpCommand("234  \tname asasdf;o") == "=234 StroGo\n\n");
}

TEST_CASE("Version", "[Gtp, version]")
{
    GoEngine::GtpInterface gtp_interface;
    CHECK(gtp_interface.executeGtpCommand("version") == "= 0.1.0\n\n");
    CHECK(gtp_interface.executeGtpCommand("4version\t") == "=4 0.1.0\n\n");
}

TEST_CASE("Komi", "[Gtp, komi]")
{
    {
        GoEngine::GtpInterface gtp_interface;
        CHECK(gtp_interface.executeGtpCommand("komi 5") == "= \n\n");
        CHECK(gtp_interface.gameController().komi() == 5);
    }

    {
        GoEngine::GtpInterface gtp_interface;
        CHECK(gtp_interface.executeGtpCommand("100 komi -4.5") == "=100 \n\n");
        CHECK(gtp_interface.gameController().komi() == -4.5);
    }
/*
    {
        GoEngine::GtpInterface gtp_interface;
        CHECK(gtp_interface.executeGtpCommand("105 komi asd") == "?105 " + ErrorMessages::INVALID_KOMI + "\n\n");
    }
*/
}

TEST_CASE("Boardsize command", "[Gtp, boardsize]")
{
    GoEngine::GtpInterface gtp_interface;
    CHECK(gtp_interface.gameController().boardsize() == 19);
    CHECK(gtp_interface.executeGtpCommand("boardsize 9") == "= \n\n");
    CHECK(gtp_interface.gameController().boardsize() == 9);
/*
    CHECK(gtp_interface.executeGtpCommand("105 boardsize asd") == "?105 " + ErrorMessages::INVALID_BOARDSIZE + "\n\n");
    CHECK(gtp_interface.gameController().boardsize() == 9);
*/
}

TEST_CASE("Clear board command", "[Gtp, clear_board]")
{
    GoEngine::GtpInterface gtp_interface;
    CHECK(gtp_interface.executeGtpCommand("boardsize 9") == "= \n\n");
    CHECK(gtp_interface.gameController().boardsize() == 9);

    CHECK(gtp_interface.executeGtpCommand("play w A9") == "= \n\n");
    CHECK(!gtp_interface.gameController().gameState().currentBoard().empty());

    CHECK(gtp_interface.executeGtpCommand("clear_board") == "= \n\n");
    CHECK(gtp_interface.gameController().gameState().currentBoard().empty());
    CHECK(gtp_interface.gameController().boardsize() == 9);
}

TEST_CASE("List commands command", "[Gtp, list_commands]")
{
    std::string expected_response = "= ";
    expected_response += "boardsize\n";
    expected_response += "clear_board\n";    
    expected_response += "fixed_handicap\n";
    expected_response += "genmove\n";
    expected_response += "known_command\n";
    expected_response += "komi\n";
    expected_response += "list_commands\n";
    expected_response += "name\n";
    expected_response += "play\n";
    expected_response += "protocol_version\n";
    expected_response += "quit\n";
    expected_response += "reg_genmove\n";
    expected_response += "set_free_handicap\n";
    expected_response += "version\n";

    expected_response += "\n\n";

    GoEngine::GtpInterface gtp_interface;
    CHECK(gtp_interface.executeGtpCommand("list_commands") == expected_response);
}

TEST_CASE("Known command", "[Gtp, known_command]")
{
    std::string command_found = "= true\n\n";
    std::string unknown_command = "= false\n\n";

    GoEngine::GtpInterface gtp_interface;
    CHECK(gtp_interface.executeGtpCommand("known_command boardsize") == command_found);
    CHECK(gtp_interface.executeGtpCommand("known_command clear_board") == command_found);
    CHECK(gtp_interface.executeGtpCommand("known_command known_command") == command_found);
    CHECK(gtp_interface.executeGtpCommand("known_command komi") == command_found);
    CHECK(gtp_interface.executeGtpCommand("known_command list_commands") == command_found);
    CHECK(gtp_interface.executeGtpCommand("known_command name") == command_found);
    CHECK(gtp_interface.executeGtpCommand("known_command play") == command_found);
    CHECK(gtp_interface.executeGtpCommand("known_command protocol_version") == command_found);
    CHECK(gtp_interface.executeGtpCommand("known_command quit") == command_found);
    CHECK(gtp_interface.executeGtpCommand("known_command version") == command_found);
    CHECK(gtp_interface.executeGtpCommand("known_command genmove") == command_found);
    CHECK(gtp_interface.executeGtpCommand("known_command set_free_handicap") == command_found);
    CHECK(gtp_interface.executeGtpCommand("known_command fixed_handicap") == command_found);
    CHECK(gtp_interface.executeGtpCommand("known_command reg_genmove") == command_found);
    
    CHECK(gtp_interface.executeGtpCommand("known_command place_free_handicap") == unknown_command);
    CHECK(gtp_interface.executeGtpCommand("known_command loadsgf") == unknown_command);
    CHECK(gtp_interface.executeGtpCommand("known_command undo") == unknown_command);
    CHECK(gtp_interface.executeGtpCommand("known_command time_settings") == unknown_command);
    CHECK(gtp_interface.executeGtpCommand("known_command time_left") == unknown_command);
    CHECK(gtp_interface.executeGtpCommand("known_command final_score") == unknown_command);
    CHECK(gtp_interface.executeGtpCommand("known_command final_status_list") == unknown_command);
    CHECK(gtp_interface.executeGtpCommand("known_command showboard") == unknown_command);
}

TEST_CASE("Set free handicap command", "[Gtp, handicap]")
{
    GoEngine::GtpInterface gtp_interface;
    CHECK(gtp_interface.executeGtpCommand("set_free_handicap d4 D16 q4 Q16") == "= \n\n");

    const auto& board_state = gtp_interface.gameController().gameState().currentBoard().boardState();

    int8_t black = 1;
    CHECK(board_state.at(3).at(3) == black);
    CHECK(board_state.at(15).at(3) == black);
    CHECK(board_state.at(15).at(15) == black);
    CHECK(board_state.at(3).at(15) == black);
}
