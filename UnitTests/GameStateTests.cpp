#include <list>

#include "catch.hpp"
#include "GameState.h"
#include "SimpleSgfRecord.h"
#include "SgfReader.h"

using namespace GoLogic;

namespace GameStateTests
{
    bool CheckNoStonesExistOfTargetColour(const Board& board, StoneColor target_colour)
    {
        const auto& state = board.boardState();
        for (const auto& row : state)
        {
            for (const auto& point : row)
            {
                if (point == static_cast<int8_t>(target_colour)) return false;
            }
        }

        return true;
    }

    TEST_CASE("GameState illegal move if beyond board size", "[GameState, Illegal, Move]")
    {
        {
            GoEngine::GameState state(7, Ruleset::CHINESE, SuperkoRuleset::PSK);
            CHECK(!state.addStone(Stone(PointLetter::A, 8, StoneColor::BLACK)));
            CHECK(!state.addStone(Stone(PointLetter::H, 1, StoneColor::BLACK)));
            CHECK(!state.addStone(Stone(PointLetter::A, 0, StoneColor::BLACK)));
        }
        {
            GoEngine::GameState state(18, Ruleset::CHINESE, SuperkoRuleset::PSK);
            CHECK(!state.addStone(Stone(PointLetter::A, 19, StoneColor::BLACK)));
            CHECK(!state.addStone(Stone(PointLetter::T, 1, StoneColor::BLACK)));
            CHECK(!state.addStone(Stone(PointLetter::A, 0, StoneColor::BLACK)));
        }
    }

    TEST_CASE("GameState illegal move if point occupied", "[GameState, Illegal, Move]")
    {
        GoEngine::GameState state(19, Ruleset::CHINESE, SuperkoRuleset::PSK);
        CHECK(state.addStone(Stone(PointLetter::J, 4, StoneColor::WHITE)));
        CHECK(!state.addStone(Stone(PointLetter::J, 4, StoneColor::WHITE)));
        CHECK(!state.addStone(Stone(PointLetter::J, 4, StoneColor::BLACK)));

        CHECK(state.addStone(Stone(PointLetter::A, 4, StoneColor::WHITE)));
        CHECK(!state.addStone(Stone(PointLetter::A, 4, StoneColor::WHITE)));
        CHECK(!state.addStone(Stone(PointLetter::A, 4, StoneColor::BLACK)));
    }

    TEST_CASE("GameState cumulative capture black and white stones", "[GameState, ConnectedGroup, Liberties, Capture, addStone]")
    {
        GoEngine::GameState state(19, Ruleset::CHINESE, SuperkoRuleset::PSK);

        CHECK(state.currentBoard().blackCapturedStones() == 0);
        CHECK(state.currentBoard().whiteCapturedStones() == 0);

        state.addStone(Stone(PointLetter::Q, 16, StoneColor::BLACK));
        state.addStone(Stone(PointLetter::D, 16, StoneColor::WHITE));
        state.addStone(Stone(PointLetter::D, 17, StoneColor::BLACK));
        state.addStone(Stone(PointLetter::D, 15, StoneColor::BLACK));
        state.addStone(Stone(PointLetter::C, 16, StoneColor::BLACK));
        state.addStone(Stone(PointLetter::E, 16, StoneColor::BLACK));
        state.addStone(Stone(PointLetter::Q, 15, StoneColor::WHITE));
        state.addStone(Stone(PointLetter::Q, 17, StoneColor::WHITE));
        state.addStone(Stone(PointLetter::P, 16, StoneColor::WHITE));
        state.addStone(Stone(PointLetter::R, 16, StoneColor::WHITE));

        CHECK(state.currentBoard().blackCapturedStones() == 1);
        CHECK(state.currentBoard().whiteCapturedStones() == 1);
    }

    TEST_CASE("GameState double capture and suicide is legal", "[GameState, ConnectedGroup, Suicide, Liberties, Capture, addStone]")
    {
        GoEngine::GameState state(19, Ruleset::CHINESE, SuperkoRuleset::PSK);

        state.addStone(Stone(PointLetter::A, 3, StoneColor::BLACK));
        state.addStone(Stone(PointLetter::B, 2, StoneColor::BLACK));
        state.addStone(Stone(PointLetter::C, 1, StoneColor::BLACK));
        state.addStone(Stone(PointLetter::A, 2, StoneColor::WHITE));
        state.addStone(Stone(PointLetter::B, 1, StoneColor::WHITE));

        CHECK(state.currentBoard().blackCapturedStones() == 0);
        CHECK(state.currentBoard().whiteCapturedStones() == 0);

        // Suicide: move should be legal since it is a capture
        CHECK(state.addStone(Stone(PointLetter::A, 1, StoneColor::BLACK)));

        CHECK(state.currentBoard().blackCapturedStones() == 0);
        CHECK(state.currentBoard().whiteCapturedStones() == 2);

        // No white stones on board
        CheckNoStonesExistOfTargetColour(state.currentBoard(), StoneColor::WHITE);
    }

    TEST_CASE("GameState illegal move if suicide", "[GameState, Illegal, Move, Suicide]")
    {
        GoEngine::GameState state(19, Ruleset::CHINESE, SuperkoRuleset::PSK);
        CHECK(state.addStone(Stone(PointLetter::H, 4, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::K, 4, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::J, 3, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::J, 5, StoneColor::WHITE)));

        // Middle of a ponuki
        CHECK(!state.addStone(Stone(PointLetter::J, 4, StoneColor::BLACK)));
        CHECK(!state.addStone(Stone(PointLetter::J, 4, StoneColor::BLACK)));
    }

    TEST_CASE("Illegal move if ko", "[GameState, Illegal, Move, Ko]")
    {
        GoEngine::GameState state(19, Ruleset::CHINESE, SuperkoRuleset::PSK);
        CHECK(state.addStone(Stone(PointLetter::K, 4, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::G, 4, StoneColor::WHITE)));

        CHECK(state.addStone(Stone(PointLetter::H, 4, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::H, 5, StoneColor::WHITE)));

        CHECK(state.addStone(Stone(PointLetter::J, 3, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::H, 3, StoneColor::WHITE)));

        CHECK(state.addStone(Stone(PointLetter::J, 5, StoneColor::BLACK)));

        CHECK(state.addStone(Stone(PointLetter::J, 4, StoneColor::WHITE)));

        // Ko forbids this move
        CHECK(!state.addStone(Stone(PointLetter::H, 4, StoneColor::BLACK)));
    }

    TEST_CASE("Superko #1", "[GameState, Illegal, Move, Superko, Ko]")
    {
        GoEngine::GameState state(19, Ruleset::CHINESE, SuperkoRuleset::PSK);

        // http://senseis.xmp.net/?Superko#toc2

        // Initial setup, black group
        CHECK(state.addStone(Stone(PointLetter::A, 16, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::B, 16, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::C, 16, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::C, 17, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::C, 18, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::B, 18, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::D, 18, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::D, 19, StoneColor::BLACK)));

        // Initial setup, white outer group
        CHECK(state.addStone(Stone(PointLetter::A, 15, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::B, 15, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::C, 15, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::D, 15, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::D, 16, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::D, 17, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::E, 17, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::E, 18, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::E, 19, StoneColor::WHITE)));

        // Initial setup, white inner stones
        CHECK(state.addStone(Stone(PointLetter::A, 17, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::A, 18, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::B, 19, StoneColor::WHITE)));

        // Move 'a' and 'b' in the example
        CHECK(state.addStone(Stone(PointLetter::C, 19, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::A, 19, StoneColor::BLACK)));

        // Move 'c' is illegal according to positional superko
        CHECK(!state.addStone(Stone(PointLetter::B, 19, StoneColor::WHITE)));
    }

    TEST_CASE("Superko #2", "[GameState, Illegal, Move, Superko, Ko]")
    {
        GoEngine::GameState state(19, Ruleset::CHINESE, SuperkoRuleset::PSK);

        // Adapted from http://senseis.xmp.net/?PositionalSuperko%2FExample
        // Only the stones necessary for producing superko have been included

        // Initial setup, black group
        CHECK(state.addStone(Stone(PointLetter::D, 4, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::D, 6, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::C, 5, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::E, 5, StoneColor::BLACK)));

        // Initial setup, white group
        CHECK(state.addStone(Stone(PointLetter::E, 4, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::F, 4, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::E, 6, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::F, 6, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::G, 5, StoneColor::WHITE)));

        // Two moves which lead to superko
        CHECK(state.addStone(Stone(PointLetter::F, 5, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::D, 5, StoneColor::WHITE)));

        // Retaking at E5 is illegal
        CHECK(!state.addStone(Stone(PointLetter::E, 5, StoneColor::BLACK)));
    }

    TEST_CASE("Superko #3", "[GameState, Illegal, Move, Superko, Ko]")
    {
        GoEngine::GameState state(6, Ruleset::CHINESE, SuperkoRuleset::PSK);

        // http://senseis.xmp.net/?RulesBeast1

        // Initial setup, black main group
        CHECK(state.addStone(Stone(PointLetter::A, 5, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::A, 4, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::A, 3, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::B, 4, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::C, 4, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::D, 4, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::D, 3, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::D, 2, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::E, 2, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::F, 2, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::E, 1, StoneColor::BLACK)));

        // Initial setup, black three stones at top
        CHECK(state.addStone(Stone(PointLetter::D, 6, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::E, 6, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::F, 5, StoneColor::BLACK)));

        // Initial setup, lone black stone
        CHECK(state.addStone(Stone(PointLetter::B, 1, StoneColor::BLACK)));

        // Initial setup, lower white group
        CHECK(state.addStone(Stone(PointLetter::A, 2, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::B, 3, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::C, 3, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::C, 2, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::C, 1, StoneColor::WHITE)));

        // Initial setup, upper white group
        CHECK(state.addStone(Stone(PointLetter::A, 6, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::B, 6, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::C, 5, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::D, 5, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::E, 5, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::E, 4, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::E, 3, StoneColor::WHITE)));
        CHECK(state.addStone(Stone(PointLetter::F, 3, StoneColor::WHITE)));

        // Sequence of moves which lead to superko
        CHECK(state.addStone(Stone(PointLetter::F, 4, StoneColor::BLACK)));

        CHECK(state.addStone(Stone(PointLetter::F, 6, StoneColor::WHITE)));

        CHECK(state.currentBoard().blackCapturedStones() == 2);
        CHECK(state.currentBoard().whiteCapturedStones() == 0);

        CHECK(state.addStone(Stone(PointLetter::B, 2, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::A, 1, StoneColor::WHITE)));

        CHECK(state.currentBoard().blackCapturedStones() == 4);
        CHECK(state.currentBoard().whiteCapturedStones() == 0);

        CHECK(state.addStone(Stone(PointLetter::B, 2, StoneColor::BLACK)));
        CHECK(state.addStone(Stone(PointLetter::B, 1, StoneColor::WHITE)));

        CHECK(state.currentBoard().blackCapturedStones() == 5);
        CHECK(state.currentBoard().whiteCapturedStones() == 0);

        CHECK(state.addStone(Stone(PointLetter::F, 5, StoneColor::BLACK))); // White passes
        CHECK(state.addStone(Stone(PointLetter::F, 4, StoneColor::BLACK)));

        // Capture is illegal
        CHECK(!state.addStone(Stone(PointLetter::F, 6, StoneColor::WHITE)));
    }

    TEST_CASE("Illegal move #1", "[GameState, Illegal, Move]")
    {
        std::ostringstream sgf;
        sgf << "(;FF[4]CA[UTF-8]AP[GoGui:1.4.9]SZ[9]"
            << "KM[6.5]PW[StroGo]DT[2018-08-27]"
            << ";B[fc];W[ch];B[fg];W[bf];B[cd];W[eg];B[ff];W[de];B[ed];W[gh]"
            << ";B[fh];W[cb];B[dd];W[gf];B[gg];W[ge];B[hh];W[fb];B[gb];W[bc]"
            << ";B[bd];W[ec];B[eb];W[gc];B[fa];W[hg];B[hf];W[he];B[ig];W[gi]"
            << ";B[hc];W[fd];B[gd];W[ae];B[ad];W[hb];B[fe];W[ef];B[ce];W[dc]"
            << ";B[cc];W[db];B[bb];W[ab];B[ac];W[cg];B[df];W[ic];B[id];W[dg]"
            << ";B[ee];W[if];B[ie])";

        SgfReader reader;
        auto record = reader.readSgfContent(sgf.str());

        GoEngine::GameState state(record.boardSize(), Ruleset::CHINESE, SuperkoRuleset::PSK);

        for (auto& sgf_move : record.moveHistory())
        {
            CHECK(state.addStone(sgf_move.stone()));
        }

        CHECK(!state.addStone(Stone(PointLetter::H, 4, StoneColor::WHITE)));
    }

    TEST_CASE("Illegal false positive #1", "[GameState, Illegal, Move]")
    {
        std::ostringstream sgf;
        sgf << "(;FF[4]CA[UTF-8]AP[GoGui:1.4.9]SZ[9]KM[6.5]PW[StroGo]DT[2018-08-27]"
            << ";B[cg];W[gc];B[cd];W[ff];B[ef];W[ee];B[eh];W[ec];B[bb];W[fe];B[gg]"
            << ";W[fg];B[hf];W[he];B[hc];W[hd];B[eb];W[fb];B[bg];W[df];B[ga];W[fa]"
            << ";B[dd];W[ed];B[cc];W[db];B[gi];W[fh];B[da];W[ea];B[ic];W[gh];B[hh]"
            << ";W[gf];B[eg];W[dg];B[ch];W[dh];B[ei];W[di];B[de];W[ca];B[ag];W[fi]"
            << ";B[hg];W[hi];B[ih];W[if];B[fc];W[fd];B[ie];W[id];B[ge];W[gd];B[gb]"
            << ";W[hb];B[ia];W[ib];B[ef];W[eg];B[ab];W[cf];B[bf];W[ce];B[bc];W[be]"
            << ";B[ad];W[ae];B[eh];W[ci];B[bi];W[ah];B[bh];W[af];B[ai])";

        SgfReader reader;
        auto record = reader.readSgfContent(sgf.str());

        GoEngine::GameState state(record.boardSize(), Ruleset::CHINESE, SuperkoRuleset::PSK);

        for (auto& sgf_move : record.moveHistory())
        {
            CHECK(state.addStone(sgf_move.stone()));
        }

        CHECK(state.addStone(Stone(PointLetter::A, 8, StoneColor::WHITE)));
    }

    TEST_CASE("Illegal false positive #2", "[GameState, Illegal, Move]")
    {
        std::ostringstream sgf;
        sgf << "(;FF[4]CA[UTF-8]AP[GoGui:1.4.9]SZ[9]KM[6.5]PW[StroGo]DT[2018-08-27]"
            << ";B[ff];W[gg];B[fg];W[cf]"
            << ";B[ce];W[eg];B[ef];W[eb];B[ec];W[ch]"
            << ";B[dg];W[fh];B[df];W[bc];B[cc];W[gc];B[gd];W[af];B[be];W[cd]"
            << ";B[bd];W[dc];B[dd];W[hg];B[gf];W[dh];B[cg];W[he];B[hf];W[ac]"
            << ";B[bf];W[bg];B[ae];W[hc];B[hd];W[da];B[db];W[cb];B[dc];W[fd]"
            << ";B[fc];W[ge];B[fe];W[id];B[ed];W[gi];B[gd];W[gb];B[fb];W[ea]"
            << ";B[fa];W[ba];B[ca];W[da];B[ea];W[bh];B[ca];W[ga];B[bb];W[ab]"
            << ";B[aa];W[de];B[ee];W[ba];B[cb];W[ai];B[ig];W[ih];B[if];W[ei]"
            << ";B[hh];W[gh];B[ii];W[hi];B[ih];W[eh];B[ag];W[ah];B[af];W[hd]"
            << ";B[fd];W[];B[aa];W[];B[ad];W[];B[hb];W[ha];B[ib];W[ic]"
            << ";B[ie];W[ab])";

        SgfReader reader;
        auto record = reader.readSgfContent(sgf.str());

        GoEngine::GameState state(record.boardSize(), Ruleset::CHINESE, SuperkoRuleset::PSK);

        for (auto& sgf_move : record.moveHistory())
        {
            CHECK(state.addStone(sgf_move.stone()));
        }

        CHECK(state.addStone(Stone(PointLetter::J, 1, StoneColor::BLACK)));
    }
}