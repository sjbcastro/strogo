#include <catch.hpp>
#include "GtpCommands.h"

using namespace GoLogic;

TEST_CASE("White move is parsed", "[PlayCommand, parse]")
{
    std::vector<std::string> args = {"W", "A2"};
    PlayCommand p(args);
    CHECK(!p.move().isPass());
    CHECK(p.move().stone().x() == PointLetter::A);
    CHECK(p.move().stone().y() == 2);
    CHECK(p.move().stone().color() == StoneColor::WHITE);
}

TEST_CASE("White move with double digits and lower case is parsed", "[PlayCommand, parse]")
{
    std::vector<std::string> args = {"w", "j15"};
    PlayCommand p(args);
    CHECK(!p.move().isPass());
    CHECK(p.move().stone().x() == PointLetter::J);
    CHECK(p.move().stone().y() == 15);
    CHECK(p.move().stone().color() == StoneColor::WHITE);
}

TEST_CASE("Black move is parsed", "[PlayCommand, parse]")
{
    std::vector<std::string> args = {"B", "D6"};
    PlayCommand p(args);
    CHECK(!p.move().isPass());
    CHECK(p.move().stone().x() == PointLetter::D);
    CHECK(p.move().stone().y() == 6);
    CHECK(p.move().stone().color() == StoneColor::BLACK);
}

TEST_CASE("Black move with double digits and lower case is parsed", "[PlayCommand, parse]")
{
    std::vector<std::string> args = {"b", "e19"};
    PlayCommand p(args);
    CHECK(!p.move().isPass());
    CHECK(p.move().stone().x() == PointLetter::E);
    CHECK(p.move().stone().y() == 19);
    CHECK(p.move().stone().color() == StoneColor::BLACK);
}

TEST_CASE("Black pass with fully qualified color", "[PlayCommand, parse, pass]")
{
    std::vector<std::string> args = {"bLaCk", "pAsS"};
    PlayCommand p(args);
    CHECK(p.move().isPass());
    CHECK(p.move().stone().x() == PointLetter::INVALID);
    CHECK(p.move().stone().y() == 0);
    CHECK(p.move().stone().color() == StoneColor::BLACK);
}

TEST_CASE("White pass with fully qualified color", "[PlayCommand, parse, pass]")
{
    std::vector<std::string> args = {"white", "PASS"};
    PlayCommand p(args);
    CHECK(p.move().isPass());
    CHECK(p.move().stone().x() == PointLetter::INVALID);
    CHECK(p.move().stone().y() == 0);
    CHECK(p.move().stone().color() == StoneColor::WHITE);
}
/*
TEST_CASE("Invalid color", "[PlayCommand, parse, pass]")
{
    std::vector<std::string> args = {"whitef", "PASS"};
    PlayCommand p(args);
    CHECK(!p.move().isPass());
    CHECK(p.move().stone().x() == PointLetter::INVALID);
    CHECK(p.move().stone().y() == 0);
}

TEST_CASE("Invalid white move", "[PlayCommand, parse, pass]")
{
    std::vector<std::string> args = {"white", "asdf;oian"};
    PlayCommand p(args);
    CHECK(!p.move().isPass());
    CHECK(p.move().stone().x() == PointLetter::INVALID);
    CHECK(p.move().stone().y() == 0);
    CHECK(p.move().stone().color() == StoneColor::WHITE);
}

TEST_CASE("Invalid black move", "[PlayCommand, parse, pass]")
{
    std::vector<std::string> args = {"b", "fg12"};
    PlayCommand p(args);
    CHECK(!p.move().isPass());
    CHECK(p.move().stone().x() == PointLetter::INVALID);
    CHECK(p.move().stone().y() == 0);
    CHECK(p.move().stone().color() == StoneColor::BLACK);
}
*/
TEST_CASE("Komi command", "[KomiCommand, parse]")
{
    std::vector<std::string> args = {"5.5"};
    KomiCommand command(args);
    CHECK(command.successfullyParsed());
    CHECK(command.value() == 5.5);
}
/*
TEST_CASE("Komi command fail", "[KomiCommand, parse]")
{
    std::vector<std::string> args = {"asdff"};
    KomiCommand command(args);
    CHECK(!command.successfullyParsed());
    CHECK(command.value() == 0.0);
}
*/
TEST_CASE("Komi command ridiculous number", "[KomiCommand, parse]")
{
    std::vector<std::string> args = {"21347902984"};
    KomiCommand command(args);
    CHECK(command.successfullyParsed());
    CHECK(command.value() == 21347902984);
}
