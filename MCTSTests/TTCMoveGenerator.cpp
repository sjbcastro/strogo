#include "TTCMoveGenerator.h"
#include "MCTSMove.h"
#include "MCTSPossibleMoves.h"

#include "TTCPossibleMoves.h"
#include "TTCMove.h"

#include <cassert>

namespace TTC
{
	std::unique_ptr<MCTS::PossibleMoves> MoveGenerator::possibleMoves() const
	{
		return std::make_unique<TTC::PossibleMoves>(_grid, _player_id);
	}

	void MoveGenerator::set(const MCTS::MoveGenerator& game_state)
	{
		auto concrete_game_state = dynamic_cast<const TTC::MoveGenerator&>(game_state);
		_grid = concrete_game_state._grid;
		_player_id = concrete_game_state._player_id;
	}

	std::unique_ptr<MCTS::MoveGenerator> MoveGenerator::clone() const
	{
		return std::make_unique<TTC::MoveGenerator>(*this);
	}

	void MoveGenerator::processMove(const MCTS::Move& m)
	{
		const Move& temp = dynamic_cast<const Move&>(m);
		_grid.set(temp.x(), temp.y(), _player_id);
		_player_id = _player_id == 1 ? 2 : 1;
	}

	std::shared_ptr<MCTS::Move> MoveGenerator::generateRandomMove(std::mt19937& rng)
	{
		auto iterator = _grid.find(0);
		size_t n_empty_points = 0;

		while (iterator != BoardGrid::end())
		{
			++n_empty_points;
			iterator = _grid.findNext(iterator, 0);
		}

		int move_index = rng() % n_empty_points;

		iterator = _grid.find(0);

		int index = 0;
		while (move_index != index)
		{
			iterator = _grid.findNext(iterator, 0);
			++index;
		}

		return std::make_shared<Move>(iterator.x(), iterator.y());
	}


	std::shared_ptr<MCTS::Move> MoveGenerator::defaultMoveWhenNoneFound() const
	{
		return nullptr;
	}

	bool MoveGenerator::gameOver(float& score) const
	{
		bool we_won = checkFor3InARow(_requested_player_id);
		auto opponent_id = _requested_player_id == 1 ? 2 : 1;
		bool we_lost = checkFor3InARow(opponent_id);

		assert(!we_won || !we_lost);

		if (we_won)
		{
			score = 1.0f;
			return true;
		}
		else if (we_lost)
		{
			score = 0.0f;
			return true;
		}
		else if (_grid.find(0) == BoardGrid::end())
		{
			score = 0.5f;
			return true;
		}

		return false;
	}

	void MoveGenerator::possibleMoves(std::shared_ptr<MCTS::PossibleMoves>& possible_moves)
	{
		if (possible_moves)
		{
			auto concrete_possible_moves = std::dynamic_pointer_cast<TTC::PossibleMoves>(possible_moves);
			concrete_possible_moves->set(_grid, _player_id);
		}
		else
		{
			possible_moves = std::make_shared<TTC::PossibleMoves>(_grid, _player_id);
		}
	}

	void MoveGenerator::print() const
	{
		_grid.printToConsole();
	}

	bool MoveGenerator::checkFor3InARow(int_fast8_t colour) const
	{
		// Diagonals
		if (_grid.get(1, 1) == colour && _grid.get(0, 0) == colour && _grid.get(2, 2) == colour) return true;
		if (_grid.get(1, 1) == colour && _grid.get(0, 2) == colour && _grid.get(2, 0) == colour) return true;

		// Horizontal rows
		if (_grid.get(0, 0) == colour && _grid.get(0, 1) == colour && _grid.get(0, 2) == colour) return true;
		if (_grid.get(1, 0) == colour && _grid.get(1, 1) == colour && _grid.get(1, 2) == colour) return true;
		if (_grid.get(2, 0) == colour && _grid.get(2, 1) == colour && _grid.get(2, 2) == colour) return true;

		// Vertical rows
		if (_grid.get(0, 0) == colour && _grid.get(1, 0) == colour && _grid.get(2, 0) == colour) return true;
		if (_grid.get(0, 1) == colour && _grid.get(1, 1) == colour && _grid.get(2, 1) == colour) return true;
		if (_grid.get(0, 2) == colour && _grid.get(1, 2) == colour && _grid.get(2, 2) == colour) return true;

		return false;
	}

}