#pragma once

#include "MCTSMoveGenerator.h"
#include "BoardGrid.h"

namespace Notakto
{
    class Move;
	class MoveGenerator : public MCTS::MoveGenerator
	{
	public:

        MoveGenerator(int_fast8_t requested_player_id) : _requested_player_id(requested_player_id) {}

        virtual std::unique_ptr<MCTS::PossibleMoves> possibleMoves() const override;

        virtual void set(const MCTS::MoveGenerator&) override;

        virtual std::unique_ptr<MCTS::MoveGenerator> clone() const override;

        //virtual int_fast8_t myId() const override;

        //virtual void processMoveSet(const MCTS::MoveSet& move_set) override;

        void processMove(const MCTS::Move& m) override;

        virtual std::shared_ptr<MCTS::Move> generateRandomMove(std::mt19937& rng) override;

        virtual std::shared_ptr<MCTS::Move> defaultMoveWhenNoneFound() const override;

        virtual bool gameOver(float& score) const override;

        virtual void possibleMoves(std::shared_ptr<MCTS::PossibleMoves>&) override;

        int_fast8_t playerColour() const { return _player_id; }

        virtual void print() const override;

        virtual bool nextMoveIsMainPlayer() const override { return _requested_player_id == _player_id; }

	private:

        bool checkGameOver() const;

        int_fast8_t _requested_player_id = 0;

        bool _game_over = false;
        int_fast8_t _loser_player_id = 0;

        // next player
        int_fast8_t _player_id = 1;
        BoardGrid _grid{ 3, 3, 2 };
	};
}