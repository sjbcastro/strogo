#pragma once

#include "MCTSMoveGenerator.h"
#include "BoardGrid.h"

namespace TTC
{
    class Move;
    class MoveGenerator : public MCTS::MoveGenerator
    {
    public:

        MoveGenerator(int_fast8_t requested_player_id) : _requested_player_id(requested_player_id) {}

        virtual std::unique_ptr<MCTS::PossibleMoves> possibleMoves() const override;

        virtual void set(const MCTS::MoveGenerator&) override;

        virtual std::unique_ptr<MCTS::MoveGenerator> clone() const override;

        //virtual int_fast8_t myId() const override;

        //virtual void processMoveSet(const MCTS::MoveGenerator& move_set) override;
        virtual void processMove(const MCTS::Move&) override;

        virtual std::shared_ptr<MCTS::Move> generateRandomMove(std::mt19937& rng) override;

        virtual std::shared_ptr<MCTS::Move> defaultMoveWhenNoneFound() const override;

        virtual bool gameOver(float& score) const override;

        virtual void possibleMoves(std::shared_ptr<MCTS::PossibleMoves>&) override;

        int_fast8_t playerColour() const { return _player_id; }

        virtual void print() const override;

        virtual bool nextMoveIsMainPlayer() const override { return _requested_player_id == _player_id; }

    private:

        bool checkFor3InARow(int_fast8_t colour) const;

        int_fast8_t _requested_player_id = 0;
        int_fast8_t _player_id = 1;
        BoardGrid _grid{ 3, 3, 3 };
    };
}