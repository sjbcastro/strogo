#include "NotaktoPossibleMoves.h"
#include "NotaktoPossibleMoves.h"
#include "MCTSMove.h"

#include <cassert>

namespace Notakto
{
	PossibleMoves::PossibleMoves(const BoardGrid& current_board, int_fast8_t player_id)
		: _current_board(current_board)
		, _board_after_current_move(current_board)
		, _player_id(player_id)
	{
		populateNumPossibleNodes();
	}

	void PossibleMoves::set(const BoardGrid& current_board, int_fast8_t player_id)
	{
		_current_board = current_board;
		_board_after_current_move = current_board;
		_player_id = player_id;
		populateNumPossibleNodes();
	}

	void PossibleMoves::advanceIterators()
	{
		_current_response_iterator = _board_after_current_move.findNext(_current_response_iterator, 0);

		if (_current_response_iterator == BoardGrid::end())
		{
			_current_move_iterator = _current_board.findNext(_current_move_iterator, 0);

			if (_current_move_iterator != BoardGrid::end())
			{
				_board_after_current_move = _current_board;
				_board_after_current_move.set(_current_move_iterator.x(), _current_move_iterator.y(), 1);
				_current_response_iterator = _board_after_current_move.find(0);
			}
		}
	}

	void PossibleMoves::generateRandomMove(std::mt19937& rng, std::shared_ptr<MCTS::Move>& move_made) const
	{
		size_t move_index = rng() % _num_possible_nodes;
		auto move_iterator = _current_board.find(0);

		for (int i = 0; i < move_index; ++i)
		{
			move_iterator = _current_board.findNext(move_iterator, 0);
		}

		move_made = std::make_shared<Move>(move_iterator.x(), move_iterator.y());
	}

	std::unique_ptr<MCTS::Move> PossibleMoves::getFirstMove()
	{
		_current_move_iterator = _current_board.find(0);
		_board_after_current_move.set(_current_move_iterator.x(), _current_move_iterator.y(), 1);
		return std::make_unique<Move>(_current_move_iterator.x(), _current_move_iterator.y());
	}

	bool PossibleMoves::getNextMove(std::shared_ptr<MCTS::Move>& next_move)
	{
		_current_move_iterator = _current_board.findNext(_current_move_iterator, 0);
		if (_current_move_iterator == BoardGrid::end())
		{
			return false;
		}

		next_move = std::make_shared<Move>(_current_move_iterator.x(), _current_move_iterator.y());

		return true;
	}

	void PossibleMoves::populateNumPossibleNodes()
	{
		_current_move_iterator = _current_board.find(0);
		_num_possible_nodes = 0;

		while (_current_move_iterator != BoardGrid::end())
		{
			++_num_possible_nodes;
			_current_move_iterator = _current_board.findNext(_current_move_iterator, 0);
		}
	}
}