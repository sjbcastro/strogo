#pragma once

#include "MCTSMove.h"
#include <stdexcept>

namespace Notakto
{
	class Move : public MCTS::Move
	{
	public:

		Move() = default;

		Move(int_fast8_t x, int_fast8_t y)
			: _x(x)
			, _y(y)
		{

		}

		int_fast8_t x() const { return _x; }
		int_fast8_t y() const { return _y; }

		virtual std::string toString() const override
		{
			return std::to_string(_x) + "," + std::to_string(_y);
		}

		virtual bool isEqual(const MCTS::Move& m) const override
		{
			auto concrete_move = dynamic_cast<const Notakto::Move&>(m);
			return concrete_move._x == _x && concrete_move._y == _y;
		}
	private:
		int_fast8_t _x = 0;
		int_fast8_t _y = 0;
	};
}