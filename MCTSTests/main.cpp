#include "MCTSAlgorithm.h"

#include "NotaktoMoveGenerator.h"
#include "NotaktoMove.h"

#include "TTCMoveGenerator.h"
#include "TTCMove.h"

#include <iostream>

int main()
{
	MCTS::Algorithm alg;
	float score; 
	bool pc_plays_next = true;

	int_fast8_t id = pc_plays_next ? 1 : 2;
	Notakto::MoveGenerator move_generator(id);

	move_generator.print();
	std::cout << std::endl;

	while (!move_generator.gameOver(score))
	{
		if (pc_plays_next)
		{
			auto move = std::dynamic_pointer_cast<Notakto::Move>(alg.findOptimalMove(move_generator, 2000));
			move_generator.processMove(*move);
			pc_plays_next = false;
		}
		else
		{
			int x, y;
			std::cout << "Make your move" << std::endl;
			std::cin >> x >> y;

			if (x >= 0)
			{
				Notakto::Move m(x, y);
				move_generator.processMove(m);
				pc_plays_next = true;
			}
			else
			{
				std::cout << "Invalid move" << std::endl;
			}
		}

		std::cout << std::endl;
		move_generator.print();
		std::cout << std::endl;
	}

	std::cout << "Game over! Score = " << score << std::endl;
}