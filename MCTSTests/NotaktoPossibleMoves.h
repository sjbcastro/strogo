#pragma once

#include "MCTSPossibleMoves.h"
#include "NotaktoMove.h"
#include "BoardGrid.h"

#include <array>

namespace Notakto
{
	class PossibleMoves : public MCTS::PossibleMoves
	{
	public:

		PossibleMoves() = default;
		PossibleMoves(const BoardGrid& current_board, int_fast8_t player_id);

		void set(const BoardGrid& current_board, int_fast8_t player_id);

		virtual size_t numPossibleNodes() const override { return _num_possible_nodes; }

		virtual void generateRandomMove(std::mt19937& rng, std::shared_ptr<MCTS::Move>& move_made) const override;

		virtual std::unique_ptr<MCTS::Move> getFirstMove() override;

		virtual bool getNextMove(std::shared_ptr<MCTS::Move>& next_move) override;

	private:

		void advanceIterators();

		void populateNumPossibleNodes();

		BoardGrid _current_board{ 3, 3, 2 };
		BoardGrid _board_after_current_move{ 3, 3, 2 };
		size_t _num_possible_nodes = 0;
		int_fast8_t _player_id = 0;

		BoardGrid::Iterator _current_move_iterator = BoardGrid::end();
		BoardGrid::Iterator _current_response_iterator = BoardGrid::end();
	};
}