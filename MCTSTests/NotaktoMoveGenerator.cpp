#include "NotaktoMoveGenerator.h"

#include "MCTSMove.h"
#include "MCTSPossibleMoves.h"

#include "NotaktoPossibleMoves.h"
#include "NotaktoMove.h"

namespace Notakto
{
	std::unique_ptr<MCTS::PossibleMoves> MoveGenerator::possibleMoves() const
	{
		return std::make_unique<Notakto::PossibleMoves>(_grid, _player_id);
	}

	void MoveGenerator::set(const MCTS::MoveGenerator& game_state)
	{
		auto concrete_game_state = dynamic_cast<const Notakto::MoveGenerator&>(game_state);
		_grid = concrete_game_state._grid;
		_player_id = concrete_game_state._player_id;
		_game_over = concrete_game_state._game_over;
	}

	std::unique_ptr<MCTS::MoveGenerator> MoveGenerator::clone() const
	{
		return std::make_unique<Notakto::MoveGenerator>(*this);
	}

	void MoveGenerator::processMove(const MCTS::Move& input)
	{
		const Move& m = dynamic_cast<const Move&>(input);

		_grid.set(m.x(), m.y(), 1);
		if (checkGameOver())
		{
			_game_over = true;
			_loser_player_id = _player_id;
		}
		_player_id = _player_id == 1 ? 2 : 1;
	}

	std::shared_ptr<MCTS::Move> MoveGenerator::generateRandomMove(std::mt19937& rng) 
	{
		auto iterator = _grid.find(0);
		size_t n_empty_points = 0;

		while (iterator != BoardGrid::end())
		{
			++n_empty_points;
			iterator = _grid.findNext(iterator, 0);
		}

		int move_index = rng() % n_empty_points;

		iterator = _grid.find(0);

		int index = 0;
		while (move_index != index)
		{
			iterator = _grid.findNext(iterator, 0);
			++index;
		}

		return std::make_shared<Move>(iterator.x(), iterator.y());
	}


	std::shared_ptr<MCTS::Move> MoveGenerator::defaultMoveWhenNoneFound() const
	{
		return nullptr;
	}

	bool MoveGenerator::gameOver(float& score) const
	{
		if (_game_over) score = _loser_player_id == _requested_player_id ? 0.0f : 1.0f;
		return _game_over;
	}

	bool MoveGenerator::checkGameOver() const
	{
		// Diagonals
		if (_grid.get(1,1) == 1 && _grid.get(0, 0) == 1 && _grid.get(2, 2) == 1) return true;
		if (_grid.get(1,1) == 1 && _grid.get(0, 2) == 1 && _grid.get(2, 0) == 1) return true;

		// Horizontal rows
		if (_grid.get(0, 0) == 1 && _grid.get(0, 1) == 1 && _grid.get(0, 2) == 1) return true;
		if (_grid.get(1, 0) == 1 && _grid.get(1, 1) == 1 && _grid.get(1, 2) == 1) return true;
		if (_grid.get(2, 0) == 1 && _grid.get(2, 1) == 1 && _grid.get(2, 2) == 1) return true;

		// Vertical rows
		if (_grid.get(0, 0) == 1 && _grid.get(1, 0) == 1 && _grid.get(2, 0) == 1) return true;
		if (_grid.get(0, 1) == 1 && _grid.get(1, 1) == 1 && _grid.get(2, 1) == 1) return true;
		if (_grid.get(0, 2) == 1 && _grid.get(1, 2) == 1 && _grid.get(2, 2) == 1) return true;

		return false;
	}

	void MoveGenerator::possibleMoves(std::shared_ptr<MCTS::PossibleMoves>& possible_moves)
	{
		if (possible_moves)
		{
			auto concrete_possible_moves = std::dynamic_pointer_cast<Notakto::PossibleMoves>(possible_moves);
			concrete_possible_moves->set(_grid, _player_id);
		}
		else
		{
			possible_moves = std::make_shared<Notakto::PossibleMoves>(_grid, _player_id);
		}
	}

	void MoveGenerator::print() const
	{
		_grid.printToConsole();
	}

}