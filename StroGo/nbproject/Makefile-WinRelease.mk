#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW-Windows
CND_DLIB_EXT=dll
CND_CONF=WinRelease
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../GoEngine/dist/WinRelease/MinGW-Windows/libgoengine.a ../GtpUtility/dist/WinRelease/MinGW-Windows/libgtputility.a ../GoBoardLogic/dist/WinRelease/MinGW-Windows/libgoboardlogic.a ../Core/dist/WinRelease/MinGW-Windows/libcore.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/strogo.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/strogo.exe: ../GoEngine/dist/WinRelease/MinGW-Windows/libgoengine.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/strogo.exe: ../GtpUtility/dist/WinRelease/MinGW-Windows/libgtputility.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/strogo.exe: ../GoBoardLogic/dist/WinRelease/MinGW-Windows/libgoboardlogic.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/strogo.exe: ../Core/dist/WinRelease/MinGW-Windows/libcore.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/strogo.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/strogo ${OBJECTFILES} ${LDLIBSOPTIONS} -static-libgcc -static-libstdc++

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../GtpUtility -I../GoEngine -I../GoBoardLogic -I../Core -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:
	cd ../GoEngine && ${MAKE}  -f Makefile CONF=WinRelease
	cd ../GtpUtility && ${MAKE}  -f Makefile CONF=WinRelease
	cd ../GoBoardLogic && ${MAKE}  -f Makefile CONF=WinRelease
	cd ../Core && ${MAKE}  -f Makefile CONF=WinRelease

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:
	cd ../GoEngine && ${MAKE}  -f Makefile CONF=WinRelease clean
	cd ../GtpUtility && ${MAKE}  -f Makefile CONF=WinRelease clean
	cd ../GoBoardLogic && ${MAKE}  -f Makefile CONF=WinRelease clean
	cd ../Core && ${MAKE}  -f Makefile CONF=WinRelease clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
