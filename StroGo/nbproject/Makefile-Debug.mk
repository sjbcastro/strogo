#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/ImGuiRenderer.o \
	${OBJECTDIR}/imgui-SFML.o \
	${OBJECTDIR}/imgui.o \
	${OBJECTDIR}/imgui_demo.o \
	${OBJECTDIR}/imgui_draw.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../GoEngine/dist/Debug/GNU-Linux/libgoengine.a ../GtpUtility/dist/Debug/GNU-Linux/libgtputility.a ../GoBoardLogic/dist/Debug/GNU-Linux/libgoboardlogic.a ../Core/dist/Debug/GNU-Linux/libcore.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/strogo

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/strogo: ../GoEngine/dist/Debug/GNU-Linux/libgoengine.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/strogo: ../GtpUtility/dist/Debug/GNU-Linux/libgtputility.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/strogo: ../GoBoardLogic/dist/Debug/GNU-Linux/libgoboardlogic.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/strogo: ../Core/dist/Debug/GNU-Linux/libcore.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/strogo: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/strogo ${OBJECTFILES} ${LDLIBSOPTIONS} -lpthread -lGL -lsfml-graphics -lsfml-window -lsfml-system

${OBJECTDIR}/ImGuiRenderer.o: ImGuiRenderer.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GtpUtility -I../GoEngine -I../GoBoardLogic -I../Core -I../Thirdparty/Json -I../Thirdparty/imgui -I../Thirdparty/imgui-sfml-v.1.0 -I../Thirdparty/SFML-2.5.1/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ImGuiRenderer.o ImGuiRenderer.cpp

${OBJECTDIR}/imgui-SFML.o: imgui-SFML.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GtpUtility -I../GoEngine -I../GoBoardLogic -I../Core -I../Thirdparty/Json -I../Thirdparty/imgui -I../Thirdparty/imgui-sfml-v.1.0 -I../Thirdparty/SFML-2.5.1/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/imgui-SFML.o imgui-SFML.cpp

${OBJECTDIR}/imgui.o: imgui.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GtpUtility -I../GoEngine -I../GoBoardLogic -I../Core -I../Thirdparty/Json -I../Thirdparty/imgui -I../Thirdparty/imgui-sfml-v.1.0 -I../Thirdparty/SFML-2.5.1/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/imgui.o imgui.cpp

${OBJECTDIR}/imgui_demo.o: imgui_demo.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GtpUtility -I../GoEngine -I../GoBoardLogic -I../Core -I../Thirdparty/Json -I../Thirdparty/imgui -I../Thirdparty/imgui-sfml-v.1.0 -I../Thirdparty/SFML-2.5.1/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/imgui_demo.o imgui_demo.cpp

${OBJECTDIR}/imgui_draw.o: imgui_draw.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GtpUtility -I../GoEngine -I../GoBoardLogic -I../Core -I../Thirdparty/Json -I../Thirdparty/imgui -I../Thirdparty/imgui-sfml-v.1.0 -I../Thirdparty/SFML-2.5.1/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/imgui_draw.o imgui_draw.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I../GtpUtility -I../GoEngine -I../GoBoardLogic -I../Core -I../Thirdparty/Json -I../Thirdparty/imgui -I../Thirdparty/imgui-sfml-v.1.0 -I../Thirdparty/SFML-2.5.1/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:
	cd ../GoEngine && ${MAKE}  -f Makefile CONF=Debug
	cd ../GtpUtility && ${MAKE}  -f Makefile CONF=Debug
	cd ../GoBoardLogic && ${MAKE}  -f Makefile CONF=Debug
	cd ../Core && ${MAKE}  -f Makefile CONF=Debug

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:
	cd ../GoEngine && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../GtpUtility && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../GoBoardLogic && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../Core && ${MAKE}  -f Makefile CONF=Debug clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
