#pragma once

#include <thread>
#include <atomic>
#include <memory>
#include <mutex>
#include <utility>
#include "GameState.h"
#include "GoEngineListener.h"
#include "ImGuiConsole.h"
#include "GtpInterface.h"

class ImGuiRenderer : public GoEngineListener
{
public:
    ImGuiRenderer();
    ~ImGuiRenderer();
    
    void gtpInterface(std::shared_ptr<GtpInterface> gtp_interface) 
    { 
        _gtp_interface = gtp_interface; 
        _console.gtpInterface(_gtp_interface);
    }
    
    virtual void pushGameState(const GameState& game_state) override 
    { 
        std::lock_guard<std::recursive_mutex> guard(_game_state_mutex);
        _game_state = std::unique_ptr<GameState>(new GameState(game_state));
    }
    
    virtual void receivedGtpResonse(const std::string& response) override
    {
        _console.receivedGtpResponse(response);
    }
    
private:
    
    void renderLoop();
   
    bool mouseClicked() const;
    
    void gameConfig();
    
    bool executeGTPCommand(const std::string& command);
    
    // Game config variables
    bool _cpu_black_player = false;
    bool _cpu_white_player = true;
    int _board_size = 19;
    int _handicap = 0;
    double komi = 6.5;
    
    // Game state
    bool _blacks_turn = true;
    
    std::thread _imgui_thread;
    std::atomic<bool> _running{false};
    std::unique_ptr<GameState> _game_state;
    std::recursive_mutex _game_state_mutex;
    ExampleAppConsole _console;
    std::shared_ptr<GtpInterface> _gtp_interface;
};