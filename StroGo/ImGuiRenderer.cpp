#include "ImGuiRenderer.h"

#include "imgui.h"
#include "imgui-SFML.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>

#include <SFML/Graphics.hpp>
#include "Utility.h"
#include "PointLetter.h"
#include <math.h>

ImGuiRenderer::ImGuiRenderer()
{
    _imgui_thread = std::thread(&ImGuiRenderer::renderLoop, this);
    _running = true;
}

ImGuiRenderer::~ImGuiRenderer()
{
    _running = false;
    if (_imgui_thread.joinable())
    {
        _imgui_thread.join();
    }
}

void ImGuiRenderer::renderLoop()
{
    sf::RenderWindow window(sf::VideoMode(1960, 1080), "ImGui + SFML = <3");
    window.setFramerateLimit(60);
    ImGui::SFML::Init(window);
    
    sf::Clock deltaClock;
    while (window.isOpen() && _running) {
        sf::Event event;
        while (window.pollEvent(event)) {
            ImGui::SFML::ProcessEvent(event);

            if (event.type == sf::Event::Closed) {
                window.close();
            }
        }
        
        bool open;
        
        bool mouse_clicked = mouseClicked();

        ImGui::SFML::Update(window, deltaClock.restart());
        
        ImGui::ShowTestWindow();
        
        // Is CTRL pressed - reserve for future
        //bool ctrl_is_pressed = ImGui::IsKeyPressed(37);
        
        ImGui::StyleColorsLight();
        
        ImGui::Begin("Board");
        
        ImDrawList* draw_list = ImGui::GetWindowDrawList();
        auto screen_pos = ImGui::GetCursorScreenPos();
        auto window_size = ImGui::GetWindowSize();
        ImGuiIO& io = ImGui::GetIO();
        
        // Start of game state mutex scope
        {
            std::lock_guard<std::recursive_mutex> guard(_game_state_mutex);
            static bool display_influence_map = false;
            std::string influence_inspection;

            const auto& board = _game_state->currentBoard();
            int board_size = static_cast<int>(board.boardState().size());

            const ImU32 black = ImColor(ImColor(0,0,0));
            const ImU32 white = ImColor(ImColor(255,255,255));
            const ImU32 board_colour = ImColor(ImColor(244,215,117));

            int paddingx = 45;
            int paddingy = 65;
            ImVec2 origin(screen_pos.x + paddingx, screen_pos.y + paddingy);
            float overall_size = std::min(window_size.x - 2*paddingx, window_size.y - 2*paddingy);
            float grid_size = overall_size/board_size;

            ImVec2 opposite_corner(screen_pos.x + window_size.x, screen_pos.y + window_size.y);
            draw_list->AddRectFilled(screen_pos, opposite_corner, board_colour, 0, 0);

            float number_offset_x = 21;
            float number_offset_y = 6;
            float letter_offset_x = 2;
            float letter_offset_y = 8;

            for (int i = 0; i < board_size; ++i)
            {
                {
                    float x = i*grid_size + origin.x;
                    float y1 = origin.y;
                    float y2 = (board_size-1)*grid_size + origin.y;
                    draw_list->AddLine(ImVec2(x, y1), ImVec2(x, y2), black, 1.0f);

                    PointLetter letter = static_cast<PointLetter>(i+1);
                    std::string letter_str = Utility::PointLetterAsString(letter);

                    draw_list->AddText(ImVec2(x-letter_offset_x, y2+letter_offset_y), black, letter_str.c_str(), NULL);
                }

                {
                    float y = i*grid_size + origin.y;
                    float x1 = origin.x;
                    float x2 = (board_size-1)*grid_size + origin.x;
                    draw_list->AddLine(ImVec2(x1, y), ImVec2(x2, y), black, 1.0f);

                    draw_list->AddText(ImVec2(x1-number_offset_x, y-number_offset_y), black, std::to_string(i+1).c_str(), NULL);
                }
            }

            for (const auto& group : board.groups())
            {
                for (const auto& stone : group.stones())
                {
                    float stone_size = grid_size * 0.4;
                    float x = stone.i()*grid_size + origin.x;
                    float y = stone.j()*grid_size + origin.y;
                    if (stone.color() == StoneColor::BLACK)
                        draw_list->AddCircleFilled(ImVec2(x, y), stone_size, black, 32);
                    else
                        draw_list->AddCircleFilled(ImVec2(x, y), stone_size, white, 32);
                }
            }

            std::string black_score = "Black : " + std::to_string(board.whiteCapturedStones());
            std::string white_score = "White : " + std::to_string(board.blackCapturedStones());

            draw_list->AddText(ImVec2(origin.x, origin.y-paddingy*5/6), black, black_score.c_str(), NULL);
            draw_list->AddText(ImVec2(origin.x, origin.y-paddingy*3/6), black, white_score.c_str(), NULL);
            
            if (display_influence_map)
            {
                auto influence_info = _game_state->metaData().influence();
                const auto& influence_map = influence_info->influence();

                for (size_t i = 0; i < influence_map.size(); ++i)
                {
                    for (size_t j = 0; j < influence_map[i].size(); ++j)
                    {
                        if (board.boardState()[i][j] == 0)
                        {
                            float x = i*grid_size + origin.x;
                            float y = j*grid_size + origin.y;
                            float rect_factor = 0.3;
                            ImVec2 min_corner(x - grid_size*rect_factor, y - grid_size*rect_factor);
                            ImVec2 max_corner(x + grid_size*rect_factor, y + grid_size*rect_factor);

                            auto val = influence_map[i][j];
                            float opacity = std::abs(val) * 255.0 / influence_info->territoryThreshold();
                            opacity = std::min(opacity, 255.0f);
                            
                            if (val > 0)
                            {
                                draw_list->AddRectFilled(min_corner, max_corner, IM_COL32(0,0,0,opacity));
                            }
                            else
                            {
                                draw_list->AddRectFilled(min_corner, max_corner, IM_COL32(255,255,255,opacity));
                            }
                            
                            float dx = std::abs(io.MousePos.x - x);
                            float dy = std::abs(io.MousePos.y - y);
                            
                            if (dx < grid_size*rect_factor && dy < grid_size*rect_factor)
                            {
                                PointLetter letter = static_cast<PointLetter>(i+1);
                                influence_inspection = Utility::PointLetterAsString(letter);
                                influence_inspection += std::to_string(j+1);
                                influence_inspection += " -> ";
                                influence_inspection += std::to_string(val);
                            }
                        }
                    }
                }
            }
            
            Point point;
            bool valid_move = false;
            if (mouse_clicked)
            {
                for (size_t i = 0; i < board_size; ++i)
                {
                    for (size_t j = 0; j < board_size; ++j)
                    {
                        if (!valid_move)
                        {
                            float x = i*grid_size + origin.x;
                            float y = j*grid_size + origin.y;
                            float rect_factor = 0.3;

                            float dx = std::abs(io.MousePos.x - x);
                            float dy = std::abs(io.MousePos.y - y);

                            if (dx < grid_size*rect_factor && dy < grid_size*rect_factor)
                            {
                                valid_move = true;
                                point = Point{static_cast<PointLetter>(i+1),static_cast<uint8_t>(j+1)};
                            }
                        }
                    }
                }
            }

            if (_blacks_turn)
            {
                if(_cpu_black_player)
                {
                    if (executeGTPCommand("genmove b")) _blacks_turn = false;
                }
                else if (valid_move)
                {
                    std::string command = "play b " + point.asString();
                    if (executeGTPCommand(command)) _blacks_turn = false;
                }
            }
            else
            {
                if (_cpu_white_player)
                {
                    if (executeGTPCommand("genmove w")) _blacks_turn = true;
                }
                else if (valid_move)
                {
                    std::string command = "play w " + point.asString();
                    if (executeGTPCommand(command)) _blacks_turn = true;
                }
            }
            
            ImGui::End();
        
            ImGui::Begin("Config");
            
            if (ImGui::CollapsingHeader("Engine config"))
            {
                static int search_depth = 2;
                static int tree_index = 0;
                
                ImGui::Text("Tree Search Type:");
                ImGui::Indent();
                ImGui::RadioButton("Alpha beta", &tree_index, 0);
                ImGui::SameLine();
                ImGui::RadioButton("Brute force", &tree_index, 1);
                ImGui::Unindent();
                
                ImGui::InputInt("Search depth", &search_depth);
                
                if (ImGui::Button("Apply"))
                {
                    if (search_depth < 0) search_depth = 0;
                    
                    GameEngineProperties properties;
                    properties.treeSearchDepth(static_cast<uint32_t>(search_depth));
                    switch (tree_index)
                    {
                        case 0:
                        {
                            properties.treeSearchType(TreeSearchType::ALPHA_BETA);
                            break;
                        }
                        case 1:
                        {
                            properties.treeSearchType(TreeSearchType::BRUTE_FORCE);
                            break;
                        }
                    }
                    
                    _gtp_interface->setGameEngineProperties(properties);
                }
            }

            if (ImGui::CollapsingHeader("Engine debug"))
            {
                ImGui::Checkbox("Display influence map", &display_influence_map);
            }
            
            if (!influence_inspection.empty())
            {
                ImGui::Text("%s", influence_inspection.c_str());
            }
            
            ImGui::End();
            
            gameConfig();
        } // End of game state mutex scope
        
        _console.Draw("Example: Console", &open);
        
        window.clear(sf::Color(255, 255, 255, 255));
        ImGui::SFML::Render(window);
        window.display();
    }

    ImGui::SFML::Shutdown();
    
    executeGTPCommand("quit");
}

void ImGuiRenderer::gameConfig()
{
    ImGui::Begin("Game Config");
    
    ImGui::Checkbox("Black CPU", &_cpu_black_player);
    ImGui::Checkbox("White CPU", &_cpu_white_player);
    
    std::vector<int> available_sizes = {9, 13, 19};
    
    static int size_index = 0;
    
    for (int i = 0; i < available_sizes.size(); ++i)
    {
        if (i > 0)
        {
            ImGui::SameLine();
        }
        
        std::string msg = std::to_string(available_sizes[i]);
        ImGui::RadioButton(msg.c_str(), &size_index, i);
    }
    
    if (ImGui::Button("New Game"))
    {
        std::string command = "boardsize " + std::to_string(available_sizes[size_index]);
        executeGTPCommand(command);
        _blacks_turn = true;
    }
    
    ImGui::End();
}


bool ImGuiRenderer::mouseClicked() const
{
    ImGuiIO& io = ImGui::GetIO();
    for (int i = 0; i < IM_ARRAYSIZE(io.MouseDown); i++)
    {
        if (ImGui::IsMouseClicked(i))          
        {
            if (ImGui::IsMousePosValid())
            {
                return true;
            }
        }
    }  
}

bool ImGuiRenderer::executeGTPCommand(const std::string& command)
{
    _console.AddLog("%s", command.c_str());
    std::string response = _gtp_interface->executeGtpCommand(command);
    return response.find('=') != std::string::npos;
}