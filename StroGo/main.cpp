#include <iostream>
#include "GtpInterface.h"
#include "EngineMoveLogger.h"
#include "GoEngineLogger.h"
#include "SgfInfluenceLogger.h"
#include "ImGuiRenderer.h"
#include <chrono>
#include <thread>

int main(int argc, char** argv)
{
    std::string arg1 = argc > 1 ? argv[1] : "";
    bool gui = arg1 == "--gui";
    
    std::shared_ptr<ImGuiRenderer> renderer = gui ? std::shared_ptr<ImGuiRenderer>(new ImGuiRenderer()) : nullptr;
    auto interface = std::shared_ptr<GtpInterface>(new GtpInterface(renderer));
    
    if (renderer) renderer->gtpInterface(interface);
    
    if (!gui)
    {
        std::vector<std::string> args;
        for (int i = 1; i < argc; ++i)
        {
            args.push_back(argv[i]);
        }
        
        GameEngineProperties properties(args);
        interface->setGameEngineProperties(properties);
    }

    EngineMoveLogger::instance()->enable();
    GoEngineLogger::instance()->enable();

    while(!interface->quitRequested())
    {
        if (gui)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
        else
        {
            std::string input = "";
            std::getline(std::cin, input);
            if (!input.empty())
            {
                std::cout << interface->executeGtpCommand(input);
            }
        }
    }

    return 0;
}
