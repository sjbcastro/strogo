#pragma once

#include <string>
#include "Stone.h"
#include <vector>

class GtpCommand
{
public:
    virtual std::string commandName() const = 0;
};

class ProtocolVersionCommand : public GtpCommand
{
public:
    virtual std::string commandName() const override { return ProtocolVersionCommand::staticCommandName(); }
    static std::string staticCommandName() { return "protocol_version"; }
};

class EngineVersionCommand : public GtpCommand
{
public:
    virtual std::string commandName() const override { return EngineVersionCommand::staticCommandName(); }
    static std::string staticCommandName() { return "version"; }
};

class NameCommand : public GtpCommand
{
public:
    virtual std::string commandName() const override { return NameCommand::staticCommandName(); }
    static std::string staticCommandName() { return "name"; }
};

class PlayCommand : public GtpCommand
{
public:
    PlayCommand(const std::vector<std::string>& args);

    GoLogic::Move move() const { return _move; }

    virtual std::string commandName() const override { return PlayCommand::staticCommandName(); }
    static std::string staticCommandName() { return "play"; }

private:
    GoLogic::Move _move;
};

class SetFreeHandicapCommand : public GtpCommand
{
public:
    SetFreeHandicapCommand(const std::vector<std::string>& args);

    virtual std::string commandName() const override { return SetFreeHandicapCommand::staticCommandName(); }
    static std::string staticCommandName() { return "set_free_handicap"; }
    
    const std::vector<GoLogic::Stone>& handicapStones() const { return _handicap_stones; }
    bool successfullyParsed() const { return _successfully_parsed; }

private:
    std::vector<GoLogic::Stone> _handicap_stones;
    bool _successfully_parsed = false;
};

class NumericalCommand : public GtpCommand
{
public:
    NumericalCommand(const std::vector<std::string>& args);

    bool successfullyParsed() const { return _successfully_parsed; }
    double value() const { return _value; }

private:
    double _value = 0;
    bool _successfully_parsed = false;
};

class SetFixedHandicapCommand : public NumericalCommand
{
public:
    SetFixedHandicapCommand(const std::vector<std::string>& args) : NumericalCommand(args) {}

    uint32_t numberOfHandicapStones() const { return static_cast<uint32_t>(value()); }
    
    virtual std::string commandName() const override { return SetFixedHandicapCommand::staticCommandName(); }
    static std::string staticCommandName() { return "fixed_handicap"; }
};

class KomiCommand : public NumericalCommand
{
public:
    KomiCommand(const std::vector<std::string>& args) : NumericalCommand(args) {}

    virtual std::string commandName() const override { return KomiCommand::staticCommandName(); }
    static std::string staticCommandName() { return "komi"; }
};

class BoardsizeCommand : public NumericalCommand
{
public:
    BoardsizeCommand(const std::vector<std::string>& args) : NumericalCommand(args) {}

    virtual std::string commandName() const override { return BoardsizeCommand::staticCommandName(); }
    static std::string staticCommandName() { return "boardsize"; }
};

class ClearBoardCommand : public GtpCommand
{
public:
    virtual std::string commandName() const override { return ClearBoardCommand::staticCommandName(); }
    static std::string staticCommandName() { return "clear_board"; }
};

class QuitCommand : public GtpCommand
{
public:
    virtual std::string commandName() const override { return QuitCommand::staticCommandName(); }
    static std::string staticCommandName() { return "quit"; }
};

class ListCommands : public GtpCommand
{
public:
    virtual std::string commandName() const override { return ListCommands::staticCommandName(); }
    static std::string staticCommandName() { return "list_commands"; }
};

class KnownCommand : public GtpCommand
{
public:

    KnownCommand(const std::vector<std::string>& args);

    virtual std::string commandName() const override { return KnownCommand::staticCommandName(); }
    static std::string staticCommandName() { return "known_command"; }

    std::string commandToFind() const { return _command; }

private:
    std::string _command;
};

class GenmoveCommand : public GtpCommand
{
public:
    GenmoveCommand(const std::vector<std::string>& args);

    GoLogic::StoneColor color() const { return _color; }
    bool successfullyParsed() const { return _successfully_parsed; }

    virtual std::string commandName() const override { return GenmoveCommand::staticCommandName(); }
    static std::string staticCommandName() { return "genmove"; }

private:
    GoLogic::StoneColor _color;
    bool                _successfully_parsed = false;
};

class RegGenmoveCommand : public GenmoveCommand
{
public:
    RegGenmoveCommand(const std::vector<std::string>& args) : GenmoveCommand(args) {}
    
    virtual std::string commandName() const override { return RegGenmoveCommand::staticCommandName(); }
    static std::string staticCommandName() { return "reg_genmove"; }
};

class UnknownCommand : public GtpCommand
{
public:
    virtual std::string commandName() const override { return UnknownCommand::staticCommandName(); }
    static std::string staticCommandName() { return "unknown_command"; }
};
