#include "GtpCommands.h"
#include "Utility.h"
#include <algorithm>

PlayCommand::PlayCommand(const std::vector<std::string>& args)
{
    assert( args.size() >= 2);
    if ( args.size() >= 2 )
    {
        std::string color_str = args[0];
        std::string play_move = args[1];

        // Convert to upper case
        std::transform(color_str.begin(), color_str.end(), color_str.begin(), ::toupper);
        std::transform(play_move.begin(), play_move.end(), play_move.begin(), ::toupper);

        GoLogic::StoneColor color = GoLogic::StoneColor::BLACK;

        if ( color_str == "W" || color_str == "WHITE" )
        {
            color = GoLogic::StoneColor::WHITE;
        }
        else if ( color_str == "B" || color_str == "BLACK" )
        {
            color = GoLogic::StoneColor::BLACK;
        }
        else
        {
            return;
        }

        if ( play_move == "PASS" )
        {
            _move.pass(color);
        }
        else
        {
            try
            {
                GoLogic::PointLetter letter = GoLogic::Utility::PointLetterFromString(play_move.substr(0,1));
                int number = stoi(play_move.substr(1,play_move.size()-1));
                _move = GoLogic::Move(letter, number, color);
            }
            catch(...)
            {
                _move = GoLogic::Move(GoLogic::PointLetter::INVALID, 0, color);
            }
        }
    }
}

SetFreeHandicapCommand::SetFreeHandicapCommand(const std::vector<std::string>& args)
{
    _successfully_parsed = true;
    for (const std::string& arg : args)
    {
        std::string stone_string = arg;
        std::transform(stone_string.begin(), stone_string.end(), stone_string.begin(), ::toupper);
        
        try
        {
            GoLogic::PointLetter letter = GoLogic::Utility::PointLetterFromString(stone_string.substr(0,1));
            int number = stoi(stone_string.substr(1,stone_string.size()-1));
            _handicap_stones.emplace_back(letter, number, GoLogic::StoneColor::BLACK);
        }
        catch(...)
        {
            assert(false);
            _successfully_parsed = false;
        }
    }
}

NumericalCommand::NumericalCommand(const std::vector<std::string>& args)
{
    assert( args.size() >= 1);
    if (args.size() >= 1)
    {
        try
        {
            _value = stod(args[0]);
            _successfully_parsed = true;
        }
        catch(...)
        {
            _successfully_parsed = false;
        }
    }
}


KnownCommand::KnownCommand(const std::vector<std::string>& args)
{
    assert(args.size() >= 1);
    _command = args[0];
}

GenmoveCommand::GenmoveCommand(const std::vector<std::string>& args)
{
    if ( args.empty() )
        return;

    std::string color_str = args[0];

    // Convert to upper case
    std::transform(color_str.begin(), color_str.end(), color_str.begin(), ::toupper);

    if ( color_str == "W" || color_str == "WHITE" )
    {
        _color = GoLogic::StoneColor::WHITE;
        _successfully_parsed = true;
    }
    else if ( color_str == "B" || color_str == "BLACK" )
    {
        _color = GoLogic::StoneColor::BLACK;
        _successfully_parsed = true;
    }
    else
    {
        _successfully_parsed = false;
    }
}
