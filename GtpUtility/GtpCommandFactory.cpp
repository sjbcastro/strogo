#include "GtpCommandFactory.h"
#include <algorithm>

GtpCommandFactory::GtpCommandFactory()
{
    // This list of commands are the ones which are publicly accessible
    // Command should only be provided in a release if they have been thoroughly tested
    // and are known to work

    _command_list.push_back(ProtocolVersionCommand::staticCommandName());
    _command_list.push_back(NameCommand::staticCommandName());
    _command_list.push_back(EngineVersionCommand::staticCommandName());
    _command_list.push_back(PlayCommand::staticCommandName());
    _command_list.push_back(KomiCommand::staticCommandName());
    _command_list.push_back(BoardsizeCommand::staticCommandName());
    _command_list.push_back(ClearBoardCommand::staticCommandName());
    _command_list.push_back(QuitCommand::staticCommandName());
    _command_list.push_back(ListCommands::staticCommandName());
    _command_list.push_back(KnownCommand::staticCommandName());
    _command_list.push_back(GenmoveCommand::staticCommandName());
    _command_list.push_back(RegGenmoveCommand::staticCommandName());
    _command_list.push_back(SetFreeHandicapCommand::staticCommandName());
    _command_list.push_back(SetFixedHandicapCommand::staticCommandName());

    std::sort(_command_list.begin(), _command_list.end());
}

std::unique_ptr<GtpCommand> GtpCommandFactory::createGtpCommand(
    const std::string& command,
    const std::vector<std::string>& args) const
{
    // Check command exists within our command list
    // This is so that known_command and list_commands are consistent
    if ( std::find(_command_list.begin(), _command_list.end(), command) == _command_list.end() )
        return std::unique_ptr<GtpCommand>(new UnknownCommand());

    if ( command == ProtocolVersionCommand::staticCommandName() )
        return std::unique_ptr<GtpCommand>(new ProtocolVersionCommand());
    else if ( command == NameCommand::staticCommandName() )
        return std::unique_ptr<GtpCommand>(new NameCommand());
    else if ( command == EngineVersionCommand::staticCommandName() )
        return std::unique_ptr<GtpCommand>(new EngineVersionCommand());
    else if ( command == PlayCommand::staticCommandName() )
        return std::unique_ptr<GtpCommand>(new PlayCommand(args));
    else if ( command == KomiCommand::staticCommandName() )
        return std::unique_ptr<GtpCommand>(new KomiCommand(args));
    else if ( command == BoardsizeCommand::staticCommandName() )
        return std::unique_ptr<GtpCommand>(new BoardsizeCommand(args));
    else if ( command == ClearBoardCommand::staticCommandName() )
        return std::unique_ptr<GtpCommand>(new ClearBoardCommand());
    else if ( command == QuitCommand::staticCommandName() )
        return std::unique_ptr<GtpCommand>(new QuitCommand());
    else if ( command == ListCommands::staticCommandName() )
        return std::unique_ptr<GtpCommand>(new ListCommands());
    else if ( command == KnownCommand::staticCommandName() )
        return std::unique_ptr<GtpCommand>(new KnownCommand(args));
    else if ( command == GenmoveCommand::staticCommandName() )
        return std::unique_ptr<GtpCommand>(new GenmoveCommand(args));
    else if ( command == RegGenmoveCommand::staticCommandName() )
        return std::unique_ptr<GtpCommand>(new RegGenmoveCommand(args));
    else if ( command == SetFreeHandicapCommand::staticCommandName() )
        return std::unique_ptr<GtpCommand>(new SetFreeHandicapCommand(args));
    else if ( command == SetFixedHandicapCommand::staticCommandName() )
        return std::unique_ptr<GtpCommand>(new SetFixedHandicapCommand(args));
    else
        return std::unique_ptr<GtpCommand>(new UnknownCommand());
}
