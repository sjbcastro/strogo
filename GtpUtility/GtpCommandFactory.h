#include <string>
#include <vector>
#include "GtpCommands.h"
#include <memory>

class GtpCommandFactory
{
public:
    GtpCommandFactory();

    std::unique_ptr<GtpCommand> createGtpCommand(
        const std::string& command,
        const std::vector<std::string>& args) const;

    std::vector<std::string> commandList() const { return _command_list; }

private:
    std::vector<std::string> _command_list;
};
