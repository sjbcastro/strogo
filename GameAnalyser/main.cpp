#include <iostream>
#include "GameController.h"
#include "SgfInfluenceLogger.h"
#include "SgfLooseGroupsLogger.h"
#include "SgfBouzyMapLogger.h"
#include "SgfTerritoryLogger.h"
#include "Properties.h"
#include "SgfReader.h"
#include "Utility.h"

int main()
{
    SgfReader reader;
    SimpleSgfRecord record = reader.readSimpleSgfFile("sgf/sample.sgf");

    SgfInfluenceLogger::instance()->enable();
    SgfInfluenceLogger::instance()->setKomi(record.komi());
    SgfInfluenceLogger::instance()->setBoardSize(record.boardSize());

    SgfTerritoryLogger::instance()->enable();
    SgfTerritoryLogger::instance()->setKomi(record.komi());
    SgfTerritoryLogger::instance()->setBoardSize(record.boardSize());
    
    GameController controller(Ruleset::CHINESE, SuperkoRuleset::PSK);
    controller.boardsize(record.boardSize());

    for (auto& sgf_move : record.moveHistory())
    {
        std::cout << Utility::MoveAsString(sgf_move) << "\t" << controller.makeMove(sgf_move) << std::endl;
        //controller.generateMove(StoneColor::BLACK, false);
        //controller.generateDebugInfo();
    }

    return 0;
}
