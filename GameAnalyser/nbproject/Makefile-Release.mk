#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../GoEngine/dist/Debug/GNU-Linux/libgoengine.a ../GoBoardLogic/dist/Debug/GNU-Linux/libgoboardlogic.a ../Core/dist/Debug/GNU-Linux/libcore.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/gameanalyser

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/gameanalyser: ../GoEngine/dist/Debug/GNU-Linux/libgoengine.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/gameanalyser: ../GoBoardLogic/dist/Debug/GNU-Linux/libgoboardlogic.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/gameanalyser: ../Core/dist/Debug/GNU-Linux/libcore.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/gameanalyser: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/gameanalyser ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Werror -I../Core -I../GoEngine -I../GoBoardLogic -I../Thirdparty/Json -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:
	cd ../GoEngine && ${MAKE}  -f Makefile CONF=Debug
	cd ../GoBoardLogic && ${MAKE}  -f Makefile CONF=Debug
	cd ../Core && ${MAKE}  -f Makefile CONF=Debug

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:
	cd ../GoEngine && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../GoBoardLogic && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../Core && ${MAKE}  -f Makefile CONF=Debug clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
