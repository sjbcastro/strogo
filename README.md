A hobby project written in C++ to play the game of Go. Currently functions through a combination of Monte Carlo Tree Search (MCTS) and heuristics. Very much a work in progress!
