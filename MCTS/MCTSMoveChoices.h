#pragma once

#include "MCTSMove.h"

#include <vector>
#include <utility>
#include <memory>

namespace MCTS
{
    // The move choices that the current player has
    class MoveChoices
    {
    public:

        class Item
        {
        public:
            Item(std::shared_ptr<Move> the_move, float score, int index)
                : _move(the_move)
                , _score(score)
                , _index(index)
            {}

            void score(float val) { _score = val; }
            void index(int val) { _index = val; }

            float score() const { return _score; }
            int index() const { return _index; }
            std::shared_ptr<Move> theMove() const { return _move; }

        private:
            std::shared_ptr<Move> _move = nullptr;
            float _score = 0.0;
            int _index = -1;
        };

        void set(std::shared_ptr<Move> the_move, float score, int index)
        {
            for (auto& item : _items)
            {
                if (item.theMove()->isEqual(*the_move))
                {
                    item.score(score);
                    item.index(index);
                    return;
                }
            }
            _items.emplace_back(the_move, score, index);
        }

        void clear() { _items.clear(); }
        size_t size() const { return _items.size(); }

        std::vector<Item>::iterator begin() { return _items.begin(); }
        std::vector<Item>::iterator end() { return _items.end(); }

    private:

        std::vector<Item> _items;
    };
}