#pragma once

#include <vector>
#include <memory>
#include <random>

namespace MCTS
{
    class Move;
    class PossibleMoves;

    // Abstract class which represents the game state at any given point. This provides enough information for the MCTS algorithm to simulate random playouts, determine
    // the result and ultimately iterate over and over to pick the next move
    class MoveGenerator
    {
    public:

        // Returns the possible moves for the current game state
        virtual std::unique_ptr<PossibleMoves> possibleMoves() const = 0;

        // Populates the possible moves for the current game state. Basically the same as the other overload but will overwrite existing memory.
        virtual void possibleMoves(std::shared_ptr<PossibleMoves>&) = 0;

        // Used to initialise the current instance based on the input
        virtual void set(const MoveGenerator&) = 0;

        // Clones the current instance
        virtual std::unique_ptr<MoveGenerator> clone() const = 0;

        // Takes the current move set and advances the game state
        virtual void processMove(const Move&) = 0;

        virtual std::shared_ptr<Move> generateRandomMove(std::mt19937& rng) = 0;

        // This is called in the event that the tree search is empty i.e. there are no legal moves to make. As I recall this was an edge case that would crop up in Battlesnake
        // when it was cornered, and for some games probably won't ever happen (e.g. by the time this case is reached for Tic-Tac-Toe, the game is already over)
        virtual std::shared_ptr<Move> defaultMoveWhenNoneFound() const = 0;

        // Returns true if the game is over, and returns a score via the output argument
        virtual bool gameOver(float&) const = 0;

        // Prints the board for debugging purposes
        virtual void print() const {}

        virtual bool nextMoveIsMainPlayer() const = 0;
    };
}
