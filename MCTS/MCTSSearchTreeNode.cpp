#include "MCTSSearchTreeNode.h"
#include "MCTSNodePool.h"

#include <cassert>

namespace MCTS
{
    void SearchTreeNode::updateScores(float score)
    {
        _total_wins += score;
        ++_total_games;

        if (_total_wins > _total_games)
        {
            std::string msg = "Total wins " + std::to_string(_total_wins) +
                " is greater than total games " + std::to_string(_total_games);
            throw std::runtime_error(msg);
        }
    }

    void SearchTreeNode::backPropagate(float score)
    {
        updateScores(score);
        SearchTreeNode* current_node = this;

        while (current_node && current_node->_parent_node)
        {
            current_node = current_node->_parent_node;
            current_node->updateScores(score);
        }
    }

    void SearchTreeNode::reachedTerminalNode(float score)
    {
        _result = score;
        _no_more_moves = true;
    }

    std::list<SearchTreeNode>::iterator SearchTreeNode::find(std::shared_ptr<Move> move_made)
    {
        for (auto it = _child_nodes.begin(); it != _child_nodes.end(); ++it)
        {
            if (it->_move_made->isEqual(*move_made))
            {
                return it;
            }
        }

        return _child_nodes.end();
    }

    bool SearchTreeNode::nodeFullyExpanded(const PossibleMoves& possible_moves) const
    {
        size_t n_expected_nodes = possible_moves.numPossibleNodes();
        return _child_nodes.size() == n_expected_nodes;
    }

    std::list<SearchTreeNode>::iterator SearchTreeNode::addNode(std::shared_ptr<Move> move_made, std::shared_ptr<PossibleMoves> possible_moves, NodePool& node_pool)
    {
        if (possible_moves) _num_possible_children = possible_moves->numPossibleNodes();

        if (!node_pool.getAllocatedNode(_child_nodes))
        {
            _child_nodes.emplace_back(this, move_made);
        }
        else
        {
            _child_nodes.back().parent(this);
            _child_nodes.back()._move_made = move_made;
        }

        auto last_it = _child_nodes.end();
        return --last_it;
    }

    std::list<SearchTreeNode>::iterator SearchTreeNode::expandNode(PossibleMoves& possible_moves, NodePool& node_pool)
    {
        bool done = false;

        std::shared_ptr<Move> move_made = possible_moves.getFirstMove();
        _num_possible_children = possible_moves.numPossibleNodes();

        for (int i = 0; i < _num_possible_children; ++i)
        {
            auto it = find(move_made);
            if (it == _child_nodes.end())
            {
                if (!node_pool.getAllocatedNode(_child_nodes))
                {
                    _child_nodes.emplace_back(this, move_made);
                }
                else
                {
                    _child_nodes.back().parent(this);
                    _child_nodes.back()._move_made = move_made;
                }

                auto last_it = _child_nodes.end();
                return --last_it;
            }

            bool found_next = possible_moves.getNextMove(move_made);
            assert(found_next);
        }

        return _child_nodes.end();
    }

}