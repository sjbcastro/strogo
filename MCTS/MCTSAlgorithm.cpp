#include "MCTSAlgorithm.h"
#include "MCTSMove.h"
#include "MCTSMoveGenerator.h"
#include "MCTSMoveChoices.h"
#include "MCTSTreeSearchLogger.h"
#include "Timer.h"

#include <sstream>
#include <cassert>

constexpr bool DEBUG_LOG = true;

namespace MCTS
{
    std::shared_ptr<Move> Algorithm::findOptimalMove(const MoveGenerator& input, float time_allowed_ms)
    {
        bool restart_search = true;
        Timer timer;

        if (restart_search)
        {
            _tree_search = SearchTreeNode{};
            _node_pool.returnAllocatedNodes(_tree_search);
        }

        int simulations = 0, evaluations = 0;

        auto game_state = input.clone();

        MoveChoices move_choices;
        std::shared_ptr<PossibleMoves> possible_moves = input.possibleMoves();

        while (timer.elapsedMilliseconds() < time_allowed_ms) 
        {
            // start with root node
            SearchTreeNode* current_node = &_tree_search;

            // Start with the root board
            game_state->set(input);

            if (current_node->noMoreMoves())
            {
                TreeSearchLogger::instance()->writeLine("Exhausted the tree search");
                break;
            }

            // Stage 1 : selection
            // Find leaf node - a node which has not been explored yet
            while (!current_node->empty() && !current_node->noMoreMoves())
            {
                bool next_move_is_main_player = game_state->nextMoveIsMainPlayer();
                game_state->possibleMoves(possible_moves);

                if (current_node->nodeFullyExpanded(*possible_moves))
                {
                    std::list<SearchTreeNode>::iterator next_node = selectNextNode(current_node, next_move_is_main_player, move_choices);

                    // Update the current node
                    current_node = &(*next_node);
                }
                else
                {
                    auto next_node = current_node->expandNode(*possible_moves, _node_pool);
                    current_node = &(*next_node);
                }

                // Update the board
                game_state->processMove(*current_node->moveMade());

                float score;
                if (game_state->gameOver(score))
                {
                    current_node->reachedTerminalNode(score);
                }
            }

            if (!current_node->noMoreMoves())
            {
                // Stage 2: expansion
                //auto possible_moves = board.possibleMoves();
                //current_node->expandNode(possible_moves, _node_pool);

                // Stage 3: simulation
                _simulator.simulatePlayout(*game_state, current_node, _node_pool);

                ++simulations;
            }

            // Stage 4: back-propagation - do this for terminal nodes as well
            current_node->backPropagate(current_node->result());

            ++evaluations;
        }

        if (_tree_search.empty())
        {
            return game_state->defaultMoveWhenNoneFound();
        }

        if (DEBUG_LOG)
        {
            TreeSearchLogger::instance()->writeLine("Simulations = " + std::to_string(simulations));
            TreeSearchLogger::instance()->writeLine("Evaluations = " + std::to_string(evaluations));

            _simulator.logAndResetStats();
        }

        auto next_move = getNextMoveFromTree(move_choices);

        if (next_move)
        {
            TreeSearchLogger::instance()->writeLine("Making move - " + next_move->toString());
        }
        else
        {
            // throw
        }

        return next_move;
    }

    std::list<SearchTreeNode>::iterator Algorithm::selectNextNode(SearchTreeNode* current_node, bool next_move_is_main_player, MoveChoices& move_choices) const
    {
        move_choices.clear();

        int index = 0;
        bool found_empty_node = false;

        // Keep using UCT until we find a leaf node
        for (auto it = current_node->begin(); it != current_node->end(); ++it, ++index)
        {
            float w = static_cast<float>(it->totalWins());
            float n = static_cast<float>(it->totalGames());
            float N = static_cast<float>(current_node->totalGames());

            float uct_value;
            if (n == 0)
            {
                found_empty_node = true;
                break;
            }
            else
            {
                float log_N = std::log(N);// _lookup_table.log(N);
                if (next_move_is_main_player)
                {
                    uct_value = (w / n) + _exploration_parameter * sqrt(log_N / n);
                }
                else
                {
                    uct_value = 1.0f - (w / n) + _exploration_parameter * sqrt(log_N / n);
                }
            }

            auto my_move = it->moveMade();

            if (!my_move)
            {
                throw std::runtime_error("nullptr");
            }

            move_choices.set(my_move, uct_value, index);
        }

        int next_node_index = 0;

        if (found_empty_node)
        {
            next_node_index = index;
        }
        else
        {
            float best_score = move_choices.begin()->score();
            next_node_index = move_choices.begin()->index();

            for (const auto& move_choice : move_choices)
            {
                if (move_choice.score() > best_score)
                {
                    best_score = move_choice.score();
                    next_node_index = move_choice.index();
                }
            }
        }

        auto next_node = current_node->begin();
        next_node = std::next(next_node, next_node_index);

        if (next_node == current_node->end())
        {
            std::ostringstream msg;
            msg << "Next node beyond range, next_node_index = " << next_node_index
                << " vs size = " << current_node->size() << std::endl;
            msg << "found empty node = " << found_empty_node << std::endl;
            msg << "move_choices size = " << move_choices.size();

            throw std::runtime_error(msg.str());
        }

        return next_node;
    }

    std::shared_ptr<Move> Algorithm::getNextMoveFromTree(MoveChoices& move_choices)
    {
        move_choices.clear();

        int index = 0;
        for (auto it = _tree_search.begin(); it != _tree_search.end(); ++it, ++index)
        {
            float w = static_cast<float>(it->totalWins());
            float n = static_cast<float>(it->totalGames());

            float value = (w / n);

            auto my_move = it->moveMade();

            if (my_move)
            {
                if (DEBUG_LOG)
                {
                    std::string log_str = std::to_string(index) + " : " + my_move->toString() + "\t" + std::to_string(w) + "\t " + std::to_string(n);
                    TreeSearchLogger::instance()->writeLine(log_str);
                }
                
                move_choices.set(my_move, value, index);
            }
            else
            {
                // throw
            }
        }

        float best_score = move_choices.begin()->score();
        int next_node_index = move_choices.begin()->index();

        for (const auto& move_choice : move_choices)
        {
            //if (DEBUG_LOG) _output_stream << move_choice.index() << " : " << ToString(move_choice.direction()) << " " << move_choice.score() << std::endl;

            if (move_choice.score() > best_score)
            {
                best_score = move_choice.score();
                next_node_index = move_choice.index();
            }
        }

        auto next_node = _tree_search.begin();
        next_node = std::next(next_node, next_node_index);

        auto next_move = next_node->moveMade();

        return next_move;
    }
}

