#pragma once

#include <random>
#include <unordered_map>
#include <vector>
#include <memory>
#include "MCTSSearchTreeNode.h"
#include "MCTSPossibleMoves.h"

namespace MCTS
{
    class MoveGenerator;
    class NodePool;

    // Class responsible for simulating random playouts until an end condition is met
    class Simulator
    {
    public:
        Simulator();

        // Simulates a random playout until an end condition is met; the result is then propagated back up to the parent node, and each node along the way is updated
        // in terms of it's total plays and wins
        void simulatePlayout(const MoveGenerator& input, SearchTreeNode*& current_node, NodePool& node_pool);

        // setter for the threshold
        void setThresholdForAbandoningSimulation(int threshold) { _threshold_for_abandoning_simulation = threshold; }

        // logs and resets the simulation stats
        void logAndResetStats();

    private:
        // Random number generator
        std::unique_ptr<std::mt19937> _rng;

        // Variables used during simulated playouts. These are memory resident and reset each time simulatePlayout is called, to avoid repetetive memory allocations
        std::shared_ptr<PossibleMoves> _possible_moves;
        std::unique_ptr<MoveGenerator> _game_state;

        // The point at which we know a simulation has gone off the rails and isn't going to produce a meaningful result. This is context specific, but has a high predefined
        // value as a fail safe (prior to this, things were very confusing when a simulation disappeared down a rabbit hole)
        int _threshold_for_abandoning_simulation = 100000;

        // Various statistics

        // How many times we reached the threshold for abandoning a simulation because it was taking too long
        int _n_incomplete_simulations = 0;
        // How many simulations we did
        int _n_simulations = 0;
        // What the average simulation was (ignoring those that were abandoned)
        double _average_simulation_length = 0.0;
    };

}