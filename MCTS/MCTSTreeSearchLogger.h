#pragma once

#include "Logger.h"
#include "Singleton.h"

namespace MCTS
{
	class TreeSearchLogger : public Logger, public Singleton<TreeSearchLogger>
	{
	public:
		virtual std::string filename() const override { return "MCTS Tree search.txt"; }
	};
}