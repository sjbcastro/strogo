#pragma once

#include <list>
#include <iostream>
#include <string>
#include <exception>
#include "MCTSPossibleMoves.h"
#include "MCTSMove.h"

namespace MCTS
{
    class NodePool;

    // Represents an individual node in the MCTS search tree. It is designed to be as lightweight as possible, and therefore stores various bits of meta data in lieu
    // of the full blown game state or even the possible moves.
    class SearchTreeNode
    {
    public:

        SearchTreeNode() = default;
        SearchTreeNode(const SearchTreeNode&) = default;
        SearchTreeNode& operator=(const SearchTreeNode&) = default;

        SearchTreeNode(SearchTreeNode* parent, std::shared_ptr<Move> move_made)
            : _parent_node(parent)
            , _move_made(move_made)
        {
        }

        // setters
        void parent(SearchTreeNode* parent) { _parent_node = parent; }
        void setNoMoreMoves() { _no_more_moves = true; }

        // mostly trivial getters
        bool empty() const { return _child_nodes.empty(); }
        int64_t totalGames() const { return _total_games; }
        float totalWins() const { return _total_wins; }
        bool noMoreMoves() const { return _no_more_moves; }
        std::list<SearchTreeNode>::iterator begin() { return _child_nodes.begin(); }
        std::list<SearchTreeNode>::iterator end() { return _child_nodes.end(); }
        size_t size() { return _child_nodes.size(); }
        float result() const { return _result; }
        bool parent() { return _parent_node != nullptr; }
        std::shared_ptr<Move> moveMade() { return _move_made; }

        // Method called for when we have reached the final stage of a MCTS iteration (not necessarily having done a simulation) and we want to propagate the result
        // back up to the top-level parent node
        void backPropagate(float score);

        // Called when we reach a game over stage during simulation - sets the result and _no_more_moves boolean
        void reachedTerminalNode(float score);

        // Finds a child node associated with the input move set
        std::list<SearchTreeNode>::iterator find(std::shared_ptr<Move> moves_made);

        // Compares the input num possible moves with the internal maximum to determine if we have fully expanded the node (i.e. populated all possible children)
        // This "lazy" expansion avoids memory explosions for expanded nodes we will never explore
        bool nodeFullyExpanded(const PossibleMoves& possible_moves) const;

        // Expands the current node by adding a node for the next non-existant possible move. Called during the expansion phase of MCTS
        std::list<SearchTreeNode>::iterator expandNode(PossibleMoves& possible_moves, NodePool& node_pool);

        // Similar to expandNode, but this is called during simulation
        std::list<SearchTreeNode>::iterator addNode(std::shared_ptr<Move> move_made, std::shared_ptr<PossibleMoves> possible_moves, NodePool& node_pool);

    private:

        // Helper method to update the internal score and number of plays
        void updateScores(float score);

        friend class NodePool;

        // Raw pointer - parents own children not the other way round, so there is no memory managemnt
        // going on here
        SearchTreeNode* _parent_node = nullptr;

        std::list<SearchTreeNode> _child_nodes;

        // Total wins and games are with respect to this node i.e. total wins/games that have been
        // evaluated from this node 
        int64_t _total_games = 0;

        // This is a float since we might later allow for a score between 0 and 1 rather than an explicit
        // win or loss. Furthermore since we are going between 0 and 1 we need to allow for a draw (0.5)
        float _total_wins = 0.0;

        // The original result
        float _result = 0.0;

        // Used to determine whether this a node that can be explored further or not
        bool _no_more_moves = false;

        // The moves made to reach this node from the parent node. This is default initialised for the top-level parent
        std::shared_ptr<Move> _move_made;

        // Number of possible child nodes we can have based on the possible moves at the game state corresponding to this node. Storing this avoids us having to store
        // the possible moves themselves.
        size_t _num_possible_children = 0;
    };
}