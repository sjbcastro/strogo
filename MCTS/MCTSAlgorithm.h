#pragma once

#include "MCTSSearchTreeNode.h"
#include "MCTSSimulator.h"
#include "MCTSNodePool.h"

#include <memory>
#include <fstream>

namespace MCTS
{
    class Move;
    class MoveGenerator;
    class BestEnemyScores;
    class MoveChoices;

    // Encapsulates the algorithm/logic for a Monte Carlo tree search
    // This is an adapted/generalised form of what I used for Battlesnake, as such there may be one or two mentions of snakes :)
    class Algorithm
    {
    public:

        // The public entry point - does as many simulations as possible within the time allowed and returns what it considers to be the best move
        std::shared_ptr<Move> findOptimalMove(const MoveGenerator& input, float time_allowed_ms);

        void setThresholdForAbandoningSimulation(int threshold) { _simulator.setThresholdForAbandoningSimulation(threshold); }

    private:

        // Uses UCT to decide which node to use for expanding - this provides a balance between exploration (is this a node which hasn't been explored much) and exploitation
        // (does this node yield a high win/play ratio). Note that the enemy move(s) are considered in this - see PossibleMoves for more info
        std::list<SearchTreeNode>::iterator selectNextNode(SearchTreeNode* current_node, bool next_move_is_main_player, MoveChoices& move_choices) const;

        // Similar to selectNextNode, but instead we are using the top level parent node as the starting point, and we're getting the final move so we're only concerned
        // about the maximum win/play ratio (again, taking enemy move(s) into account)
        std::shared_ptr<Move> getNextMoveFromTree(MoveChoices& move_choices);

        // The top level parent node, which spawns a search tree
        SearchTreeNode _tree_search;
        // Class used for simulating playouts. This has memory of its own, as well as an RNG, so helpful to keep this memory resident
        Simulator _simulator;
        // Node pool which initially has a bunch of nodes allocated which means we don't need to repetitively allocate memory
        NodePool _node_pool;
        // Exploration parameter - this is used in the UCT algorithm
        float _exploration_parameter = 1.4f;
    };
}
