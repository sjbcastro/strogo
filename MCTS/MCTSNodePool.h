#pragma once

#include "MCTSSearchTreeNode.h"
#include <exception>

namespace MCTS
{
    // Helper class used to allocate search tree nodes up front and return them on request
    // To be honest, this could be templated and used to allocate other things that might get frequently allocated
    class NodePool
    {
    public:

        // Constructor - allocates a large number of nodes
        NodePool()
        {
            for (int i = 0; i < 1000000; ++i)
                _node_pool.emplace_back();
        }

        // Splices an allocated node (if one exists) onto the end of the input list. Returns false if there are no more allocated nodes
        bool getAllocatedNode(std::list<SearchTreeNode>& target_list);

        // Returns allocated nodes back into the node pool
        void returnAllocatedNodes(SearchTreeNode& current_node);

        size_t size() const { return _node_pool.size(); }

    private:
        // The allocated memory
        std::list<SearchTreeNode> _node_pool;
    };

}
