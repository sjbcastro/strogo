#include "MCTSSimulator.h"

#include "MCTSMoveGenerator.h"
#include "MCTSTreeSearchLogger.h"

#include <chrono>

namespace MCTS
{
	Simulator::Simulator()
	{
		// obtain a seed from the system clock:
		unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
		//seed1 = 3213995776;

        TreeSearchLogger::instance()->writeLine("Using seed " + std::to_string(seed1));

		_rng = std::make_unique<std::mt19937>(seed1);
	}
	void Simulator::simulatePlayout(const MoveGenerator& input, SearchTreeNode*& current_node, NodePool& node_pool)
	{
        if (!_game_state)
        {
            _game_state = input.clone();
        }

        _game_state->set(input);

        int number_of_moves = 0;
        float result = 0.0;
        while (!_game_state->gameOver(result))
        {
            auto next_move = _game_state->generateRandomMove(*_rng);

            if (!next_move)
            {
                // Assume game over if we couldn't find a valid move
                return;
            }

            auto it = current_node->find(next_move);
            if (it == current_node->end())
            {
                it = current_node->addNode(next_move, nullptr, node_pool);
            }
            current_node = &(*it);

            // Process moves on board
            _game_state->processMove(*next_move);

            ++number_of_moves;

            if (number_of_moves > _threshold_for_abandoning_simulation)
            {
                ++_n_incomplete_simulations;
                return;
            }
        }

        ++_n_simulations;
        _average_simulation_length = (_average_simulation_length * (_n_simulations - 1.0) / _n_simulations) + static_cast<double>(number_of_moves) / _n_simulations;

        current_node->reachedTerminalNode(result);
	}

    void Simulator::logAndResetStats()
    {
        TreeSearchLogger::instance()->writeLine("Logging simulation stats:");
        TreeSearchLogger::instance()->writeLine("\tNumber of simulations " + std::to_string(_n_simulations));
        TreeSearchLogger::instance()->writeLine("\tAverage simulation length " + std::to_string(_average_simulation_length));
        TreeSearchLogger::instance()->writeLine("\tNumber of incomplete simulations " + std::to_string(_n_incomplete_simulations));

        _n_incomplete_simulations = 0;
        _n_simulations = 0;
        _average_simulation_length = 0.0;
    }
}