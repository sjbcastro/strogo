#pragma once

#include <string>

namespace MCTS
{
    // Abstract class representing a move in whatever game we are playing
    class Move
    {
    public:

        virtual std::string toString() const = 0;

        virtual bool isEqual(const Move&) const = 0;
    };
}
