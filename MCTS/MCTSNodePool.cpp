#include "MCTSNodePool.h"

namespace MCTS
{
    bool NodePool::getAllocatedNode(std::list<SearchTreeNode>& target_list)
    {
        if (_node_pool.empty())
        {
            return false;
        }
        else
        {
            target_list.splice(target_list.end(), _node_pool, _node_pool.begin());
            return true;
        }
    }

    void NodePool::returnAllocatedNodes(SearchTreeNode& current_node)
    {
        for (auto child_node_it = current_node.begin(); child_node_it != current_node.end(); ++child_node_it)
        {
            if (!child_node_it->empty())
            {
                returnAllocatedNodes(*child_node_it);
            }

            if (!child_node_it->empty())
            {
                throw std::runtime_error("returnAllocatedNodes: child node was not emptied properly");
            }
        }

        // By this point all of the child nodes should be empty
        auto& nodes_to_splice = current_node._child_nodes;
        _node_pool.splice(_node_pool.end(), nodes_to_splice, nodes_to_splice.begin(), nodes_to_splice.end());
    }
}