#pragma once

#include <memory>
#include <random>

namespace MCTS
{
	class Move;

	// This class is completely generic so as to allow subclasses to specify how the possible moves are stored.
	// The intention of this class is that it represents the possible moves that the main player can make AND the possible responses of the opponent(s)
	// Originally this code was written for Battlesnake, where the players move simultaneously, and so we had to consider the opponent's move at the same time.
	// However, as it happens, considering all possible moves and responses in one go makes the UCT algorithm easier to write, as the node selection is always from the
	// point of view of the player requesting the next move. The other side to this is that it makes this class more complicated...
	class PossibleMoves
	{
	public:

		// Main function used for simulating random playouts
		virtual void generateRandomMove(std::mt19937& rng, std::shared_ptr<MCTS::Move>& move_made) const = 0;

		// This is used by the MCTS algorithm when considering whether a node has been fully expanded or not
		virtual size_t numPossibleNodes() const = 0;

		// This and the next method are used when expanding a node (i.e. populating it with subsequent child nodes). This should get the first possible move set (what 'first'
		// means is up to the subclass to decide)
		virtual std::unique_ptr<Move> getFirstMove() = 0;

		// Used following the previous method when expanding a node. Basically used to iterate through the possible move sets, so that the SearchTreeNode can populate the
		// next child node
		virtual bool getNextMove(std::shared_ptr<Move>& next_move) = 0;
	};
}
