#include "Stone.h"
#include "Utility.h"
#include <sstream>

namespace GoLogic
{
    Point::Point(PointLetter x, uint8_t y)
        : _x(x)
        , _y(y)
    {
    }

    bool Point::operator<(const Point& rhs) const
    {
        if (_y < rhs._y)
            return true;
        else if (_y > rhs._y)
            return false;
        else
            return _x < rhs._x;
    }

    std::string Point::asString() const
    {
        std::ostringstream oss;
        oss << Utility::PointLetterAsString(_x) << (int)_y;
        return oss.str();
    }

    Stone::Stone(const Point& point, StoneColor color)
        : _point(point)
        , _color(color)
    {
    }

    Stone::Stone(PointLetter x, uint8_t y, StoneColor color)
        : _point(Point(x, y))
        , _color(color)
    {
    }

    std::string Stone::asString() const
    {
        return _point.asString();
    }

    bool Stone::operator<(const Stone& rhs) const
    {
        return _point < rhs._point;
    }

    Move::Move(MoveType move_type)
    {
        if (move_type == MoveType::RESIGN)
        {
            _resign = true;
            _is_pass = false;
        }
        else
        {
            _resign = false;
            _is_pass = true;
        }
    }

    Move::Move(Stone s)
        : _stone(s)
        , _is_pass(false)
        , _resign(false)
    {
    }

    Move::Move(Point p, StoneColor color)
        : _stone(p, color)
        , _is_pass(false)
        , _resign(false)
    {
    }

    Move::Move(PointLetter x, uint8_t y, StoneColor color)
        : _stone(x, y, color)
        , _is_pass(false)
        , _resign(false)
    {
    }

    void Move::pass(StoneColor color)
    {
        _is_pass = true;
        _resign = false;
        _stone = Stone(Point(), color);
    }

    void Move::resign()
    {
        _is_pass = false;
        _resign = true;
        _stone = Stone();
    }

    std::string Move::asString() const
    {
        if (_is_pass)
        {
            return "PASS";
        }
        else if (_resign)
        {
            return "RESIGN";
        }
        else
        {
            std::string val = _stone.color() == StoneColor::BLACK ? "B" : "W";
            return val + " " + _stone.asString();
        }
    }

    bool operator==(const Point& lhs, const Point& rhs)
    {
        return lhs.x() == rhs.x() && lhs.y() == rhs.y();
    }

    bool operator==(const Stone& lhs, const Stone& rhs)
    {
        return lhs.x() == rhs.x() && lhs.y() == rhs.y() && lhs.color() == rhs.color();
    }

    bool operator!=(const Point& lhs, const Point& rhs)
    {
        return lhs.x() != rhs.x() || lhs.y() != rhs.y();
    }

    bool operator!=(const Stone& lhs, const Stone& rhs)
    {
        return lhs.x() != rhs.x() || lhs.y() != rhs.y() || lhs.color() != rhs.color();
    }
    bool operator==(const Move& lhs, const Move& rhs)
    {
        return lhs.isPass() == rhs.isPass() && lhs.isResign() == rhs.isResign() && lhs.stone() == rhs.stone();
    }
}