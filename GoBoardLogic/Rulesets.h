#pragma once

// The intention is just to support Chinese rules with positional superko
// but the enums are defined nonetheless in case this changes

namespace GoLogic
{
    enum class Ruleset : uint8_t
    {
        CHINESE
    };

    enum class SuperkoRuleset : uint8_t
    {
        PSK     // Positional superko
    };
}