#pragma once

#include <inttypes.h>
#include <assert.h>
#include <string>

namespace GoLogic
{
    enum class PointLetter : uint16_t
    {
        INVALID = 0,
        A = 1,
        B = 2,
        C = 3,
        D = 4,
        E = 5,
        F = 6,
        G = 7,
        H = 8,
        J = 9,
        K = 10,
        L = 11,
        M = 12,
        N = 13,
        O = 14,
        P = 15,
        Q = 16,
        R = 17,
        S = 18,
        T = 19
    };
}