#pragma once

#include "Stone.h"
#include <list>

namespace GoLogic
{
    class SimpleSgfRecord
    {
    public:
        SimpleSgfRecord() = default;

        double komi() const { return _komi; }
        void komi(double val) { _komi = val; }

        uint8_t boardSize() const { return _board_size; }
        void boardSize(uint8_t val) { _board_size = val; }

        void addMove(const Move& m) { _move_history.emplace_back(m); }

        const std::list<Move>& moveHistory() const { return _move_history; }

    private:
        double _komi;
        uint8_t _board_size;
        std::list<Move> _move_history;
    };
}