#include "Board.h"
#include <algorithm>
#include <iostream>
#include "Utility.h"

namespace GoLogic
{
    Board::Board(uint8_t sz)
        : _size(sz)
    {
        // Force board to break if we get a board size we don't like
        if (_size > 19 || _size < 5)
            _size = 0;

        Zobrist2DHashTable::instance()->initialise(_size);
        _board_state = std::vector<std::vector<int8_t>>(_size, std::vector<int8_t>(_size, 0));
        _group_map = std::vector<uint_fast16_t>(_size * _size, 0);
    }

    bool Board::addStone(const Stone& s)
    {
        if (isMoveLegalWithoutKo(s))
        {
            _can_undo_last_move_by_removing = true;
            _number_last_captured = 0;

            // Start by finding a group to add the stone to or creating a new group for it
            bool found_neighbouring_group = false;

            setGroupID(s.i(), s.j(), _next_group_id);
            _next_group_id++;

            checkForNeighbouringFriendlyGroupAndMerge(s, 1, 0);
            checkForNeighbouringFriendlyGroupAndMerge(s, 0, 1);
            checkForNeighbouringFriendlyGroupAndMerge(s, -1, 0);
            checkForNeighbouringFriendlyGroupAndMerge(s, 0, -1);

            addStoneToBoardState(s);

            // Check for capture
            checkForCapture(s);

            return true;
        }

        return false;
    }

    void Board::undoLastMove(const Stone& s)
    {
        uint8_t x = (uint8_t)s.x() - 1;
        uint8_t y = s.y() - 1;
        setPoint(x, y, 0);
        setGroupID(x, y, 0);

        _hash ^= Zobrist2DHashTable::instance()->getValue(s);
        _next_group_id--;
    }

    bool Board::isMoveLegalWithoutKo(const Stone& s) const
    {
        // Check point is on the board
        if ((uint8_t)s.x() <= 0 || (uint8_t)s.x() > _size || s.y() <= 0 || s.y() > _size)
            return false;

        // Check the point is not already occupied
        if (getPoint(s.i(), s.j()) != 0) return false;

        std::function<bool(const Point&)> AcceptAnyPoint = [](const Point&)
        {
            return true;
        };

        ;
        if (!findAdjacentPointMatchingCriteria(s, 0, AcceptAnyPoint))
        {
            return isCapture(s) || neighbouringGroupHasLiberty(s);
        }

        return true;
    }


    void Board::addStoneToBoardState(const Stone& s)
    {
        uint8_t x = (uint8_t)s.x() - 1;
        uint8_t y = s.y() - 1;
        _board_state[x][y] = (int8_t)s.color();

        _hash ^= Zobrist2DHashTable::instance()->getValue(s);
    }

    void Board::checkForCapture(const Stone& last_move)
    {
        handleNeighbouringEnemyGroupCapture(last_move, 1, 0);
        handleNeighbouringEnemyGroupCapture(last_move, 0, 1);
        handleNeighbouringEnemyGroupCapture(last_move, -1, 0);
        handleNeighbouringEnemyGroupCapture(last_move, 0, -1);
    }

    bool Board::findAdjacentPointMatchingCriteria(const Stone& last_move, int8_t matching_color, std::function<bool(const Point& point)>& callback) const
    {
        uint8_t x = (uint8_t)last_move.x() - 1;
        uint8_t y = last_move.y() - 1;

        if (x > 0 && _board_state[x - 1][y] == matching_color)
        {
            PointLetter letter = (PointLetter)((uint8_t)last_move.x() - 1);
            if (callback(Point(letter, last_move.y()))) return true;
        }

        if (x < _size - 1 && _board_state.at(x + 1).at(y) == matching_color)
        {
            PointLetter letter = (PointLetter)((uint8_t)last_move.x() + 1);
            if (callback(Point(letter, last_move.y()))) return true;
        }

        if (y > 0 && _board_state.at(x).at(y - 1) == matching_color)
        {
            if (callback(Point(last_move.x(), last_move.y() - 1))) return true;
        }

        if (y < _size - 1 && _board_state.at(x).at(y + 1) == matching_color)
        {
            if (callback(Point(last_move.x(), last_move.y() + 1))) return true;
        }

        return false;
    }

    bool Board::doesNeighbouringFriendlyGroupHaveAnyLiberties(const Stone& last_move, int_fast8_t dx, int_fast8_t dy) const
    {
        auto expected_colour = static_cast<int8_t>(last_move.color());

        int8_t x = last_move.i() + dx;
        int8_t y = last_move.j() + dy;

        if (x >= 0 && y >= 0 && x < _size && y < _size)
        {
            if (_board_state.at(x).at(y) == expected_colour)
            {
                auto target_group_id = getGroupID(x, y);
                if (doesGroupHaveAnyLibertiesExcludingCandidateMove(target_group_id, last_move)) return true;
            }
        }

        return false;
    }

    void Board::checkForNeighbouringFriendlyGroupAndMerge(const Stone& last_move, int_fast8_t dx, int_fast8_t dy)
    {
        auto expected_colour = static_cast<int8_t>(last_move.color());

        int8_t x = last_move.i() + dx;
        int8_t y = last_move.j() + dy;

        if (x >= 0 && y >= 0 && x < _size && y < _size)
        {
            if (_board_state.at(x).at(y) == expected_colour)
            {
                _can_undo_last_move_by_removing = false;

                auto target_group_id = getGroupID(x, y);
                auto new_group_id = getGroupID(last_move.i(), last_move.j());
                if (target_group_id != new_group_id)
                {
                    replaceGroupsWithNewID(target_group_id, x, y, new_group_id);
                }
            }
        }
    }

    bool Board::checkForNeighbouringEnemyGroupCapture(const Stone& last_move, int_fast8_t dx, int_fast8_t dy) const
    {
        auto expected_colour = -static_cast<int8_t>(last_move.color());

        int8_t x = last_move.i() + dx;
        int8_t y = last_move.j() + dy;

        if (x >= 0 && y >= 0 && x < _size && y < _size)
        {
            if (_board_state.at(x).at(y) == expected_colour)
            {
                auto target_group_id = getGroupID(x, y);
                if (!doesGroupHaveAnyLibertiesExcludingCandidateMove(target_group_id, last_move)) return true;
            }
        }

        return false;
    }

    void Board::handleNeighbouringEnemyGroupCapture(const Stone& last_move, int_fast8_t dx, int_fast8_t dy)
    {
        auto expected_colour = -static_cast<int8_t>(last_move.color());
        auto expected_colour_enum = static_cast<StoneColor>(expected_colour);

        int8_t x = last_move.i() + dx;
        int8_t y = last_move.j() + dy;

        if (x >= 0 && y >= 0 && x < _size && y < _size)
        {
            if (_board_state.at(x).at(y) == expected_colour)
            {
                auto target_group_id = getGroupID(x, y);
                if (!doesGroupHaveAnyLiberties(target_group_id)) handleGroupCapture(expected_colour_enum, x, y, target_group_id);
            }
        }
    }

    void Board::replaceGroupsWithNewID(uint_fast16_t target_group_id, int_fast8_t x, int_fast8_t y, uint_fast16_t new_group_id)
    {
        if (x >= 0 && y >= 0 && x < _size && y < _size)
        {
            if (getGroupID(x, y) == target_group_id)
            {
                setGroupID(x, y, new_group_id);

                replaceGroupsWithNewID(target_group_id, x - 1, y, new_group_id);
                replaceGroupsWithNewID(target_group_id, x + 1, y, new_group_id);
                replaceGroupsWithNewID(target_group_id, x, y - 1, new_group_id);
                replaceGroupsWithNewID(target_group_id, x, y + 1, new_group_id);
            }
        }
    }
#
    void Board::removeGroup(uint_fast16_t target_group_id, int_fast8_t x, int_fast8_t y, StoneColor captured_color, int& n_replaced)
    {
        if (x >= 0 && y >= 0 && x < _size && y < _size)
        {
            if (getGroupID(x, y) == target_group_id)
            {
                setGroupID(x, y, 0);
                setPoint(x, y, 0);
                _hash ^= Zobrist2DHashTable::instance()->getValue(captured_color, x, y);
                ++n_replaced;

                removeGroup(target_group_id, x - 1, y, captured_color, n_replaced);
                removeGroup(target_group_id, x + 1, y, captured_color, n_replaced);
                removeGroup(target_group_id, x, y - 1, captured_color, n_replaced);
                removeGroup(target_group_id, x, y + 1, captured_color, n_replaced);
            }
        }
    }

    bool Board::doesGroupHaveAnyLiberties(uint_fast16_t target_group_id) const
    {
        for (uint8_t x = 0; x < _size; ++x)
        {
            for (uint8_t y = 0; y < _size; ++y)
            {
                if (getGroupID(x, y) == target_group_id)
                {
                    if (x > 0 && getPoint(x - 1, y) == 0) return true;
                    else if (x < _size - 1 && getPoint(x + 1, y) == 0) return true;
                    else if (y > 0 && getPoint(x, y - 1) == 0) return true;
                    else if (y < _size-1 && getPoint(x, y + 1) == 0) return true;
                }
            }
        }

        return false;
    }

    bool Board::doesGroupHaveAnyLibertiesExcludingCandidateMove(uint_fast16_t target_group_id, const Stone& candidate_move) const
    {
        for (uint8_t x = 0; x < _size; ++x)
        {
            for (uint8_t y = 0; y < _size; ++y)
            {
                if (getGroupID(x, y) == target_group_id)
                {
                    if (x > 0 && isPointVacantAndNotCandidateMove(x-1, y, candidate_move)) return true;
                    else if (x < _size - 1 && isPointVacantAndNotCandidateMove(x + 1, y, candidate_move)) return true;
                    else if (y > 0 && isPointVacantAndNotCandidateMove(x, y - 1, candidate_move)) return true;
                    else if (y < _size - 1 && isPointVacantAndNotCandidateMove(x, y + 1, candidate_move)) return true;
                }
            }
        }

        return false;
    }

    bool Board::isPointVacantAndNotCandidateMove(int_fast8_t x, int_fast8_t y, const Stone& candidate_move) const
    {
        return getPoint(x, y) == 0 && (candidate_move.i() != x || candidate_move.j() != y);
    }

    void Board::handleGroupCapture(StoneColor captured_color, int_fast8_t x, int_fast8_t y, uint_fast16_t group_id)
    {
        _can_undo_last_move_by_removing = false;

        int n_captured = 0; 
        removeGroup(group_id, x, y, captured_color, n_captured);
        if (captured_color == StoneColor::BLACK)
        {
            _black_captured_stones += n_captured;
            _number_last_captured += n_captured;
        }
        else
        {
            _white_captured_stones += n_captured;
            _number_last_captured += n_captured;
        }
    }

    bool Board::isCapture(const Stone& s) const
    {
        if (checkForNeighbouringEnemyGroupCapture(s, 1, 0)) return true;
        if (checkForNeighbouringEnemyGroupCapture(s, 0, 1)) return true;
        if (checkForNeighbouringEnemyGroupCapture(s, -1, 0)) return true;
        if (checkForNeighbouringEnemyGroupCapture(s, 0, -1)) return true;

        return false;
    }

    bool Board::neighbouringGroupHasLiberty(const Stone& s) const
    {
        if (doesNeighbouringFriendlyGroupHaveAnyLiberties(s, 1, 0)) return true;
        if (doesNeighbouringFriendlyGroupHaveAnyLiberties(s, 0, 1)) return true;
        if (doesNeighbouringFriendlyGroupHaveAnyLiberties(s, -1, 0)) return true;
        if (doesNeighbouringFriendlyGroupHaveAnyLiberties(s, 0, -1)) return true;

        return false;
    }

    bool Board::areBoardPositionsEquivalent(const Board& rhs) const
    {
        return _hash == rhs._hash;
    }

    void Board::printState() const
    {
        for (auto row : _board_state)
        {
            for (auto val : row)
            {
                if (val == 0) std::cout << ". ";
                else if (val == 1) std::cout << "X ";
                else if (val == -1) std::cout << "O ";
            }
            std::cout << std::endl;
        }
    }

    void Board::printGroupIDMap() const
    {
        for (int i = 0; i < _size; ++i)
        {
            for (int j = 0; j < _size; ++j)
            {
                std::cout << _group_map[i + j * _size] << " ";
            }
            std::cout << std::endl;
        }
    }

    bool Board::empty() const
    {
        for (const auto& row : _board_state)
            for (const auto& point : row)
                if (point != 0)
                    return false;

        return true;
    }

    int Board::numberOfEmptyPoints() const
    {
        int count = 0;

        for (int i = 0; i < _group_map.size(); ++i) 
            if (_group_map[i] == 0)
                ++count;

        return count;
    }
}