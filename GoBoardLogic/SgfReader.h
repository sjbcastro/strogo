#pragma once

#include "SimpleSgfRecord.h"

namespace GoLogic
{
    class SgfReader
    {
    public:
        SimpleSgfRecord readSimpleSgfFile(const std::string& filename) const;
        SimpleSgfRecord readSgfContent(const std::string& content) const;

    private:
        std::string parseTag(const std::string& tag_id, const std::string& sgf_text, size_t pos = 0) const;
        bool parseTagForNumber(const std::string& tag_id, const std::string& sgf_text, double& val) const;
        bool parseTagForNumber(const std::string& tag_id, const std::string& sgf_text, int& val) const;
    };
}