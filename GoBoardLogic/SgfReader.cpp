#include "SgfReader.h"
#include <fstream>
#include <streambuf>
#include "Utility.h"

namespace GoLogic
{
    SimpleSgfRecord SgfReader::readSimpleSgfFile(const std::string& filename) const
    {
        std::ifstream ifs(filename);
        std::string content((std::istreambuf_iterator<char>(ifs)),
            std::istreambuf_iterator<char>());

        return readSgfContent(content);
    }

    SimpleSgfRecord SgfReader::readSgfContent(const std::string& content) const
    {
        SimpleSgfRecord record;

        if (parseTag("FF", content) == "4")
        {
            int board_size = 19;
            double komi = 6.5;

            parseTagForNumber("SZ", content, board_size);
            parseTagForNumber("KM", content, komi);

            record.komi(komi);
            record.boardSize(board_size);

            size_t current = content.find(";");
            size_t game_end = content.find_last_of(")");

            if (game_end == std::string::npos) return record;

            while (current < game_end)
            {
                current = content.find(";", current + 1);

                std::string color_str = content.substr(current + 1, 1);
                std::string move_str = parseTag(color_str, content, current);

                if (move_str.size() == 2)
                {
                    Point p = Utility::PointFromSgfString(move_str);

                    if (color_str == "B")
                    {
                        record.addMove(Move(p, StoneColor::BLACK));
                    }
                    else if (color_str == "W")
                    {
                        record.addMove(Move(p, StoneColor::WHITE));
                    }
                }
            }
        }

        return record;
    }

    std::string SgfReader::parseTag(const std::string& tag_id, const std::string& content, size_t pos) const
    {
        size_t tag_start = content.find(tag_id, pos) + tag_id.length() + 1;
        size_t tag_end = content.find("]", tag_start);
        if (tag_start != std::string::npos && tag_end != std::string::npos)
            return content.substr(tag_start, tag_end - tag_start);
        else
            return "";
    }

    bool SgfReader::parseTagForNumber(const std::string& tag_id, const std::string& content, double& value) const
    {
        std::string str = parseTag(tag_id, content);
        if (str.empty()) return false;

        try
        {
            value = std::stod(str);
            return true;
        }
        catch (std::exception&)
        {
            return false;
        }
    }

    bool SgfReader::parseTagForNumber(const std::string& tag_id, const std::string& content, int& value) const
    {
        std::string str = parseTag(tag_id, content);
        if (str.empty()) return false;
        try
        {
            value = std::stoi(str);
            return true;
        }
        catch (std::exception&)
        {
            return false;
        }
    }
}