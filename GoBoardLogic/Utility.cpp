#include "Utility.h"
#include <sstream>

namespace GoLogic
{
    std::string Utility::PointLetterAsString(PointLetter val)
    {
        switch (val)
        {
        case PointLetter::A: return "A";
        case PointLetter::B: return "B";
        case PointLetter::C: return "C";
        case PointLetter::D: return "D";
        case PointLetter::E: return "E";
        case PointLetter::F: return "F";
        case PointLetter::G: return "G";
        case PointLetter::H: return "H";
        case PointLetter::J: return "J";
        case PointLetter::K: return "K";
        case PointLetter::L: return "L";
        case PointLetter::M: return "M";
        case PointLetter::N: return "N";
        case PointLetter::O: return "O";
        case PointLetter::P: return "P";
        case PointLetter::Q: return "Q";
        case PointLetter::R: return "R";
        case PointLetter::S: return "S";
        case PointLetter::T: return "T";
        default:
            assert(false);
        }

        return "NULL";
    }

    PointLetter Utility::PointLetterFromString(const std::string& str)
    {
        if (str == "A") return PointLetter::A;
        else if (str == "B") return PointLetter::B;
        else if (str == "C") return PointLetter::C;
        else if (str == "D") return PointLetter::D;
        else if (str == "E") return PointLetter::E;
        else if (str == "F") return PointLetter::F;
        else if (str == "G") return PointLetter::G;
        else if (str == "H") return PointLetter::H;
        else if (str == "J") return PointLetter::J;
        else if (str == "K") return PointLetter::K;
        else if (str == "L") return PointLetter::L;
        else if (str == "M") return PointLetter::M;
        else if (str == "N") return PointLetter::N;
        else if (str == "O") return PointLetter::O;
        else if (str == "P") return PointLetter::P;
        else if (str == "Q") return PointLetter::Q;
        else if (str == "R") return PointLetter::R;
        else if (str == "S") return PointLetter::S;
        else if (str == "T") return PointLetter::T;
        else
        {
            assert(false);
            return PointLetter::INVALID;
        }
    }



    bool Utility::areStonesAdjacent(const Stone& s1, const Stone& s2)
    {
        // The absolute differences in x and y must either be 0 and 1, or 1 and 0
        // In either case the sum of the differences add up to 1 and is the only way for them to add up to 1
        uint8_t val = std::abs((uint8_t)s1.x() - (uint8_t)s2.x()) + std::abs(s1.y() - s2.y());
        return val == 1;
    }

    std::string Utility::MoveAsString(const Move& m)
    {
        if (m.isPass())
            return "PASS";

        return m.stone().asString();
    }

    std::string Utility::PointAsSgfString(const Point& p)
    {
        uint8_t x = (uint8_t)p.x();
        uint8_t y = p.y();

        std::string str;
        str += 'a' + x - 1;
        str += 'a' + y - 1;

        return str;
    }

    Point Utility::PointFromSgfString(const std::string& str)
    {
        assert(str.size() == 2);
        if (str.size() == 2)
        {
            uint8_t x = (int)(str[0]) - (int)('a') + 1;
            uint8_t y = (int)(str[1]) - (int)('a') + 1;

            return Point(PointLetter(x), y);
        }
        else
        {
            return Point();
        }
    }
}