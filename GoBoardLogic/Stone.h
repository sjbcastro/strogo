#pragma once

#include "PointLetter.h"

namespace GoLogic
{
    enum class StoneColor : int8_t
    {
        BLACK = 1,
        WHITE = -1
    };

    class Point
    {
    public:
        Point() = default;
        Point(PointLetter x, uint8_t y);

        PointLetter x() const { return _x; }
        uint8_t y() const { return _y; }

        bool operator<(const Point& rhs) const;

        std::string asString() const;

        void x(PointLetter val) { _x = val; }
        void y(uint8_t val) { _y = val; }

    protected:
        PointLetter _x = PointLetter::INVALID;
        uint8_t     _y = 0;
    };

    class Stone
    {
    public:
        Stone() = default;
        Stone(const Point& point, StoneColor color);
        Stone(PointLetter x, uint8_t y, StoneColor color);

        inline uint8_t i() const { return static_cast<uint8_t>(_point.x()) - 1; }
        inline uint8_t j() const { return _point.y() - 1; }

        inline PointLetter x() const { return _point.x(); }
        inline uint8_t y() const { return _point.y(); }
        inline Point point() const { return _point; }
        inline StoneColor color() const { return _color; }

        inline void x(PointLetter val) { _point.x(val); }
        inline void y(uint8_t val) { _point.y(val); }

        std::string asString() const;

        bool operator<(const Stone& rhs) const;

    protected:
        Point           _point;
        StoneColor      _color;
    };

    enum class MoveType : uint8_t
    {
        PASS,
        RESIGN
    };

    class Move
    {
    public:
        Move() = default;
        Move(MoveType move_type);
        Move(Stone s);
        Move(Point p, StoneColor color);
        Move(PointLetter x, uint8_t y, StoneColor color);

        void pass(StoneColor color);
        bool isPass() const { return _is_pass; }

        void resign();
        bool isResign() const { return _resign; }

        Stone stone() const { return _stone; }

        std::string asString() const;

    protected:
        Stone _stone;
        bool _is_pass = false;
        bool _resign = false;
    };

    // TO-DO : this should be member function
    bool operator==(const Point& lhs, const Point& rhs);
    bool operator!=(const Point& lhs, const Point& rhs);
    bool operator==(const Stone& lhs, const Stone& rhs);
    bool operator!=(const Stone& lhs, const Stone& rhs);
    bool operator==(const Move& lhs, const Move& rhs);
}