#pragma once

#include "Singleton.h"
#include <memory>
#include <vector>
#include <bitset>
#include "Stone.h"

namespace GoLogic
{
    constexpr size_t BitArraySize = 128;
    using ZobristHash = std::bitset<BitArraySize>;

    class Zobrist2DHashTable : public Singleton<Zobrist2DHashTable>
    {
    public:

        void initialise(size_t board_size);

        const ZobristHash& getValue(const Stone& stone) const;
        const ZobristHash& getValue(StoneColor color, int8_t x, int8_t y) const;

    private:
        std::vector<ZobristHash> _hash_table;
        size_t _board_size = 0;
    };
}