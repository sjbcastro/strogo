#pragma once

#include "Zobrist2DHashTable.h"

#include <list>
#include <iostream>
#include <vector>
#include <functional>

class BoardTestHelper;
class GameEngineUtils;

namespace GoLogic
{
    class Board
    {
    public:
        Board() = default;
        Board(uint8_t new_size);

        bool addStone(const Stone& s);
        void undoLastMove(const Stone& s);
        bool isMoveLegalWithoutKo(const Stone& s) const;
        uint8_t boardSize() const { return _size; }

        bool areBoardPositionsEquivalent(const Board& rhs) const;

        uint16_t blackCapturedStones() const { return _black_captured_stones; }
        uint16_t whiteCapturedStones() const { return _white_captured_stones; }

        const std::vector<std::vector<int8_t>>& boardState() const { return _board_state; }
        const auto& groupIDMap() const { return _group_map; }

        const ZobristHash& zobristHash() const { return _hash; }

        void printState() const;
        void printGroupIDMap() const;

        bool empty() const;

        bool canUndoLastMoveByRemoving() const { return _can_undo_last_move_by_removing; }

        int numberOfEmptyPoints() const;

        int numberStonesLastCapture() const { return _number_last_captured; }

    private:

        inline int8_t getPoint(int8_t x, int8_t y) const { return _board_state.at(x).at(y); }
        inline void setPoint(int8_t x, int8_t y, int8_t value) { _board_state[x][y] = value; }
        inline uint_fast16_t getGroupID(int8_t x, int8_t y) const { return _group_map.at(x + y * _size); }
        inline void setGroupID(int8_t x, int8_t y, uint_fast16_t value) { _group_map[x+y*_size] = value; }

        friend class BoardTestHelper;
        friend class GameEngineUtils;

        void addStoneToBoardState(const Stone& s);
        void checkForCapture(const Stone& last_move);

        // Finds a point adjacent to the last move matching the predicate (basically a function which takes a point and returns true or false)
        bool findAdjacentPointMatchingCriteria(const Stone& last_move, int8_t matching_color, std::function<bool(const Point&)>& predicate) const;
        // Checks if there is a friendly group next to last_move (as determined by dx/dy) and returns true if it has liberties
        bool doesNeighbouringFriendlyGroupHaveAnyLiberties(const Stone& last_move, int_fast8_t dx, int_fast8_t dy) const;
        // Checks for a neighbouring friendly group in the direction dx/dy and merges the last move with it if so
        void checkForNeighbouringFriendlyGroupAndMerge(const Stone& last_move, int_fast8_t dx, int_fast8_t dy);
        // Checks if our last move would catch an enmy group in the direction dx/dy
        bool checkForNeighbouringEnemyGroupCapture(const Stone& last_move, int_fast8_t dx, int_fast8_t dy) const;
        // Handles the capturing of an enemy group in the direction dx/dy
        void handleNeighbouringEnemyGroupCapture(const Stone& last_move, int_fast8_t dx, int_fast8_t dy);
        // Replaces a group with target group ID with the new group ID, starting at the point x/y and spreading out
        void replaceGroupsWithNewID(uint_fast16_t target_group_id, int_fast8_t x, int_fast8_t y, uint_fast16_t new_group_id);
        // Removes a group as defined by the target group ID, starting at the point x/y. Returns the number captured via n_replaced.
        void removeGroup(uint_fast16_t target_group_id, int_fast8_t x, int_fast8_t y, StoneColor captured_color, int& n_replaced);
        // Checks if the group defined by the target ID has any liberties
        bool doesGroupHaveAnyLiberties(uint_fast16_t target_group_id) const;
        // Checks if the group defined by the target ID has any liberties, but excludes the current move (for when we haven't added the stone yet)
        bool doesGroupHaveAnyLibertiesExcludingCandidateMove(uint_fast16_t target_group_id, const Stone& candidate_move) const;
        // Checks if x/y is vacant and not equivalent to the candidate move
        bool isPointVacantAndNotCandidateMove(int_fast8_t x, int_fast8_t y, const Stone& candidate_move) const;
        // Handles the capture of the group specified by the input group ID. Starts at x/y and spreads out.
        void handleGroupCapture(StoneColor captured_color, int_fast8_t x, int_fast8_t y, uint_fast16_t group_id);

        bool isCapture(const Stone& s) const;
        bool neighbouringGroupHasLiberty(const Stone& s) const;

        uint8_t                     _size;
        uint16_t                    _black_captured_stones = 0;
        uint16_t                    _white_captured_stones = 0;

        // Note: this is just another representation of stone groups
        // Whenever board state needs to be updated, stone groups should be updated
        // then board state should be updated or regenerated
        std::vector<std::vector<int8_t>>   _board_state;

        // Each point has an ID - 0 indicates unoccupied, anything else indicates the group ID
        std::vector<uint_fast16_t> _group_map;
        // This is the group ID the next stone will have. Increments every time we make a move
        uint_fast16_t _next_group_id = 1;

        // Whether or not we can undo the last move simply by removing the stone from the board state and group map (i.e. no captures/merges to worry about)
        bool _can_undo_last_move_by_removing = false;

        // How many stones were last captured by the last move. Used to determine how far we need to search back through board hashes for superko
        int _number_last_captured = 0;

        // The Zobrist hash of the board state
        ZobristHash                 _hash;
    };
}