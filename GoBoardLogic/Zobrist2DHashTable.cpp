#include "Zobrist2DHashTable.h"
#include <random>
#include <stdexcept>

namespace GoLogic
{
    void Zobrist2DHashTable::initialise(size_t board_size)
    {
        if (_board_size != board_size)
        {
            _board_size = board_size;

            // Size of hash table is board size squared, multiplied by 2 (number of pieces)
            size_t hash_table_size = board_size * board_size * 2;
            _hash_table = std::vector<ZobristHash>
                (hash_table_size, ZobristHash{});

            std::minstd_rand rng(0);
            std::bernoulli_distribution bernoulli_dist(0.5);

            for (ZobristHash& hash_table_entry : _hash_table)
            {
                for (int i = 0; i < hash_table_entry.size(); ++i)
                {
                    hash_table_entry[i] = bernoulli_dist(rng);
                }
            }
        }
    }

    const ZobristHash& Zobrist2DHashTable::getValue(const Stone& stone) const
    {
        size_t colour = (stone.color() == StoneColor::BLACK) ? 0 : 1;
        size_t index = colour + stone.i() * 2 + stone.j() * _board_size * 2;

        if (index < _hash_table.size())
        {
            return _hash_table[index];
        }
        else
        {
            throw std::out_of_range("Index out of bounds Zobrist2DHashTable::getValue");
        }
    }
    const ZobristHash& Zobrist2DHashTable::getValue(StoneColor color, int8_t x, int8_t y) const
    {
        size_t colour = (color == StoneColor::BLACK) ? 0 : 1;
        size_t index = colour + x * 2 + y * _board_size * 2;

        if (index < _hash_table.size())
        {
            return _hash_table[index];
        }
        else
        {
            throw std::out_of_range("Index out of bounds Zobrist2DHashTable::getValue");
        }
    }
}