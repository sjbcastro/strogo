#pragma once

#include "PointLetter.h"
#include "Stone.h"

namespace GoLogic
{
    class Utility
    {
    public:
        static std::string PointLetterAsString(PointLetter val);
        static PointLetter PointLetterFromString(const std::string& str);
        static bool areStonesAdjacent(const Stone& s1, const Stone& s2);
        static std::string MoveAsString(const Move& m);
        static std::string PointAsSgfString(const Point& p);
        static Point PointFromSgfString(const std::string& str);
    };
}