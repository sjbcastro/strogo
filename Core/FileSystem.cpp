#include "FileSystem.h"
#include <cstdlib>
#include <sys/types.h>
#include <Windows.h>
#include <Shlobj.h>
#include <iostream>

std::string FileSystem::getOutputDirectory()
{
    std::string home_dir;
#ifdef _WIN32 
    char* szPath = new char[300];
    SHGetFolderPathA(NULL,
        CSIDL_PROFILE | CSIDL_FLAG_CREATE,
        NULL,
        0,
        szPath);
    home_dir = std::string(szPath);
    delete szPath;
#else
    home_dir = getenv("HOME");
#endif
    home_dir += "/.StroGo/";
    return home_dir;
}


void FileSystem::ensureOutputDirectoryExists()
{
    std::string directory = FileSystem::getOutputDirectory();
    
#ifdef _WIN32 
    CreateDirectoryA(directory.c_str(), NULL);
#else
    if (opendir(directory.c_str()) == nullptr)
    {
        std::string cmd = "mkdir -p " + directory;
        auto result = system(cmd.c_str());
    }
#endif
}