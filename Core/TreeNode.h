#pragma once

#include <memory>
#include <vector>

// Abstract base class for creating tree structures
class TreeNode
{
public:

    // Default constructor
    TreeNode() = default;

    // Virtual destructor
    virtual ~TreeNode() {}

    // Checks if the current node is a root node (i.e. has no parent)
    bool isRootNode() const { return _parent == nullptr; }

    // Return parent node
    TreeNode* parentNode() const { return _parent; }

protected:

    // Constructor for a child node - should only be called as part of addChildNode and hence is protected
    TreeNode(TreeNode* parent) : _parent(parent) {}

    // Pointer to parent node - only allow one
    TreeNode* _parent = nullptr;

    // List of pointers to child nodes
    std::vector<std::unique_ptr<TreeNode>> _children;
};
