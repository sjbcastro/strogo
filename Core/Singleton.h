#pragma once

template<class T>
class Singleton
{
public:
	static T* instance()
	{
            if(!m_instance)
            {
                Singleton<T>::m_instance = new T();
            }

            return m_instance;
	}
	static void destroy()
	{
            delete Singleton<T>::m_instance;
            Singleton<T>::m_instance = 0;
	}

private:

	Singleton(Singleton const&){};
	Singleton& operator=(Singleton const&){};

protected:
	static T* m_instance;

	Singleton(){ m_instance = static_cast <T*> (this); };
	~Singleton(){  };
};

template<class T>
T* Singleton<T>::m_instance = 0;
