#include "Logger.h"
#include <chrono>
#include <ctime>
#include <fstream>
#include "FileSystem.h"

void Logger::enable()
{
    _enabled = true;
    _filepath = FileSystem::getOutputDirectory() + filename();
    FileSystem::ensureOutputDirectoryExists();
    
    // Start with a clean slate
    std::ofstream out;
    out.open(_filepath);
    out.close();
}

void Logger::writeLine(const std::string& str)
{
    if (_enabled)
    {
        std::ofstream out;
        out.open(_filepath, std::ios::app);

        std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
        std::time_t start_time = std::chrono::system_clock::to_time_t(now);
        char timedisplay[100];
        struct tm buf;
        errno_t err = localtime_s(&buf, &start_time);
        if (std::strftime(timedisplay, sizeof(timedisplay), "%B %d, %Y %H:%M:%S", &buf)) 
        {
            out << timedisplay << ": ";
        }

        out << str << "\n";

        out.close();
    }
}
