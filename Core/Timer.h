#include <chrono>

class Timer
{
public:

    inline float elapsedMilliseconds() const
    {
        auto current_time = std::chrono::steady_clock::now();
        auto elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds>(current_time - _start);
        return static_cast<float>(elapsed_time.count());
    }
private:
    std::chrono::time_point<std::chrono::steady_clock> _start = std::chrono::steady_clock::now();
};
