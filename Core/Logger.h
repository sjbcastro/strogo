#pragma once

#include <string>

class Logger
{
public:

    virtual std::string filename() const = 0;
    void enable();
    void writeLine(const std::string& str);

protected:
    std::string _filepath;
    bool _enabled = false;
};


