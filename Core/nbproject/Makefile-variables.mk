#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux
CND_ARTIFACT_NAME_Debug=libcore.a
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux/libcore.a
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux/package
CND_PACKAGE_NAME_Debug=Core.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux/package/Core.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux
CND_ARTIFACT_NAME_Release=libcore.a
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux/libcore.a
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux/package
CND_PACKAGE_NAME_Release=Core.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux/package/Core.tar
# WinRelease configuration
CND_PLATFORM_WinRelease=MinGW-Windows
CND_ARTIFACT_DIR_WinRelease=dist/WinRelease/MinGW-Windows
CND_ARTIFACT_NAME_WinRelease=libcore.a
CND_ARTIFACT_PATH_WinRelease=dist/WinRelease/MinGW-Windows/libcore.a
CND_PACKAGE_DIR_WinRelease=dist/WinRelease/MinGW-Windows/package
CND_PACKAGE_NAME_WinRelease=Core.tar
CND_PACKAGE_PATH_WinRelease=dist/WinRelease/MinGW-Windows/package/Core.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
