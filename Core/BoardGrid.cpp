#include "BoardGrid.h"

#include <cmath>
#include <cassert>
#include <iostream>
#include <stdexcept>

BoardGrid::BoardGrid(int_fast8_t max_x, int_fast8_t max_y, int_fast8_t max_states)
	: _max_states(max_states)
	, _max_x(max_x)
	, _max_y(max_y)
	, _data(max_y, 0)
{
}

int_fast8_t BoardGrid::get(int_fast8_t x, int_fast8_t y) const
{
	int_fast8_t temp = 0;
	return masterGetSet(x, y, nullptr, temp);
}

void BoardGrid::set(int_fast8_t x, int_fast8_t y, int_fast8_t value) 
{
	int_fast8_t increment = 0;
	masterGetSet(x, y, &value, increment);
	_data[y] += increment;
}

void BoardGrid::printToConsole() const
{
	for (int_fast8_t x = 0; x < _max_x; ++x)
	{
		for (int_fast8_t y = 0; y < _max_y; ++y)
		{
			std::cout << (int)get(x, y) << " ";
		}
		std::cout << std::endl;
	}
}

std::vector<std::pair<int_fast8_t, int_fast8_t>> BoardGrid::getCoordinatesMatchingValue(int_fast8_t value) const
{
	std::vector<std::pair<int_fast8_t, int_fast8_t>> points;

	for (int_fast8_t x = 0; x < _max_x; ++x)
	{
		for (int_fast8_t y = 0; y < _max_y; ++y)
		{
			if (get(x, y) == value)
			{
				points.emplace_back(x, y);
			}
		}
	}
	
	return points;
}

int_fast8_t BoardGrid::masterGetSet(int_fast8_t x, int_fast8_t y, int_fast8_t* new_value, int_fast8_t& value_to_increment_by) const
{
	if (new_value)
	{
		if (*new_value >= _max_states)
		{
			throw std::logic_error("Input value exceeds _max_states");
		}
	}
	if (x >= 0 && x < _max_x && y >= 0 && y < _max_y)
	{
		assert(_data.size() == _max_y);
		assert(y < _data.size());
		if (y < _data.size())
		{
			int_fast8_t base = (x > 0) ? (int_fast8_t)std::pow(_max_states, x) : 1;
			int_fast64_t current_value = (_data[y] / base) % _max_states;

			if (current_value >= _max_states)
			{
				throw std::logic_error("Retrieved value exceeds the maximum");
			}

			if (new_value)
			{
				int_fast8_t diff = *new_value - static_cast<int_fast8_t>(current_value);
				value_to_increment_by = diff * base;
			}
			else
			{
				return static_cast<int_fast8_t>(current_value);
			}
		}
		else
		{
			throw std::logic_error("y >= _data.size() - this indicates a bug in BoardGrid");
		}
	}
	else
	{
		throw std::logic_error("x and/or y beyond bounds");
	}

	return -1;
}

BoardGrid::Iterator BoardGrid::find(int_fast8_t value) const
{
	BoardGrid::Iterator it;
	if (get(0, 0) == value)
	{
		return it;
	}
	else
	{
		return findNext(it, value);
	}
}

BoardGrid::Iterator BoardGrid::findNext(Iterator current, int_fast8_t value) const
{
	++current._x;

	while (current._y < _max_y)
	{
		while (current._x < _max_x)
		{
			if (get(current._x, current._y) == value)
			{
				return current;
			}
			else
			{
				++current._x;
			}
		}

		++current._y;
		current._x = 0;
	}

	return end();
}

BoardGrid::Iterator BoardGrid::end()
{
	Iterator it;
	it._x = -1;
	it._y = -1;
	return it;
}