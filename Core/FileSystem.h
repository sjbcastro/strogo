#pragma once

#include <string>

class FileSystem
{
public:
    static std::string getOutputDirectory();
    static void ensureOutputDirectoryExists();
};