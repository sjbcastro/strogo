#pragma once

#include <cinttypes>
#include <vector>
#include <utility>

class BoardGrid
{
public:

	BoardGrid(int_fast8_t max_x, int_fast8_t max_y, int_fast8_t max_states);

	int_fast8_t get(int_fast8_t x, int_fast8_t y) const;
	void set(int_fast8_t x, int_fast8_t y, int_fast8_t value);

	void printToConsole() const;

	std::vector<std::pair<int_fast8_t, int_fast8_t>> getCoordinatesMatchingValue(int_fast8_t value) const;

	class Iterator
	{
	public:

		int_fast8_t x() const { return _x; }
		int_fast8_t y() const { return _y; }

		bool operator==(const Iterator& rhs) const
		{
			return _x == rhs._x && _y == rhs._y;
		}

		bool operator!=(const Iterator& rhs) const
		{
			return _x != rhs._x || _y != rhs._y;
		}

	private:
		friend class BoardGrid;	

		int_fast8_t _x = 0;
		int_fast8_t _y = 0;
	};

	Iterator find(int_fast8_t value) const;

	Iterator findNext(Iterator current, int_fast8_t value) const;

	static Iterator end();

private:

	int_fast8_t masterGetSet(int_fast8_t x, int_fast8_t y, int_fast8_t* new_value, int_fast8_t& value_to_increment_by) const;

	int_fast8_t _max_states;
	int_fast8_t _max_x; 
	int_fast8_t _max_y;
	std::vector<int_fast64_t> _data;
};