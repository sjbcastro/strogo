#! /usr/bin/env python

from getopt import *
import popen2
import sys
import string
import re
from TwoGTPCore import GTP_match

white    = "gnugo --mode gtp --level 0"
black    = "../StroGo/dist/Release/GNU-Linux/strogo"
komi     = ""
size     = "9"
handicap = 9
handicap_type = "fixed"
streak_length = -1
if handicap > 1: 
    komi = "0.5"
else:
    komi = "5.5"


games   = 1
sgfbase = "game"

verbose = 0

match = GTP_match(white, black, size, komi, handicap, handicap_type,
                  streak_length, [])

if False: 
    results = match.endgame_contest(sgfbase)
    win_black = 0
    win_white = 0
    for res in results:
        print res
        if res > 0.0:
            win_white += 1
        elif res < 0.0:
            win_black += 1
    print "%d games, %d wins for black, %d wins for white." \
         % (len(results), win_black, win_white)

else:
    results, cputimes = match.play(games, sgfbase)

    i = 0
    for resw, resb in results:
        i = i + 1
        if resw == resb:
            print "Game %d: %s" % (i, resw)
        else:
            print "Game %d: %s %s" % (i, resb, resw)
    if (cputimes["white"] != "0"):
        print "White: %ss CPU time" % cputimes["white"]
    if (cputimes["black"] != "0"):
        print "Black: %ss CPU time" % cputimes["black"]
