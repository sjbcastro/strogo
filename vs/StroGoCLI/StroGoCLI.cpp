#include <iostream>
#include "GtpInterface.h"
#include "EngineMoveLogger.h"
#include "GoEngineLogger.h"
#include "SgfInfluenceLogger.h"
#include <chrono>
#include <thread>

int main(int argc, char** argv)
{
    auto interface = std::shared_ptr<GoEngine::GtpInterface>(new GoEngine::GtpInterface(nullptr));

    std::vector<std::string> args;
    for (int i = 1; i < argc; ++i)
    {
        args.push_back(argv[i]);
    }

    GameEngineProperties properties(args);
    interface->setGameEngineProperties(properties);

    EngineMoveLogger::instance()->enable();
    GoEngineLogger::instance()->enable();

    while (!interface->quitRequested())
    {
        std::string input = "";
        std::getline(std::cin, input);
        if (!input.empty())
        {
            std::cout << interface->executeGtpCommand(input);
        }
    }

    return 0;
}
